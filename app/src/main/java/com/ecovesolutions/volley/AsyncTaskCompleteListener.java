package com.ecovesolutions.volley;


/**
 * @author Satheeshkumar
 */
public interface AsyncTaskCompleteListener {
	void onTaskCompleted(String response, int serviceCode);
}
