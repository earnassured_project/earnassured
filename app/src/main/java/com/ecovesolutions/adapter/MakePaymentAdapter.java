package com.ecovesolutions.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.MakePayment_Model;
import com.ecovesolutions.model.TransactionModel;

import java.util.ArrayList;

/**
 * Created by Admin on 4/17/2017.
 */

public class MakePaymentAdapter extends ArrayAdapter<MakePayment_Model> {
    Activity activity;
    ArrayList<MakePayment_Model> arr_model;
    Typeface gotham_light,gotham_book;
    public MakePaymentAdapter(Activity activity, int resource, ArrayList<MakePayment_Model> arr_model) {
        super(activity,resource,arr_model);
        // TODO Auto-generated constructor stub
        this.activity=activity;
        this.arr_model=arr_model;
        gotham_light= Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_book.otf");
    }
    private class ViewHolder{
        TextView tv_id,tv_idn,tv_mdate,tv_mdaten,tv_pdate,tv_pdaten,tv_amount,tv_amountn,tv_recipient,tv_recipientn
            ,tv_payment_details,tv_payment_detailsn,tv_action,tv_actionn,tv_contact_recipient,tv_contact_recipientn,tv_extend,tv_extendn,tv_bitcoinn,tv_bitcoin;
    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ViewHolder holder;
        if (view==null) {
            LayoutInflater vi=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=vi.inflate(R.layout.makepaymentadapter, null);
            holder=new ViewHolder();

            holder.tv_id=(TextView)view.findViewById(R.id.tv_id);
            holder.tv_idn=(TextView)view.findViewById(R.id.tv_idn);
            holder.tv_mdate=(TextView)view.findViewById(R.id.tv_mdate);
            holder.tv_mdaten=(TextView)view.findViewById(R.id.tv_mdaten);
            holder.tv_pdate=(TextView)view.findViewById(R.id.tv_pdate);
            holder.tv_pdaten=(TextView)view.findViewById(R.id.tv_pdaten);
            holder.tv_amount=(TextView)view.findViewById(R.id.tv_amount);
            holder.tv_amountn=(TextView)view.findViewById(R.id.tv_amountn);
            holder.tv_recipient=(TextView)view.findViewById(R.id.tv_recipient);
            holder.tv_recipientn=(TextView)view.findViewById(R.id.tv_recipientn);
            holder.tv_payment_details=(TextView)view.findViewById(R.id.tv_payment_details);
            holder.tv_payment_detailsn=(TextView)view.findViewById(R.id.tv_payment_detailsn);
            holder.tv_action=(TextView)view.findViewById(R.id.tv_action);
            holder.tv_actionn=(TextView)view.findViewById(R.id.tv_actionn);
            holder.tv_contact_recipient=(TextView)view.findViewById(R.id.tv_contact_recipient);
            holder.tv_contact_recipientn=(TextView)view.findViewById(R.id.tv_contact_recipientn);
            holder.tv_extend=(TextView)view.findViewById(R.id.tv_extend);
            holder.tv_extendn=(TextView)view.findViewById(R.id.tv_extendn);
            holder.tv_bitcoinn=(TextView)view.findViewById(R.id.tv_bitcoinn);
            holder.tv_bitcoin=(TextView)view.findViewById(R.id.tv_bitcoin);

            holder.tv_id.setTypeface(gotham_book);
            holder.tv_idn.setTypeface(gotham_light);
            holder.tv_mdate.setTypeface(gotham_book);
            holder.tv_mdaten.setTypeface(gotham_light);
            holder.tv_pdate.setTypeface(gotham_book);
            holder.tv_pdaten.setTypeface(gotham_light);
            holder.tv_amount.setTypeface(gotham_book);
            holder.tv_amountn.setTypeface(gotham_light);
            holder.tv_recipient.setTypeface(gotham_book);
            holder.tv_recipientn.setTypeface(gotham_light);
            holder.tv_payment_details.setTypeface(gotham_book);
            holder.tv_payment_detailsn.setTypeface(gotham_light);
            holder.tv_action.setTypeface(gotham_book);
            holder.tv_actionn.setTypeface(gotham_light);
            holder.tv_contact_recipient.setTypeface(gotham_book);
            holder.tv_contact_recipientn.setTypeface(gotham_light);
            holder.tv_extend.setTypeface(gotham_book);
            holder.tv_extendn.setTypeface(gotham_light);
            holder.tv_bitcoin.setTypeface(gotham_book);
            holder.tv_bitcoinn.setTypeface(gotham_light);

            view.setTag(holder);
        } else {
            holder=(ViewHolder)view.getTag();
        }

        final MakePayment_Model model=arr_model.get(position);
        holder.tv_idn.setText(model.getId());
        holder.tv_mdaten.setText(model.getMatch_date());
        holder.tv_pdaten.setText(model.getExpire_date());
        holder.tv_amountn.setText(model.getAmount_off());
        holder.tv_recipientn.setText(model.getUsername());
        holder.tv_actionn.setText(model.getAction());
        holder.tv_payment_detailsn.setText(model.getBankname());

//        holder.tv_bitcoinn.setText(model.getBitcoin());
//        holder.tv_actionn.setText(model.getPayment_confirm());
        holder.tv_contact_recipientn.setText(model.getAccountname());

        return view;
    }

}
