package com.ecovesolutions.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.MakeRequestModel;
import com.ecovesolutions.model.PaymentModel;

import java.util.ArrayList;

/**
 * Created by Admin on 4/19/2017.
 */

public class MakeRequestAdapter extends ArrayAdapter<MakeRequestModel> {

    Activity activity;
    ArrayList<MakeRequestModel> arr_model;
    Typeface gotham_light,gotham_book;
    public MakeRequestAdapter(Activity activity, int resource, ArrayList<MakeRequestModel> arr_model) {
        super(activity, resource,arr_model);
        // TODO Auto-generated constructor stub
        this.activity=activity;
        this.arr_model=arr_model;
        gotham_light=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_book.otf");
    }
    private class ViewHolder{
        TextView tv_recordid,tv_numberid,tv_mdate,tv_date,tv_damount,tv_amount,tv_yieldammount,tv_yamount,tv_bonus,tv_bamount,tv_reqest,tv_ramount,tv_status,tv_inprogress,tv_onetime,tv_onetimen;

    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ViewHolder holder;
        if (view==null) {
            LayoutInflater vi=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=vi.inflate(R.layout.make_request_child, null);
            holder=new ViewHolder();
            holder.tv_recordid = (TextView) view.findViewById(R.id.tv_recordid);
            holder.tv_numberid = (TextView) view.findViewById(R.id.tv_numberid);
            holder.tv_mdate = (TextView) view.findViewById(R.id.tv_mdate);
            holder.tv_date = (TextView) view.findViewById(R.id.tv_date);
            holder.tv_status = (TextView) view.findViewById(R.id.tv_status);
            holder.tv_damount = (TextView) view.findViewById(R.id.tv_damount);
            holder.tv_amount = (TextView) view.findViewById(R.id.tv_amount);
            holder.tv_yieldammount = (TextView) view.findViewById(R.id.tv_yieldammount);
            holder.tv_yamount = (TextView) view.findViewById(R.id.tv_yamount);
            holder.tv_bonus = (TextView) view.findViewById(R.id.tv_bonus);
            holder.tv_bamount = (TextView) view.findViewById(R.id.tv_bamount);
            holder.tv_reqest = (TextView) view.findViewById(R.id.tv_reqest);
            holder.tv_ramount = (TextView) view.findViewById(R.id.tv_ramount);
            holder.tv_inprogress = (TextView) view.findViewById(R.id.tv_inprogress);
            holder.tv_onetime = (TextView) view.findViewById(R.id.tv_onetime);
            holder.tv_onetimen= (TextView) view.findViewById(R.id.tv_onetimen);

            holder.tv_recordid.setTypeface(gotham_book);
            holder.tv_numberid.setTypeface(gotham_light);
            holder.tv_mdate.setTypeface(gotham_book);
            holder.tv_date.setTypeface(gotham_light);
            holder.tv_status.setTypeface(gotham_book);
            holder.tv_damount.setTypeface(gotham_book);
            holder.tv_amount.setTypeface(gotham_light);
            holder.tv_yieldammount.setTypeface(gotham_book);
            holder.tv_yamount.setTypeface(gotham_light);
            holder.tv_bonus.setTypeface(gotham_book);
            holder.tv_bamount.setTypeface(gotham_light);
            holder.tv_status.setTypeface(gotham_book);
            holder.tv_reqest.setTypeface(gotham_book);
            holder.tv_ramount.setTypeface(gotham_light);
            holder.tv_inprogress.setTypeface(gotham_light);
            holder.tv_onetime.setTypeface(gotham_book);
            holder.tv_onetimen.setTypeface(gotham_light);
            view.setTag(holder);
        } else {
            holder=(ViewHolder) view.getTag();
        }

        final MakeRequestModel model=arr_model.get(position);
        holder.tv_numberid.setText(model.getPh_id());
        holder.tv_date.setText(model.getEnd_date());
        holder.tv_amount.setText(model.getAmount_off());
        holder.tv_yamount.setText(model.getYield_amount2());
        holder.tv_bamount.setText(model.getBonus());
        holder.tv_ramount.setText(model.getRequest());
        holder.tv_inprogress.setText(model.getStatus());
        holder.tv_onetimen.setText(model.getOne_time_bonus());
        return view;
    }

}

