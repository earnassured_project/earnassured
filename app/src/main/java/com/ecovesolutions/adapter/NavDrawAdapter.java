package com.ecovesolutions.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.Model_Drawer;

import java.util.ArrayList;

/**
 * @author Satheeshkumar
 **/
public class NavDrawAdapter extends ArrayAdapter<Model_Drawer>{

	Context context;
	ArrayList<Model_Drawer> arr_list;
	Typeface gotham_light,gotham_book;
	public NavDrawAdapter(Context context, int resource,
                          ArrayList<Model_Drawer> arr_list) {
		super(context, resource, arr_list);
		// TODO Auto-generated constructor stub
		this.context=context;
		this.arr_list=arr_list;
		gotham_light= Typeface.createFromAsset(context.getAssets(), "fonts/gotham_light.ttf");
		gotham_book=Typeface.createFromAsset(context.getAssets(), "fonts/gotham_book.otf");
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return arr_list.size();
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Viewholder holder=null;
		if (convertView==null) {
			LayoutInflater infater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView=infater.inflate(R.layout.items_nav, null);
			holder=new Viewholder();
			holder.ll_item=(LinearLayout)convertView.findViewById(R.id.ll_item);
			holder.tv_item=(TextView)convertView.findViewById(R.id.tv_item);
			holder.iv_item=(ImageView)convertView.findViewById(R.id.iv_item);
			holder.tv_item.setTypeface(gotham_light);
			convertView.setTag(holder);
		} else {
			holder=(Viewholder) convertView.getTag();
		}
				
		Model_Drawer model=arr_list.get(position);
		holder.tv_item.setText(model.getName());
		holder.iv_item.setImageResource(model.getImage());

		return convertView;
	}
	
	class Viewholder{
		LinearLayout ll_item;
		TextView tv_item;
		ImageView iv_item;
	}

}
