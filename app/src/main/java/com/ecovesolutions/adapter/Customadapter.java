package com.ecovesolutions.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ecovesolutions.earnassured.R;

/**
 * Created by Admin on 4/13/2017.
 */

public class Customadapter extends ArrayAdapter<String> {
    private final Activity _context;
    private final String[] _text;
  public static   TextView txtpaymentlist;
    Typeface gotham_light,gotham_book;


    public Customadapter(Activity context, String[] text) {
        super(context, R.layout.child_layout, text);
        this._context = context;
        this._text = text;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = _context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.child_layout, null, true);
         txtpaymentlist = (TextView) rowView.findViewById(R.id.tv_child);

        txtpaymentlist.setText(_text[position]);
        gotham_light= Typeface.createFromAsset(getContext().getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(getContext().getAssets(), "fonts/gotham_book.otf");
        txtpaymentlist.setTypeface(gotham_light);
        return rowView;
    }

}