package com.ecovesolutions.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.TestimonialsModel;
import com.ecovesolutions.utilities.CircleImageView;
import com.ecovesolutions.utilities.Constants;

import java.util.ArrayList;

/**
 * Created by Admin on 4/20/2017.
 */

public class TestimonialsAdapter extends RecyclerView.Adapter<TestimonialsAdapter.MyViewHolder> {

    private ArrayList<TestimonialsModel> countryList;
    Activity activity;
    Typeface gotham_light, gotham_book;
    public TestimonialsAdapter(Activity activity, ArrayList<TestimonialsModel> countryList) {
        this.countryList = countryList;
        this.activity=activity;
        gotham_light = Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_light.ttf");
        gotham_book = Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_book.otf");
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name,tv_message;
        public CircleImageView image;

        public MyViewHolder(View view) {
            super(view);
            tv_name=(TextView)view.findViewById(R.id.tv_name);
            tv_message=(TextView)view.findViewById(R.id.tv_message);
            image=(CircleImageView)view.findViewById(R.id.image);

            tv_name.setTypeface(gotham_book);
            tv_message.setTypeface(gotham_light);
        }
    }



    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        TestimonialsModel model = countryList.get(position);
        holder.tv_name.setText(model.getName());
        holder.tv_message.setText(model.getMessage());
        String image=model.getImage();
        if(image != null && !image.isEmpty()){
            Glide.with(activity)
                    .load(image)
                    .asBitmap()
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .priority(Priority.IMMEDIATE)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            holder.image.setImageBitmap(resource);
                        }
                    });

        }else {
            try {
                holder.image.setBackgroundResource(R.mipmap.pro_iv);
                holder.image.setScaleType(ImageView.ScaleType.CENTER_CROP);
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("exception", Log.getStackTraceString(e));
            }
        }
    }

    @Override
    public int getItemCount() {
        return countryList.size();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.testimonialsadapter,parent, false);
        return new MyViewHolder(v);
    }
}