package com.ecovesolutions.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.PaymentConfirmationModel;

import java.util.ArrayList;

/**
 * Created by Admin on 4/20/2017.
 */

public class PaymentConfirmationAdapter extends ArrayAdapter<PaymentConfirmationModel> {

    Activity activity;
    ArrayList<PaymentConfirmationModel> arr_model;
    RelativeLayout rl_main;
    Typeface gotham_light,gotham_book;
    public PaymentConfirmationAdapter(Activity activity, int resource, ArrayList<PaymentConfirmationModel> arr_model, RelativeLayout rl_main) {
        super(activity, resource,arr_model);
        // TODO Auto-generated constructor stub
        this.activity=activity;
        this.arr_model=arr_model;
        this.rl_main=rl_main;
        gotham_light=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_book.otf");
    }
    private class ViewHolder{
        Typeface gotham_light,gotham_book;
        TextView tv_paymentrecord,tv_paymentid,tv_mdate,tv_date,tv_expiredate,tv_edate,tv_ammount,tv_noamount,tv_paidby,tv_paidname,tv_paytype,tv_paytypen,tv_proof,tv_status,tv_inprogress,tv_action,tv_actionn;
        Button btn_view;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ViewHolder holder;
        if (view==null) {
            LayoutInflater vi=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=vi.inflate(R.layout.paymentconfirmationadapter, null);
            holder=new ViewHolder();
            holder.tv_paymentrecord = (TextView) view.findViewById(R.id.tv_paymentrecord);
            holder.tv_paymentid = (TextView) view.findViewById(R.id.tv_paymentid);
            holder.tv_mdate = (TextView) view.findViewById(R.id.tv_mdate);
            holder.tv_date = (TextView) view.findViewById(R.id.tv_date);
            holder.tv_expiredate = (TextView) view.findViewById(R.id.tv_expiredate);
            holder.tv_edate = (TextView) view.findViewById(R.id.tv_edate);
            holder.tv_ammount = (TextView) view.findViewById(R.id.tv_ammount);
            holder.tv_noamount = (TextView) view.findViewById(R.id.tv_noamount);
            holder.tv_paidby = (TextView) view.findViewById(R.id.tv_paidby);
            holder.tv_paidname = (TextView) view.findViewById(R.id.tv_paidname);
            holder.tv_paytype = (TextView) view.findViewById(R.id.tv_paytype);
            holder.tv_paytypen = (TextView) view.findViewById(R.id.tv_paytypen);
            holder.tv_proof = (TextView) view.findViewById(R.id.tv_proof);
            holder.tv_status = (TextView) view.findViewById(R.id.tv_status);
            holder.tv_inprogress = (TextView) view.findViewById(R.id.tv_inprogress);
            holder.tv_action = (TextView) view.findViewById(R.id.tv_action);
            holder.tv_actionn = (TextView) view.findViewById(R.id.tv_actionn);

            holder.btn_view = (Button) view.findViewById(R.id.btn_view);



            holder.tv_paymentrecord.setTypeface(gotham_book);
            holder.tv_paymentid.setTypeface(gotham_light);
            holder.tv_mdate.setTypeface(gotham_book);
            holder.tv_date.setTypeface(gotham_light);
            holder.tv_expiredate.setTypeface(gotham_book);
            holder.tv_edate.setTypeface(gotham_light);
            holder.tv_ammount.setTypeface(gotham_book);
            holder.tv_noamount.setTypeface(gotham_light);
            holder.tv_paidby.setTypeface(gotham_book);
            holder.tv_paidname.setTypeface(gotham_light);
            holder.tv_paytype.setTypeface(gotham_book);
            holder.tv_paytypen.setTypeface(gotham_book);
            holder.tv_proof.setTypeface(gotham_book);
            holder.tv_status.setTypeface(gotham_book);
            holder.tv_inprogress.setTypeface(gotham_light);
            holder.tv_action.setTypeface(gotham_book);
            holder.tv_actionn.setTypeface(gotham_light);
            holder.btn_view.setTypeface(gotham_light);

            view.setTag(holder);
        } else {
            holder=(ViewHolder) view.getTag();
        }

        final PaymentConfirmationModel model=arr_model.get(position);

        holder.tv_paymentid.setText(model.getPh_id());
        holder.tv_date.setText(model.getMatch_date());
        holder.tv_edate.setText(model.getExpire_date());
        holder.tv_noamount.setText(model.getAmount_off());
        holder.tv_paidname.setText(model.getUsername());
        holder.tv_paytypen.setText(model.getType());
        holder.tv_actionn.setText(model.getAction());
        holder.tv_inprogress.setText(model.getStatus());
//        if(model.getStatus().matches("completed")){
//            holder.tv_inprogress.setTextColor(activity.getResources().getColor(R.color.app_color1));
//        }else{
//            holder.tv_inprogress.setTextColor(activity.getResources().getColor(R.color.app_color2));
//        }
        holder.btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                initiatePopUpWindow(model.getProfile_pic());
            }
        });
        return view;
    }
    protected void initiatePopUpWindow(String image) {
        // TODO Auto-generated method stub
        LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view=inflater.inflate(R.layout.popupwindow, null);
        ImageView iv_close=(ImageView)view.findViewById(R.id.iv_close);
        final ImageView iv_image=(ImageView)view.findViewById(R.id.iv_image);
        Button btn_close=(Button)view.findViewById(R.id.btn_close);

        btn_close.setTypeface(gotham_light);
        Glide.with(activity)
                .load(image)
                .asBitmap()
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .priority(Priority.IMMEDIATE)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        iv_image.setImageBitmap(resource);
                    }
                });

        final PopupWindow popUpWindow=new PopupWindow(view, android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                android.view.ViewGroup.LayoutParams.MATCH_PARENT, true);
        popUpWindow.showAtLocation(rl_main, Gravity.CENTER, 0, 0);
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpWindow.dismiss();
            }
        });
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpWindow.dismiss();
            }
        });

    }
}
