package com.ecovesolutions.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.News_Model;
import com.ecovesolutions.utilities.AnyUtils;

import java.util.ArrayList;

/**
 * Created by Admin on 4/14/2017.
 */

public class NewsAdapter extends ArrayAdapter<News_Model> {

    Activity activity;
    ArrayList<News_Model> arr_model;
    public NewsAdapter(Activity activity, int resource, ArrayList<News_Model> arr_model) {
        super(activity, resource,arr_model);
        // TODO Auto-generated constructor stub
        this.activity=activity;
        this.arr_model=arr_model;
    }
    private class ViewHolder{
        TextView tv_subject,tv_message;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ViewHolder holder;
        if (convertView==null) {
            LayoutInflater vi=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=vi.inflate(R.layout.newsadapter, null);
            holder=new ViewHolder();
            holder.tv_subject=(TextView)convertView.findViewById(R.id.tv_subject);
            holder.tv_message=(TextView)convertView.findViewById(R.id.tv_message);

            holder.tv_subject.setTypeface(AnyUtils.getGothamLight(activity));
            holder.tv_message.setTypeface(AnyUtils.getGothamBook(activity));
            convertView.setTag(holder);
        } else {
            holder=(ViewHolder) convertView.getTag();
        }

        final News_Model model=arr_model.get(position);

        holder.tv_subject.setText(model.getSubject());
        holder.tv_message.setText(model.getMessage());

        return convertView;
    }

}
