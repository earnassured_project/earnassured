package com.ecovesolutions.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.Allpayment_Model;
import com.ecovesolutions.model.TransactionModel;

import java.util.ArrayList;

/**
 * Created by Admin on 4/17/2017.
 */

public class AllpaymnetAdapter extends ArrayAdapter<Allpayment_Model> {


        Activity activity;
        ArrayList<Allpayment_Model> arr_model;
        Typeface gotham_light,gotham_book;
public AllpaymnetAdapter(Activity activity, int resource, ArrayList<Allpayment_Model> arr_model) {
        super(activity, resource,arr_model);
        // TODO Auto-generated constructor stub
        this.activity=activity;
        this.arr_model=arr_model;
        gotham_light=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_book.otf");
        }
private class ViewHolder{
    TextView tv_allrecordid,tv_allpayidnumber,tv_matchdate,tv_mathdatenumber,tv_ammount,
            tv_ammountnumber,tv_Recipient,tv_Recipientnumber,tv_payment_details,tv_payment_detailsnumber,tv_Confirmation,tv_cvalue;
        }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final AllpaymnetAdapter.ViewHolder holder;
        if (convertView==null) {
            LayoutInflater vi=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=vi.inflate(R.layout.allpayment_child, null);
            holder=new AllpaymnetAdapter.ViewHolder();


            holder.tv_allrecordid = (TextView) convertView.findViewById(R.id.tv_allrecordid);
            holder.tv_allpayidnumber = (TextView) convertView.findViewById(R.id.tv_allpayidnumber);
            holder.tv_matchdate = (TextView) convertView.findViewById(R.id.tv_matchdate);
            holder.tv_mathdatenumber = (TextView) convertView.findViewById(R.id.tv_mathdatenumber);
            holder.tv_ammount = (TextView) convertView.findViewById(R.id.tv_ammount);
            holder.tv_ammountnumber = (TextView) convertView.findViewById(R.id.tv_ammountnumber);
            holder.tv_Recipient = (TextView) convertView.findViewById(R.id.tv_Recipient);
            holder.tv_Recipientnumber = (TextView) convertView.findViewById(R.id.tv_Recipientnumber);
            holder.tv_payment_details = (TextView) convertView.findViewById(R.id.tv_payment_details);
            holder.tv_payment_detailsnumber = (TextView) convertView.findViewById(R.id.tv_payment_detailsnumber);
            holder.tv_Confirmation = (TextView) convertView.findViewById(R.id.tv_Confirmation);
            holder.tv_cvalue = (TextView) convertView.findViewById(R.id.tv_cvalue);



            holder.tv_allrecordid.setTypeface(gotham_book);
            holder.tv_allpayidnumber.setTypeface(gotham_book);
            holder.tv_matchdate.setTypeface(gotham_book);
            holder.tv_mathdatenumber.setTypeface(gotham_light);
            holder.tv_ammount.setTypeface(gotham_book);
            holder.tv_Recipient.setTypeface(gotham_book);
            holder.tv_ammountnumber.setTypeface(gotham_light);
            holder.tv_Recipientnumber.setTypeface(gotham_light);
            holder.tv_payment_details.setTypeface(gotham_book);
            holder.tv_Confirmation.setTypeface(gotham_book);
            holder.tv_cvalue.setTypeface(gotham_light);
            holder.tv_payment_detailsnumber.setTypeface(gotham_light);

            convertView.setTag(holder);
        } else {
            holder=(AllpaymnetAdapter.ViewHolder) convertView.getTag();
        }

        final Allpayment_Model model=arr_model.get(position);

        holder.tv_allpayidnumber.setText(model.getPh_id());
        holder.tv_matchdate.setText(model.getMatch_date());
        holder.tv_ammountnumber.setText(model.getAmount_off());
        holder.tv_cvalue.setText(model.getPayment_confirm());
        holder.tv_Recipientnumber.setText(model.getUsername());
        holder.tv_payment_detailsnumber.setText(model.getBitcoin());

        if(model.getPayment_confirm().matches("Confirmed")){
            holder.tv_cvalue.setTextColor(activity.getResources().getColor(R.color.app_color1));
        }else{
            holder.tv_cvalue.setTextColor(activity.getResources().getColor(R.color.app_color2));
        }
        return convertView;
    }

}
