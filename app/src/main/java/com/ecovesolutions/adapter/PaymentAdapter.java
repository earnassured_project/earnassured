package com.ecovesolutions.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.PaymentModel;
import com.ecovesolutions.model.ViewReferral_Model;

import java.util.ArrayList;

import static com.ecovesolutions.fragment.Change_Password.context;

/**
 * Created by Admin on 4/18/2017.
 */

public class PaymentAdapter extends ArrayAdapter<PaymentModel> {

    Activity activity;
    ArrayList<PaymentModel> arr_model;
    Typeface gotham_light,gotham_book;
    RelativeLayout rl_main;
    public PaymentAdapter(Activity activity, int resource, ArrayList<PaymentModel> arr_model,RelativeLayout rl_main) {
        super(activity, resource,arr_model);
        // TODO Auto-generated constructor stub
        this.activity=activity;
        this.arr_model=arr_model;
        this.rl_main=rl_main;
        gotham_light=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_book.otf");
    }
    private class ViewHolder{
        TextView tv_histrorysnumber,tv_hsno,tv_request,tv_rid,tv_expiredate,tv_edate,tv_paidbyhistory,tv_historypaidbyname,tv_status,tv_confirmed,tv_proof,tv_amount,tv_amountn;
        Button btn_view;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final PaymentAdapter.ViewHolder holder;
        if (convertView==null) {
            LayoutInflater vi=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=vi.inflate(R.layout.payment_child, null);
            holder=new PaymentAdapter.ViewHolder();
            holder.tv_histrorysnumber = (TextView) convertView.findViewById(R.id.tv_histrorysnumber);
            holder.tv_hsno = (TextView) convertView.findViewById(R.id.tv_hsno);
            holder.tv_request = (TextView) convertView.findViewById(R.id.tv_request);
            holder.tv_rid = (TextView) convertView.findViewById(R.id.tv_rid);
            holder.tv_status = (TextView) convertView.findViewById(R.id.tv_status);
            holder.tv_expiredate = (TextView) convertView.findViewById(R.id.tv_expiredate);
            holder.tv_edate=(TextView) convertView.findViewById(R.id.tv_edate);
            holder.tv_paidbyhistory = (TextView) convertView.findViewById(R.id.tv_paidbyhistory);
            holder.tv_historypaidbyname= (TextView) convertView.findViewById(R.id.tv_historypaidbyname);
            holder.tv_hsno = (TextView) convertView.findViewById(R.id.tv_hsno);
            holder.tv_request = (TextView) convertView.findViewById(R.id.tv_request);
            holder.btn_view = (Button) convertView.findViewById(R.id.btn_view);
            holder.tv_confirmed= (TextView) convertView.findViewById(R.id.tv_confirmed);
            holder.tv_proof= (TextView) convertView.findViewById(R.id.tv_proof);
            holder.tv_amount= (TextView) convertView.findViewById(R.id.tv_amount);
            holder.tv_amountn= (TextView) convertView.findViewById(R.id.tv_amountn);

            holder.tv_histrorysnumber.setTypeface(gotham_book);
            holder.tv_hsno.setTypeface(gotham_light);
            holder.tv_request.setTypeface(gotham_book);
            holder.tv_rid.setTypeface(gotham_light);
            holder.tv_expiredate.setTypeface(gotham_book);
            holder.tv_edate.setTypeface(gotham_light);
            holder.tv_paidbyhistory.setTypeface(gotham_book);
            holder.tv_historypaidbyname.setTypeface(gotham_light);
            holder.tv_status.setTypeface(gotham_book);
            holder.tv_confirmed.setTypeface(gotham_light);
            holder.tv_proof.setTypeface(gotham_book);
            holder.btn_view.setTypeface(gotham_light);
            holder.tv_amount.setTypeface(gotham_book);
            holder.tv_amountn.setTypeface(gotham_light);

            convertView.setTag(holder);
        } else {
            holder=(PaymentAdapter.ViewHolder) convertView.getTag();
        }

        final PaymentModel model=arr_model.get(position);

        holder.tv_hsno.setText(model.getSno());
        holder.tv_rid.setText(model.getRequest_id());
        holder.tv_edate.setText(model.getExpiry_date());
        holder.tv_historypaidbyname.setText(model.getUsername());
        holder.tv_confirmed.setText(model.getPayment_confirm());
        holder.tv_amountn.setText(model.getProfile_pic());
        holder.btn_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                initiatePopUpWindow(model.getProfile_pic());
            }
        });
        return convertView;
    }
    protected void initiatePopUpWindow(String image) {
        // TODO Auto-generated method stub
        LayoutInflater inflater=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view=inflater.inflate(R.layout.popupwindow, null);
        ImageView iv_close=(ImageView)view.findViewById(R.id.iv_close);
        final ImageView iv_image=(ImageView)view.findViewById(R.id.iv_image);
        Button btn_close=(Button)view.findViewById(R.id.btn_close);

        btn_close.setTypeface(gotham_light);
        Glide.with(activity)
                .load(image)
                .asBitmap()
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.RESULT)
                .priority(Priority.IMMEDIATE)
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        iv_image.setImageBitmap(resource);
                    }
                });

        final PopupWindow popUpWindow=new PopupWindow(view, android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                android.view.ViewGroup.LayoutParams.MATCH_PARENT, true);
        popUpWindow.showAtLocation(rl_main, Gravity.CENTER, 0, 0);
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpWindow.dismiss();
            }
        });
        btn_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpWindow.dismiss();
            }
        });

    }
}
