package com.ecovesolutions.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.ReferralBonus_Model;
import com.ecovesolutions.model.ViewReferral_Model;

import java.util.ArrayList;

/**
 * Created by Admin on 4/13/2017.
 */

public class ReferralBonusAdapter extends ArrayAdapter<ReferralBonus_Model> {

    Activity activity;
    ArrayList<ReferralBonus_Model> arr_model;
    Typeface gotham_light,gotham_book;
    public ReferralBonusAdapter(Activity activity, int resource, ArrayList<ReferralBonus_Model> arr_model) {
        super(activity, resource,arr_model);
        // TODO Auto-generated constructor stub
        this.activity=activity;
        this.arr_model=arr_model;
        gotham_light=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_book.otf");
    }
    private class ViewHolder{
        TextView tv_id,tv_idn,tv_name,tv_namen,tv_email,tv_emailn,tv_phone,tv_phonen,tv_country,tv_countryn,tv_date,tv_daten;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ViewHolder holder;
        if (convertView==null) {
            LayoutInflater vi=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=vi.inflate(R.layout.referralbonusadapter, null);
            holder=new ViewHolder();
            holder.tv_id=(TextView)convertView.findViewById(R.id.tv_id);
            holder.tv_idn=(TextView)convertView.findViewById(R.id.tv_idn);
            holder.tv_name=(TextView)convertView.findViewById(R.id.tv_name);
            holder.tv_namen=(TextView)convertView.findViewById(R.id.tv_namen);
            holder.tv_email=(TextView)convertView.findViewById(R.id.tv_email);
            holder.tv_emailn=(TextView)convertView.findViewById(R.id.tv_emailn);
            holder.tv_phone=(TextView)convertView.findViewById(R.id.tv_phone);
            holder.tv_phonen=(TextView)convertView.findViewById(R.id.tv_phonen);
            holder.tv_country=(TextView)convertView.findViewById(R.id.tv_country);
            holder.tv_countryn=(TextView)convertView.findViewById(R.id.tv_countryn);
            holder.tv_date=(TextView)convertView.findViewById(R.id.tv_date);
            holder.tv_daten=(TextView)convertView.findViewById(R.id.tv_daten);


            holder.tv_id.setTypeface(gotham_book);
            holder.tv_name.setTypeface(gotham_book);
            holder.tv_email.setTypeface(gotham_book);
            holder.tv_phone.setTypeface(gotham_book);
            holder.tv_country.setTypeface(gotham_book);
            holder.tv_date.setTypeface(gotham_book);

            holder.tv_idn.setTypeface(gotham_light);
            holder.tv_namen.setTypeface(gotham_light);
            holder.tv_emailn.setTypeface(gotham_light);
            holder.tv_phonen.setTypeface(gotham_light);
            holder.tv_countryn.setTypeface(gotham_light);
            holder.tv_daten.setTypeface(gotham_light);

            convertView.setTag(holder);
        } else {
            holder=(ViewHolder) convertView.getTag();
        }

        final ReferralBonus_Model model=arr_model.get(position);

        holder.tv_idn.setText(model.getSno());
        holder.tv_namen.setText(model.getMember());
        holder.tv_emailn.setText(model.getDonation_amount());
        holder.tv_phonen.setText(model.getMybonus());
        holder.tv_countryn.setText(model.getStatus());
        holder.tv_daten.setText(model.getDate());

        return convertView;
    }

}
