package com.ecovesolutions.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.utilities.CircleImageView;
import com.ecovesolutions.utilities.Constants;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 4/12/2017.
 */

public class RecentAdapter extends BaseAdapter {
    Activity activity;
    ArrayList<HashMap<String, String>> arr_model;
    Typeface gotham_light,gotham_book;
    public RecentAdapter(Activity activity,ArrayList<HashMap<String, String>> arr_model)
    {
        this.activity=activity;
        this.arr_model=arr_model;
        gotham_light=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_book.otf");
    }
    public class ViewHolder{
        TextView tv_time;
        Button btn_type;
        CircleImageView userProfilePic;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        final ViewHolder holder;
        if (convertView==null) {
            LayoutInflater vi=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=vi.inflate(R.layout.recent_items, null);
            holder=new ViewHolder();
            holder.tv_time=(TextView)convertView.findViewById(R.id.tv_time);
            holder.btn_type=(Button)convertView.findViewById(R.id.btn_type);
            holder.userProfilePic=(CircleImageView)convertView.findViewById(R.id.userProfilePic);

            holder.tv_time.setTypeface(gotham_light);
            holder.btn_type.setTypeface(gotham_book);
            convertView.setTag(holder);
        } else {
            holder=(ViewHolder) convertView.getTag();
        }
        HashMap<String, String> map=arr_model.get(position);
        holder.tv_time.setText(map.get(Constants.Params.TIME));
        if(map.get(Constants.Params.TYPE).matches("Active")){
            holder.btn_type.setBackground(activity.getResources().getDrawable(R.drawable.app_button1));
            holder.btn_type.setText("Active");
        }else{
            holder.btn_type.setBackground(activity.getResources().getDrawable(R.drawable.app_button2));
            holder.btn_type.setText("Blocked");
        }
        String image=map.get(Constants.Params.IMAGE_URL);
        if(image != null && !image.isEmpty()){
            Glide.with(activity)
                    .load(image)
                    .asBitmap()
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .priority(Priority.IMMEDIATE)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            holder.userProfilePic.setImageBitmap(resource);
                        }
                    });

        }else {
            try {
                holder.userProfilePic.setBackgroundResource(R.mipmap.pro_iv);
                holder.userProfilePic.setScaleType(ImageView.ScaleType.CENTER_CROP);
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("exception", Log.getStackTraceString(e));
            }
        }
        return convertView;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return arr_model.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }
}
