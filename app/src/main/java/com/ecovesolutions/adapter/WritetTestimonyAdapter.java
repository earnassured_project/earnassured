package com.ecovesolutions.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.WritetTestimonyModel;

import java.util.ArrayList;

/**
 * Created by Admin on 4/20/2017.
 */

public class WritetTestimonyAdapter extends RecyclerView.Adapter<WritetTestimonyAdapter.MyViewHolder> {
    Activity activity;
    ArrayList<WritetTestimonyModel> arr_model;
    Typeface gotham_light,gotham_book;
    public WritetTestimonyAdapter(Activity activity, int resource, ArrayList<WritetTestimonyModel> arr_model) {
        // TODO Auto-generated constructor stub
        this.activity=activity;
        this.arr_model=arr_model;
        gotham_light= Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_book.otf");
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.writetestimonychild,parent, false);
        return new MyViewHolder(v);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_record,tv_name,tv_message,tv_reqestedamount,tv_status,
                tv_recordn,tv_namen,tv_messagen,tv_reqestedamountn,tv_statusn;
        public MyViewHolder(View view) {
            super(view);
            tv_record = (TextView) view.findViewById(R.id.tv_record);
            tv_name = (TextView) view.findViewById(R.id.tv_name);
            tv_message = (TextView) view.findViewById(R.id.tv_message);
            tv_reqestedamount = (TextView) view.findViewById(R.id.tv_reqestedamount);
            tv_status = (TextView) view.findViewById(R.id.tv_status);


            tv_recordn = (TextView) view.findViewById(R.id.tv_recordn);
            tv_namen= (TextView) view.findViewById(R.id.tv_namen);
            tv_messagen = (TextView) view.findViewById(R.id.tv_messagen);
            tv_reqestedamountn = (TextView) view.findViewById(R.id.tv_reqestedamountn);
            tv_statusn = (TextView) view.findViewById(R.id.tv_statusn);

            tv_record.setTypeface(gotham_book);
            tv_name.setTypeface(gotham_book);
            tv_message.setTypeface(gotham_book);
            tv_reqestedamount.setTypeface(gotham_book);
            tv_status.setTypeface(gotham_book);

            tv_recordn.setTypeface(gotham_light);
            tv_namen.setTypeface(gotham_light);
            tv_messagen.setTypeface(gotham_light);
            tv_reqestedamountn.setTypeface(gotham_light);
            tv_statusn.setTypeface(gotham_light);
        }
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        WritetTestimonyModel model=arr_model.get(position);
        holder.tv_recordn.setText(model.getId());
        holder.tv_namen.setText(model.getUsername());
        holder.tv_messagen.setText(model.getMessage());
        holder.tv_reqestedamountn.setText(model.getAmount());
        holder.tv_statusn.setText(model.getStatus());


    }

    @Override
    public int getItemCount() {
        return arr_model.size();
    }


}
