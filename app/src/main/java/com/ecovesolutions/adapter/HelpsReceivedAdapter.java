package com.ecovesolutions.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.HelpsReceivedModel;
import com.ecovesolutions.utilities.CircleImageView;

import java.util.ArrayList;

/**
 * Created by Admin on 4/21/2017.
 */

public class HelpsReceivedAdapter extends ArrayAdapter<HelpsReceivedModel> {
    Activity activity;
    ArrayList<HelpsReceivedModel> arr_model;
    Typeface gotham_light,gotham_book;
    public HelpsReceivedAdapter(Activity activity, int resource, ArrayList<HelpsReceivedModel> arr_model) {
        super(activity, resource,arr_model);
        // TODO Auto-generated constructor stub
        this.activity=activity;
        this.arr_model=arr_model;
        gotham_light=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_book.otf");
    }
    private class ViewHolder{
        TextView tv_name,tv_namen,tv_amount,tv_amountn,tv_date,tv_daten;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ViewHolder holder;
        if (convertView==null) {
            LayoutInflater vi=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=vi.inflate(R.layout.helpsreceivedchild, null);
            holder=new ViewHolder();

            holder.tv_name = (TextView) convertView.findViewById(R.id.tv_name);
            holder.tv_namen = (TextView) convertView.findViewById(R.id.tv_namen);
            holder.tv_amount = (TextView) convertView.findViewById(R.id.tv_amount);
            holder.tv_amountn = (TextView) convertView.findViewById(R.id.tv_amountn);
            holder.tv_date = (TextView) convertView.findViewById(R.id.tv_date);
            holder.tv_daten = (TextView) convertView.findViewById(R.id.tv_daten);

            holder.tv_name.setTypeface(gotham_book);
            holder.tv_namen.setTypeface(gotham_light);
            holder.tv_amount.setTypeface(gotham_book);
            holder.tv_amountn.setTypeface(gotham_light);
            holder.tv_date.setTypeface(gotham_book);
            holder.tv_daten.setTypeface(gotham_light);

            convertView.setTag(holder);
        } else {
            holder=(ViewHolder) convertView.getTag();
        }

        final HelpsReceivedModel model=arr_model.get(position);

        holder.tv_namen.setText(model.getName());
        holder.tv_amountn.setText(model.getAmount());
        holder.tv_daten.setText(model.getDate());

        return convertView;
    }

}

