package com.ecovesolutions.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.ReferralBonus_Model;
import com.ecovesolutions.model.Reqhistory_Model;
import com.ecovesolutions.model.TransactionModel;

import java.util.ArrayList;

/**
 * Created by Admin on 4/18/2017.
 */

public class RequestHistoryAdapter extends ArrayAdapter<Reqhistory_Model> {

    Activity activity;
    ArrayList<Reqhistory_Model> arr_model;
    Typeface gotham_light,gotham_book;

    public RequestHistoryAdapter(Activity activity, int resource, ArrayList<Reqhistory_Model> arr_model) {
        super(activity, resource,arr_model);
        // TODO Auto-generated constructor stub
        this.activity=activity;
        this.arr_model=arr_model;
        gotham_light=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_book.otf");
    }
    private class ViewHolder{
        TextView tv_sno,tv_snumber,tv_reqdate,tv_rdate,tv_totalamount,tv_tamount,tv_amountpaid,tv_amountnumber,tv_amountoutstanding,tv_outnumber,tv_paymentstatus;
        TextView btn_confirmed,tv_refbonus,tv_refbonusn;    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final RequestHistoryAdapter.ViewHolder holder;
        if (convertView==null) {
            LayoutInflater vi=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=vi.inflate(R.layout.requesthistory_child, null);
            holder=new RequestHistoryAdapter.ViewHolder();
            holder.tv_sno = (TextView) convertView.findViewById(R.id.tv_sno);
            holder.  tv_snumber = (TextView) convertView.findViewById(R.id.tv_snumber);
            holder.  tv_reqdate = (TextView) convertView.findViewById(R.id.tv_reqdate);
            holder.  tv_rdate = (TextView) convertView.findViewById(R.id.tv_rdate);
            holder. tv_totalamount = (TextView) convertView.findViewById(R.id.tv_totalamount);
            holder. tv_tamount = (TextView) convertView.findViewById(R.id.tv_tamount);
            holder. tv_amountpaid = (TextView) convertView.findViewById(R.id.tv_amountpaid);
            holder.  tv_amountnumber = (TextView) convertView.findViewById(R.id.tv_amountnumber);
            holder.   tv_amountoutstanding = (TextView) convertView.findViewById(R.id.tv_amountoutstanding);
            holder.  tv_outnumber = (TextView) convertView.findViewById(R.id.tv_outnumber);
            holder. tv_paymentstatus = (TextView) convertView.findViewById(R.id.tv_paymentstatus);
            holder.  btn_confirmed = (TextView) convertView.findViewById(R.id.btn_confirmed);
            holder.tv_refbonus= (TextView) convertView.findViewById(R.id.tv_refbonus);
            holder.tv_refbonusn= (TextView) convertView.findViewById(R.id.tv_refbonusn);
            holder.tv_sno.setTypeface(gotham_book);
            holder. tv_snumber.setTypeface(gotham_book);
            holder. tv_reqdate.setTypeface(gotham_book);
            holder. tv_rdate.setTypeface(gotham_book);
            holder. tv_totalamount.setTypeface(gotham_book);
            holder.tv_amountpaid.setTypeface(gotham_book);
            holder.tv_tamount.setTypeface(gotham_book);
            holder.tv_amountoutstanding.setTypeface(gotham_book);
            holder.tv_amountnumber.setTypeface(gotham_book);
            holder.tv_paymentstatus.setTypeface(gotham_book);
            holder. btn_confirmed.setTypeface(gotham_light);
            holder.tv_outnumber.setTypeface(gotham_book);
            holder.  btn_confirmed.setTypeface(gotham_light);
            holder.tv_refbonus.setTypeface(gotham_book);
            holder.tv_refbonusn.setTypeface(gotham_light);
            convertView.setTag(holder);
        } else {
            holder=(RequestHistoryAdapter.ViewHolder) convertView.getTag();
        }

        final Reqhistory_Model model=arr_model.get(position);

        holder.tv_snumber.setText(model.getSno());
        holder.tv_rdate.setText(model.getRequest_date());
        holder.tv_tamount.setText(model.getAmount_paid());
        holder.tv_outnumber.setText(model.getAmountout());
        holder.tv_amountnumber.setText(model.getAmount_off());
        holder.btn_confirmed.setText(model.getPayment_status());
        holder.tv_refbonusn.setText(model.getRef_bonus());
        return convertView;
    }

}
















































