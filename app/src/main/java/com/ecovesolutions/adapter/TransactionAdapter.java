package com.ecovesolutions.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.ReferralBonus_Model;
import com.ecovesolutions.model.TransactionModel;

import java.util.ArrayList;

/**
 * Created by Admin on 4/17/2017.
 */

public class TransactionAdapter extends ArrayAdapter<TransactionModel> {


    Activity activity;
    ArrayList<TransactionModel> arr_model;
    Typeface gotham_light,gotham_book;
    public TransactionAdapter(Activity activity, int resource, ArrayList<TransactionModel> arr_model) {
        super(activity, resource,arr_model);
        // TODO Auto-generated constructor stub
        this.activity=activity;
        this.arr_model=arr_model;
        gotham_light=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(activity.getAssets(), "fonts/gotham_book.otf");
    }
    private class ViewHolder{
        TextView tv_transrecordid,tv_hsno,tv_startdate,tv_startdatenumber,tv_ammountofferd,
                tv_offernumber,tv_paidamount,tv_amountnumber,tv_tconfirmed,tv_tnumber,tv_out,tv_outnumber,tv_cred,tv_crednumber,tv_status,tv_inprogress;    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ViewHolder holder;
        if (convertView==null) {
            LayoutInflater vi=(LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=vi.inflate(R.layout.transaction_history, null);
            holder=new ViewHolder();


            holder.tv_transrecordid = (TextView) convertView.findViewById(R.id.tv_transrecordid);
            holder.tv_hsno = (TextView) convertView.findViewById(R.id.tv_hsno);
            holder.tv_startdate = (TextView) convertView.findViewById(R.id.tv_startdate);
            holder.tv_startdatenumber = (TextView) convertView.findViewById(R.id.tv_startdatenumber);
            holder.tv_ammountofferd = (TextView) convertView.findViewById(R.id.tv_ammountofferd);
            holder.tv_offernumber = (TextView) convertView.findViewById(R.id.tv_offernumber);
            holder.tv_paidamount = (TextView) convertView.findViewById(R.id.tv_paidamount);
            holder.tv_amountnumber = (TextView) convertView.findViewById(R.id.tv_amountnumber);
            holder.tv_tconfirmed = (TextView) convertView.findViewById(R.id.tv_tconfirmed);
            holder.tv_tnumber = (TextView) convertView.findViewById(R.id.tv_tnumber);
            holder.tv_out = (TextView) convertView.findViewById(R.id.tv_out);
            holder.tv_status = (TextView) convertView.findViewById(R.id.tv_status);
            holder.tv_inprogress = (TextView) convertView.findViewById(R.id.tv_inprogress);
            holder.tv_outnumber = (TextView) convertView.findViewById(R.id.tv_outnumber);
            holder.tv_cred = (TextView) convertView.findViewById(R.id.tv_cred);
            holder.tv_crednumber = (TextView) convertView.findViewById(R.id.tv_crednumber);



            holder.tv_transrecordid.setTypeface(gotham_book);
            holder.tv_hsno.setTypeface(gotham_book);
            holder.tv_startdate.setTypeface(gotham_book);
            holder.tv_startdatenumber.setTypeface(gotham_light);
            holder.tv_ammountofferd.setTypeface(gotham_book);
            holder.tv_paidamount.setTypeface(gotham_book);
            holder.tv_offernumber.setTypeface(gotham_light);
            holder.tv_amountnumber.setTypeface(gotham_light);
            holder.tv_tconfirmed.setTypeface(gotham_book);
            holder.tv_out.setTypeface(gotham_book);
            holder.tv_outnumber.setTypeface(gotham_light);
            holder.tv_status.setTypeface(gotham_book);
            holder.tv_inprogress.setTypeface(gotham_light);
            holder.tv_cred.setTypeface(gotham_book);
            holder.tv_tnumber.setTypeface(gotham_light);
            holder.tv_crednumber.setTypeface(gotham_light);
            convertView.setTag(holder);
        } else {
            holder=(ViewHolder) convertView.getTag();
        }

        final TransactionModel model=arr_model.get(position);

        holder.tv_hsno.setText(model.getId());
        holder.tv_startdatenumber.setText(model.getStart_date());
        holder.tv_offernumber.setText(model.getAmount_off());
        holder.tv_amountnumber.setText(model.getAmount_paid());
        holder.tv_tnumber.setText(model.getTotal_confirmed());
        holder.tv_crednumber.setText(model.getCred_score());
        holder.tv_outnumber.setText(model.getAmountout());
        holder.tv_inprogress.setText(model.getStatus());
//        if(model.getStatus().matches("completed")){
//            holder.tv_inprogress.setTextColor(activity.getResources().getColor(R.color.app_color1));
//        }else{
//            holder.tv_inprogress.setTextColor(activity.getResources().getColor(R.color.app_color2));
//        }

        return convertView;
    }

}
