package com.ecovesolutions.earnassured;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.ecovesolutions.fragment.Contact_Us;
import com.ecovesolutions.fragment.Dashboard;
import com.ecovesolutions.fragment.DashboardFragment;
import com.ecovesolutions.fragment.DonateAdmin_Fragment;
import com.ecovesolutions.fragment.DonateCharity_Fragment;
import com.ecovesolutions.fragment.FragmentDrawer;
import com.ecovesolutions.fragment.GiveHelp_Fragment;
import com.ecovesolutions.fragment.MyAccount_Fragment;
import com.ecovesolutions.fragment.Myreferral_Fragment;
import com.ecovesolutions.fragment.News_Fragment;
import com.ecovesolutions.fragment.ReceiveHelp_Fragment;
import com.ecovesolutions.fragment.RecentHelp_Fragment;
import com.ecovesolutions.fragment.Write_Testimony;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener{
    Activity activity;
    Toolbar toolbar;
    public FragmentDrawer drawerFragment;
    DrawerLayout drawerLayout;
    PreferenceHelper pHelper;
    private CallbackManager callbackManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity=this;
        try {
            FacebookSdk.sdkInitialize(getApplicationContext());
            callbackManager= CallbackManager.Factory.create();
        } catch (Exception e) {
            // TODO: handle exception
//				Log.e("ttt", ""+Log.getStackTraceString(e));
        }
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        pHelper = new PreferenceHelper(activity);
        drawerLayout=(DrawerLayout)findViewById(R.id.drawer_layout);
        drawerFragment = (FragmentDrawer)getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);
        drawerFragment.setDrawerListener(this);
        displayView(1);
    }
    private void displayView(int position) {
        Log.d("ttt","a="+position);
        Fragment fragment = null;
        String title = getString(R.string.app_name);
        switch (position) {
            case 1:
                title = getString(R.string.dashboard);
                fragment = new Dashboard().newInstance();
                break;
            case 2:
                title = getString(R.string.myreferrals);
                fragment=new Myreferral_Fragment().newInstance();
                break;
            case 3:
                title = getString(R.string.myaccount);
                fragment=new MyAccount_Fragment().newInstance();
                break;
            case 4:
                title = getString(R.string.givehelp);
                fragment=new GiveHelp_Fragment().newInstance();
                break;
            case 5:
                title = getString(R.string.receivehelp);
                fragment=new ReceiveHelp_Fragment().newInstance();
                break;
            case 6:
                title = getString(R.string.givetestimony);
                fragment=new Write_Testimony().newInstance();
                break;
            case 7:
                title = getString(R.string.news);
                fragment=new News_Fragment().newInstance();
                break;
            case 8:
                title = getString(R.string.dt_admin);
                fragment=new DonateAdmin_Fragment().newInstance();
                break;
            case 9:
                title = getString(R.string.dt_charity);
                fragment=new DonateCharity_Fragment().newInstance();

                break;
            case 10:
                title = getString(R.string.contact_us);
                fragment=new Contact_Us().newInstance();
                break;
            case 11:
                Intent intent=new Intent(getApplicationContext(), Login_view.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                pHelper.Logout();
                LoginManager.getInstance().logOut();
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
        getSupportActionBar().setTitle(title);


    }
//    public static void check(Contaxt activity){
//        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        fragmentTransaction.replace(R.id.container_body, fragment);
//        fragmentTransaction.addToBackStack(null);
//        fragmentTransaction.commit();
//    }
    @Override
    public void onDrawerItemSelected(View view, int position) {
        // TODO Auto-generated method stub
        displayView(position);
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
