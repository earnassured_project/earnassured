package com.ecovesolutions.earnassured;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Login_view extends AppCompatActivity implements AsyncTaskCompleteListener,Response.ErrorListener{
    Activity activity;
    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;
    Typeface gotham_light,gotham_book;
    TextView tv_home,tv_forgot,tv_welecomeloginview,tv_discription,txtCaptcha;
    Button btn_register,btn_login;
    EditText et_mail,et_password ,edCaptcha;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_view);

        activity=this;
        pHelper = new PreferenceHelper(this);
        requestQueue = Volley.newRequestQueue(this);
        tv_home=(TextView)findViewById(R.id.tv_home);
        tv_welecomeloginview=(TextView)findViewById(R.id.tv_welecomeloginview);
        tv_forgot=(TextView)findViewById(R.id.tv_forgot);
        btn_register=(Button)findViewById(R.id.btn_register);
        btn_login=(Button)findViewById(R.id.btn_login);
        et_mail=(EditText)findViewById(R.id.et_mail);
        et_password=(EditText)findViewById(R.id.et_password);
        edCaptcha=(EditText)findViewById(R.id.edCaptcha);
        txtCaptcha = (TextView)findViewById(R.id.txtCaptcha);

        //font style
        gotham_light= Typeface.createFromAsset(this.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(this.getAssets(), "fonts/gotham_book.otf");
        tv_home.setTypeface(gotham_book);
        tv_forgot.setTypeface(gotham_book);

        tv_welecomeloginview.setTypeface(gotham_book);
        btn_register.setTypeface(gotham_light);
        btn_login.setTypeface(gotham_light);
        et_mail.setTypeface(gotham_light);
        et_password.setTypeface(gotham_light);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((et_mail.getText().toString().length()==0)) {
                    Toast.makeText(getApplicationContext(), "Please enter Email or Username", Toast.LENGTH_SHORT).show();
                }else if (et_password.getText().toString().length()==0) {
                    Toast.makeText(getApplicationContext(), "Please enter Password", Toast.LENGTH_SHORT).show();
                }else if (et_password.getText().toString().length()<6) {
                    Toast.makeText(getApplicationContext(), "Password Minimum 6 Characters", Toast.LENGTH_SHORT).show();
                }
//                else if (!AnyUtils.isValidPassword(et_password.getText().toString().trim())) {
//                    Toast.makeText(getApplicationContext(), "must be alpha numeric,must contain at least one symbol", Toast.LENGTH_SHORT).show();
//                }
             /*   else {
                    postLogin();
                }*/
                else if (edCaptcha.getText().toString().length()==0) {
                  Toast.makeText(getApplicationContext(),"Please enter Captcha",Toast.LENGTH_LONG).show();
                    requestFocus(edCaptcha);

                }else if(!edCaptcha.getText().toString().trim().equalsIgnoreCase(txtCaptcha.getText().toString().trim())){
                  Toast.makeText(getApplicationContext(),"Please enter correct captcha",Toast.LENGTH_LONG).show();
                    requestFocus(edCaptcha);

                } else {
                    postLogin();
                }
            }
        });
        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), Register_Screen.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });
        tv_forgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), Forgot_Screen.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });
        tv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), Login_Screen.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
            }
        });
        et_mail.setText("anoop@ecovesolutions.com");
        et_password.setText("1234567");
    }
    public void postLogin(){
        if (!AnyUtils.isNetworkAvailable(this)) {
            AnyUtils.showToast(getResources().getString(R.string.NoInternet), this);
            return;
        }
        AnyUtils.startDialog(activity);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.LOGIN+"pro");
        map.put(Constants.Params.EMAIL, et_mail.getText().toString());
        map.put("pass", et_password.getText().toString());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,1, this, this));
    }
    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        if(!activity.isFinishing()){
            AnyUtils.showserver_popup(activity);
        }
    }
    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(activity);
        switch (serviceCode) {
            case 1:
                try {
                    JSONObject jObj = new JSONObject(response);
                    Constants.status = jObj.getString(Constants.Params.STATUS);
                    Constants.message = jObj.getString(Constants.Params.MESSAGE);
                    if (Constants.status.matches("true")) {
                        JSONObject data = jObj.getJSONObject("data");
                        pHelper.putUserId(data.getString("userid"));
                        pHelper.putEmail(data.getString("email"));
                        pHelper.putName(data.getString("username"));
                        pHelper.putMobile_No(data.getString("phone"));
                        pHelper.putPassword(data.getString("password"));
                        pHelper.putBitcoin(data.getString(Constants.Params.BITCOIN));
                        pHelper.putProfileUrl(data.getString("profile_pic"));
                        pHelper.putCountry(data.getString("leva"));
                        pHelper.putLogin_type("0");

                        pHelper.putLogin_Status("earnassured");
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

                    } else {
                        if (!activity.isFinishing()) {
                            AnyUtils.showserver_popup(Constants.message, activity);
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }
    void hideKeyboard(View view){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public String generateAuthCode() {
        int authCode = (int) (Math.random() * 9000) + 1000;
        return String.valueOf(authCode);
    }
    @Override
    protected void onResume() {
        super.onResume();
        txtCaptcha.setText(generateAuthCode());
       // setViews();


        txtCaptcha.setText(txtCaptcha.getText().toString().trim());

    }





    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(getApplicationContext(), Login_Screen.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }
}
