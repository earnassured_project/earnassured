package com.ecovesolutions.earnassured;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.mukesh.countrypicker.fragments.CountryPicker;
import com.mukesh.countrypicker.interfaces.CountryPickerListener;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.models.User;
import io.fabric.sdk.android.Fabric;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;

import io.fabric.sdk.android.Fabric;
import retrofit2.Call;

import static android.R.id.progress;

public class Register_Screen extends AppCompatActivity implements AsyncTaskCompleteListener,Response.ErrorListener {

    Activity activity;
    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;
    Button btn_register_view,btn_login,btn_check,btn_fb,btn_twitter;
    EditText et_mail,et_user_name,et_password,et_c_password,et_first_name,et_last_name,et_phone,et_bitcoin ,et_referer,edCaptcha;
    TextView tv_county,tv_registerheading , txtCaptcha;
    private CountryPicker mCountryPicker;
    private CallbackManager callbackManager;
    private String f_profile_pic="",f_email="",f_name="",f_first_name="",f_last_name="",f_country="",f_city="",code="";
    private String t_id,t_email, t_name, t_full_name, t_profile_image,t_address;
    ScrollView main;
    Typeface gotham_light,gotham_book;

    private TwitterAuthClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register__screen);
        activity=this;
        if (!Fabric.isInitialized()) {
            TwitterAuthConfig authConfig = new TwitterAuthConfig("WdlzA6omGh2hLq4QwU7q8vl9x", "JN8ZMXL5WjfaPPZLx6NudVc1xmtOOJTL3mGv7hx60g6MNnI9RK");
            Fabric.with(this, new Crashlytics(),new Twitter(authConfig));
            //        Fabric.with(this, new Crashlytics());

        }
        pHelper = new PreferenceHelper(this);
        requestQueue = Volley.newRequestQueue(this);
        gotham_light= Typeface.createFromAsset(this.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(this.getAssets(), "fonts/gotham_book.otf");

        client = new TwitterAuthClient();

        try {
            FacebookSdk.sdkInitialize(getApplicationContext());
            callbackManager=CallbackManager.Factory.create();
        } catch (Exception e) {
            // TODO: handle exception
//				Log.e("ttt", ""+Log.getStackTraceString(e));
        }

        btn_register_view=(Button)findViewById(R.id.btn_register_view);
        btn_login=(Button)findViewById(R.id.btn_login);
        btn_check=(Button)findViewById(R.id.btn_check);
        btn_fb=(Button)findViewById(R.id.btn_fb);
        btn_twitter=(Button)findViewById(R.id.btn_twitter);
        edCaptcha= (EditText)findViewById(R.id.edCaptcha);
        txtCaptcha = (TextView)findViewById(R.id.txtCaptcha);

        et_mail=(EditText)findViewById(R.id.et_email);
        et_user_name=(EditText)findViewById(R.id.et_user_name);
        et_password=(EditText)findViewById(R.id.et_password);
        et_c_password=(EditText)findViewById(R.id.et_c_password);
        et_first_name=(EditText)findViewById(R.id.et_first_name);
        et_last_name=(EditText)findViewById(R.id.et_last_name);
        et_phone=(EditText)findViewById(R.id.et_phone);
        et_bitcoin=(EditText)findViewById(R.id.et_bitcoin);
        et_referer=(EditText)findViewById(R.id.et_referer);
        tv_county=(TextView)findViewById(R.id.tv_county);
        tv_registerheading=(TextView)findViewById(R.id.tv_registerheading);

        tv_county.setTypeface(gotham_book);
        tv_registerheading.setTypeface(gotham_book);
        et_mail.setTypeface(gotham_light);
        et_user_name.setTypeface(gotham_light);
        et_password.setTypeface(gotham_light);
        et_c_password.setTypeface(gotham_light);
        et_first_name.setTypeface(gotham_light);
        et_last_name.setTypeface(gotham_light);
        et_phone.setTypeface(gotham_light);
        et_bitcoin.setTypeface(gotham_light);
        btn_fb.setTypeface(gotham_light);
        btn_twitter.setTypeface(gotham_light);
        btn_register_view.setTypeface(gotham_light);
        btn_login.setTypeface(gotham_light);
        btn_check.setTypeface(gotham_light);
        et_referer.setTypeface(gotham_light);

        main=(ScrollView)findViewById(R.id.main);
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult result) {
                // TODO Auto-generated method stub
                AccessToken access_token=result.getAccessToken();
                GraphRequest request=GraphRequest.newMeRequest(access_token, new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject json, GraphResponse response) {
                        // TODO Auto-generated method stub
                        f_profile_pic="";
                        f_email="";
                        f_name="";
                        f_first_name="";
                        f_last_name="";
                        f_country="";
                        f_city="";
                        Log.d("ttt", "fbresponse="+response);
                        if (response.getError()!=null) {
                            Toast.makeText(getApplicationContext(), "error="+response.getError(), Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                AnyUtils.startDialog(activity);
                                Profile profile= Profile.getCurrentProfile();
                                URL profile_pic =null;

                                if (profile!=null) {
                                    String new_id=profile.getId();
                                    try {
                                        profile_pic = new URL("https://graph.facebook.com/" + new_id + "/picture?type=large");

                                    } catch (MalformedURLException e) {
                                        e.printStackTrace();
                                    }
                                }
                                f_email = json.optString("email");
                                f_name = json.getString("name");
                                f_first_name = json.getString("first_name");
                                f_last_name = json.getString("last_name");
                                if (profile_pic!=null) {
                                    f_profile_pic = profile_pic.toString();
                                }
                                Log.d("ttt","image="+profile_pic);
                                f_country =json.getJSONObject("location").getJSONObject("location").getString("country");
                                f_city =json.getJSONObject("location").getJSONObject("location").getString("city");




                                loginWithFB(f_email,f_name,f_profile_pic,f_first_name,f_last_name,f_city,f_country);
                            } catch (Exception e) {
                                // TODO: handle exception
                                loginWithFB(f_email,f_name,f_profile_pic,f_first_name,f_last_name,f_city,f_country);
                                Log.e("ttt", ""+Log.getStackTraceString(e));
//                                AnyUtils.stopDialog(activity);
                            }
                        }
                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields","id,name,email,gender,first_name,last_name,location{location}");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // TODO Auto-generated method stub
                Log.e("ttt", "onCancel");
                Toast.makeText(getApplicationContext(), "cancelled", Toast.LENGTH_SHORT).show();
                AnyUtils.stopDialog(activity);
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onError(FacebookException error) {
                // TODO Auto-generated method stub
                AnyUtils.stopDialog(activity);
                Log.e("ttt", "asdsd="+error);
                Toast.makeText(getApplicationContext(), ""+error, Toast.LENGTH_SHORT).show();
                if (error instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                    }
                }

            }
        });

        btn_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((et_user_name.getText().toString().length()==0)) {
                    Toast.makeText(getApplicationContext(), "Please enter Username", Toast.LENGTH_SHORT).show();
                }else if (et_user_name.getText().toString().length()<3) {
                    Toast.makeText(getApplicationContext(), "Password Minimum 3 Characters", Toast.LENGTH_SHORT).show();
                }else{
                    postCheckName();
                }
            }
        });
        btn_register_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((et_mail.getText().toString().length()==0)) {
                    Toast.makeText(getApplicationContext(), "Please enter Email ID", Toast.LENGTH_SHORT).show();
                }else if (!AnyUtils.eMailValidation(et_mail.getText().toString().trim())) {
                    Toast.makeText(getApplicationContext(), "Please enter valid Email ID", Toast.LENGTH_SHORT).show();
                }else if (et_user_name.getText().toString().length()<3) {
                    Toast.makeText(getApplicationContext(), "Password Minimum 3 Characters", Toast.LENGTH_SHORT).show();
                }else if (et_password.getText().toString().length()==0) {
                    Toast.makeText(getApplicationContext(), "Please enter Password", Toast.LENGTH_SHORT).show();
                }else if (et_password.getText().toString().length()<6) {
                    Toast.makeText(getApplicationContext(), "Password Minimum 8 Characters", Toast.LENGTH_SHORT).show();
                }
//                else if (!AnyUtils.isValidPassword(et_password.getText().toString().trim())) {
//                    Toast.makeText(getApplicationContext(), "must be alpha numeric,must contain at least one symbol", Toast.LENGTH_SHORT).show();
//                }
                else if (et_c_password.getText().toString().length()==0) {
                    Toast.makeText(getApplicationContext(), "Please enter Confirm Password", Toast.LENGTH_SHORT).show();
                }else if (!et_password.getText().toString().matches(et_c_password.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "Please enter Password Not Matching", Toast.LENGTH_SHORT).show();
                }else if (et_first_name.getText().toString().length()==0) {
                    Toast.makeText(getApplicationContext(), "Please enter First Name", Toast.LENGTH_SHORT).show();
                }else if (et_last_name.getText().toString().length()==0) {
                    Toast.makeText(getApplicationContext(), "Please enter Last Name", Toast.LENGTH_SHORT).show();
                }else if (et_phone.getText().toString().length()==0) {
                    Toast.makeText(getApplicationContext(), "Please enter Phone Number", Toast.LENGTH_SHORT).show();
                }else if (et_phone.getText().toString().length()<7) {
                    Toast.makeText(getApplicationContext(), "Please enter the valid Phone Number", Toast.LENGTH_SHORT).show();
                }else if (tv_county.getText().toString().matches("Select Country")) {
                    Toast.makeText(getApplicationContext(), "Please select Country", Toast.LENGTH_SHORT).show();
                }else if (et_bitcoin.getText().toString().length()==0) {
                    Toast.makeText(getApplicationContext(), "Please enter BitCoin", Toast.LENGTH_SHORT).show();
                }else if (edCaptcha.getText().toString().length()==0 ) {
                    Toast.makeText(getApplicationContext(), "Please enter captcha", Toast.LENGTH_LONG).show();
                }else if(!edCaptcha.getText().toString().trim().equalsIgnoreCase(txtCaptcha.getText().toString().trim())){
                    Toast.makeText(getApplicationContext(),"Please enter correct captcha",Toast.LENGTH_LONG).show();
                    requestFocus(edCaptcha);

                } else {
                    postRegister();
                }
            }
        });
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), Login_view.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });
        btn_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AnyUtils.isNetworkAvailable(activity)) {
                    LoginManager.getInstance().logInWithReadPermissions(Register_Screen.this, Arrays.asList("email","public_profile","user_location"));

                } else {
                    final Snackbar sn=Snackbar.make(main, ""+getResources().getString(R.string.NoInternet), Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                        }
                    });
                    sn.setActionTextColor(getResources().getColor(R.color.google));
                    sn.show();
                }
            }
        });
        btn_twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (AnyUtils.isNetworkAvailable(activity)) {
                    AnyUtils.startDialog(activity);
                    client.authorize(Register_Screen.this, new Callback<TwitterSession>() {
                        @Override
                        public void success(Result<TwitterSession> result) {
                            t_name = result.data.getUserName();
                            t_id = result.data.getUserId() + "";
                            Call<User> call = TwitterCore.getInstance().getApiClient().getAccountService().verifyCredentials(true, false);
                            call.enqueue(new Callback<User>() {
                                @Override
                                public void success(Result<User> userResult) {
                                    //If it succeeds creating a User object from userResult.data
                                    t_full_name = userResult.data.name;
                                    t_profile_image = userResult.data.profileImageUrl.replace("_normal", "");;
                                    t_email = userResult.data.email;
                                    t_address=userResult.data.location;
                                    if(t_email==null){
                                        t_email="";
                                    }
                                    if(t_address==null){
                                        t_address="";
                                    }
                                    loginWithTW(t_full_name,t_profile_image,t_email,t_address);
                                }
                                @Override
                                public void failure(TwitterException e) {
                                    AnyUtils.stopDialog(activity);
                                    Toast.makeText(Register_Screen.this,getResources().getString(R.string.somwthing_went),Toast.LENGTH_SHORT).show();
                                }
                            });

                        }

                        @Override
                        public void failure(TwitterException e) {
                            AnyUtils.stopDialog(activity);
                            Toast.makeText(Register_Screen.this,getResources().getString(R.string.somwthing_went),Toast.LENGTH_SHORT).show();

                        }
                    });

                } else {
                    final Snackbar sn=Snackbar.make(main, ""+getResources().getString(R.string.NoInternet), Snackbar.LENGTH_LONG).setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // TODO Auto-generated method stub
                        }
                    });
                    sn.setActionTextColor(getResources().getColor(R.color.google));
                    sn.show();
                }
            }
        });
        initialize();
    }
    private void initialize() {
        mCountryPicker = CountryPicker.newInstance("Select Country");
        mCountryPicker.setListener(new CountryPickerListener() {
            @Override public void onSelectCountry(String name, String code, String dialCode,int flagDrawableResID) {
                Log.d("ttt","name"+name);
                tv_county.setText(name);
                code=dialCode;
                mCountryPicker.dismiss();
            }
        });
        tv_county.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mCountryPicker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });
    }
    public void loginWithTW(String name,String image,String email,String address){
        if (!AnyUtils.isNetworkAvailable(this)) {
            AnyUtils.showToast(getResources().getString(R.string.NoInternet), this);
            return;
        }
            HashMap<String, String> map = new HashMap<String, String>();
            map.put(Constants.Params.URL, Constants.ServiceType.LOGIN);
            map.put(Constants.Params.ACTION,"twitterregister");
            map.put(Constants.Params.EMAIL, email);
            map.put(Constants.Params.USER_NAME, name);
            map.put(Constants.Params.PROFILE_PIC, image);
            map.put(Constants.Params.BITCOIN, address);
            map.put(Constants.Params.LAST_NAME, "");
            map.put(Constants.Params.FIRST_NAME, "");
            map.put("leva", address);
            requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,4, this, this));

        }

    public void loginWithFB(String email,String name,String image,String fname,String lname,String city,String country){
        if (!AnyUtils.isNetworkAvailable(this)) {
            AnyUtils.showToast(getResources().getString(R.string.NoInternet), this);
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.LOGIN);
        map.put(Constants.Params.ACTION,"facebookregister");
        map.put(Constants.Params.EMAIL, email);
        map.put(Constants.Params.USER_NAME, name);
        map.put(Constants.Params.FIRST_NAME, fname);
        map.put(Constants.Params.LAST_NAME, lname);
        map.put(Constants.Params.BITCOIN, city);
        map.put(Constants.Params.PROFILE_PIC, image);
        map.put("leva", country);
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,3, this, this));

    }
    public void postRegister(){
        if (!AnyUtils.isNetworkAvailable(this)) {
            AnyUtils.showToast(getResources().getString(R.string.NoInternet), this);
            return;
        }
        AnyUtils.startDialog(activity);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.REGISTER);
        map.put("email", et_mail.getText().toString());
        map.put("username", et_user_name.getText().toString());
        map.put("password", et_password.getText().toString());
        map.put("password2", et_c_password.getText().toString());
        map.put("first_name", et_first_name.getText().toString());
        map.put("last_name", et_last_name.getText().toString());
        map.put("bitcoin", et_bitcoin.getText().toString());
        map.put("phone", code+et_phone.getText().toString());
        map.put("sponser", tv_county.getText().toString());
        map.put("referer", et_referer.getText().toString());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,2, this, this));
    }
    public void postCheckName(){
        if (!AnyUtils.isNetworkAvailable(this)) {
            AnyUtils.showToast(getResources().getString(R.string.NoInternet), this);
            return;
        }
        AnyUtils.startDialog(activity);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.LOGIN);
        map.put(Constants.Params.USER_NAME, et_user_name.getText().toString());
        map.put(Constants.Params.ACTION, "ckeckusername");
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,1, this, this));
    }
    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        if(!activity.isFinishing()){
            AnyUtils.showserver_popup(activity);
        }
    }
    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(activity);
        switch (serviceCode) {
            case 1:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){
                        Toast.makeText(Register_Screen.this,Constants.message,Toast.LENGTH_SHORT).show();
                    }else{
                        if(!activity.isFinishing()){
                            AnyUtils.showserver_popup(Constants.message,activity)	;
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            //signup
            case 2:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){
                        JSONObject data =jObj.getJSONObject("data");
//                        pHelper.putUserId(data.getString(Constants.Params.ID));
//                        pHelper.putEmail(data.getString(Constants.Params.EMAIL));
//                        pHelper.putName(data.getString(Constants.Params.USER_NAME));
//                        pHelper.putMobile_No(data.getString(Constants.Params.MOBILE));
//                        pHelper.putPassword(data.getString(Constants.Params.PASSWORD));
//                        pHelper.putBitcoin(data.getString(Constants.Params.BITCOIN));

                        Toast.makeText(Register_Screen.this,data.getString("success"),Toast.LENGTH_SHORT).show();

                        Intent intent=new Intent(activity, Login_view.class);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

                    }else{
                        if(!activity.isFinishing()){
                            AnyUtils.showserver_popup(Constants.message,activity)	;
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            //facebook
            case 3:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){

                        JSONObject data =jObj.getJSONObject("data");
                        pHelper.putUserId(data.getString(Constants.Params.ID));
                        pHelper.putEmail(data.getString(Constants.Params.EMAIL));
                        pHelper.putName(data.getString(Constants.Params.USER_NAME));
                        pHelper.putProfileUrl(data.getString(Constants.Params.PROFILE_PIC));
                        pHelper.putBitcoin(data.getString(Constants.Params.BITCOIN));
                        pHelper.putLogin_type("1");

                        pHelper.putLogin_Status("earnassured");
                        Intent intent=new Intent(activity, MainActivity.class);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

                    }else{
                        if(!activity.isFinishing()){
                            AnyUtils.showserver_popup(Constants.message,activity)	;
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            //twitter
            case 4:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){

                        JSONObject data =jObj.getJSONObject("data");
                        pHelper.putUserId(data.getString(Constants.Params.ID));
                        pHelper.putEmail(data.getString(Constants.Params.EMAIL));
                        pHelper.putName(data.getString(Constants.Params.USER_NAME));
                        pHelper.putProfileUrl(data.getString(Constants.Params.PROFILE_PIC));
                        pHelper.putBitcoin(data.getString(Constants.Params.BITCOIN));
                        pHelper.putLogin_type("1");

                        pHelper.putLogin_Status("earnassured");
                        Intent intent=new Intent(activity, MainActivity.class);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

                    }else{
                        if(!activity.isFinishing()){
                            AnyUtils.showserver_popup(Constants.message,activity)	;
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }

    private void requestFocus(View view){
        if(view.requestFocus()){
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    void hideKeyboard(View view){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }



    public String generateAuthCode() {
        int authCode = (int) (Math.random() * 9000) + 1000;
        return String.valueOf(authCode);
    }
    @Override
    protected void onResume() {
        super.onResume();
        txtCaptcha.setText(generateAuthCode());
        // setViews();


        txtCaptcha.setText(txtCaptcha.getText().toString().trim());

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(getApplicationContext(), Login_Screen.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE){
            client.onActivityResult(requestCode,resultCode,data);
        }else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

}
