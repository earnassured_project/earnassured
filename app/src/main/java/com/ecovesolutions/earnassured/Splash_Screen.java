package com.ecovesolutions.earnassured;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.ecovesolutions.utilities.PreferenceHelper;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import io.fabric.sdk.android.Fabric;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Splash_Screen extends AppCompatActivity {


    public static PreferenceHelper pHelper;
    public static Activity activity;
   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!Fabric.isInitialized()) {
            TwitterAuthConfig authConfig = new TwitterAuthConfig("WdlzA6omGh2hLq4QwU7q8vl9x", "JN8ZMXL5WjfaPPZLx6NudVc1xmtOOJTL3mGv7hx60g6MNnI9RK");
            Fabric.with(this, new Crashlytics(),new Twitter(authConfig));
            //        Fabric.with(this, new Crashlytics());

        }

        setContentView(R.layout.activity_splash__screen);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        activity=this;
        pHelper = new PreferenceHelper(this);


        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.ecovesolutions.earnassured",PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        new GetInfoAll().execute();
    }
    public void forceCrash(View view) {
        throw new RuntimeException("This is a crash");
    }

    class GetInfoAll extends AsyncTask<Void, Void, Void>
    {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
        }
        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);

            String login_id=pHelper.getLogin_Status();
            if(login_id != null && !login_id.isEmpty()){
                if (login_id.matches("earnassured")) {
                    Intent intent=new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                }else{
                    Intent intent=new Intent(getApplicationContext(), Login_Screen.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                }
            }else{
                Intent intent=new Intent(getApplicationContext(), Login_Screen.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        }

    }
}
