package com.ecovesolutions.earnassured;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.ecovesolutions.earnassured.R.id.et_email;

public class Forgot_Screen extends AppCompatActivity implements AsyncTaskCompleteListener,Response.ErrorListener{
    Activity activity;
    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;
    TextView tv_goback,tv_forgetheading,tv_discription;
    Button btn_send;
    EditText et_forgot;
    Typeface gotham_light,gotham_book;
    String strname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot__screen); activity=this;
        pHelper = new PreferenceHelper(this);
        requestQueue = Volley.newRequestQueue(this);

        tv_goback=(TextView)findViewById(R.id.tv_goback);
        tv_forgetheading=(TextView)findViewById(R.id.tv_forgetheading);

        btn_send=(Button)findViewById(R.id.btn_send);
        et_forgot=(EditText)findViewById(R.id.et_forgot);
        tv_discription=(TextView)findViewById(R.id.tv_discription);

// font style
        gotham_light= Typeface.createFromAsset(this.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(this.getAssets(), "fonts/gotham_book.otf");
        tv_goback.setTypeface(gotham_book);
        tv_forgetheading.setTypeface(gotham_book);
        et_forgot.setTypeface(gotham_light);
        btn_send.setTypeface(gotham_light);
        tv_discription.setTypeface(gotham_book);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_forgot.getText().toString().length()==0){
                    Toast.makeText(getApplicationContext(), "Please enter  Email ID", Toast.LENGTH_SHORT).show();
                }else if (!AnyUtils.eMailValidation(et_forgot.getText().toString().trim())) {
                    Toast.makeText(getApplicationContext(), "Please enter valid Email ID", Toast.LENGTH_SHORT).show();
                }else{
                    postForgot();

                }

            }
        });

        tv_goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), Login_view.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
            }
        });
    }
    public void postForgot(){
        if (!AnyUtils.isNetworkAvailable(this)) {
            AnyUtils.showToast(getResources().getString(R.string.NoInternet), this);
            return;
        }
        AnyUtils.startDialog(activity);
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.LOGIN+"forgotpw_pro");
        map.put(Constants.Params.EMAIL, et_forgot.getText().toString());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,1, this, this));
    }
    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        if(!activity.isFinishing()){
            AnyUtils.showserver_popup(activity);
        }
    }
    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(activity);
        switch (serviceCode) {
            case 1:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){
                        Toast.makeText(getApplicationContext(), Constants.message, Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(getApplicationContext(), Login_view.class);
                        startActivity(intent);
                        finish();
                        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                    }else{
                        if(!activity.isFinishing()){
                            AnyUtils.showserver_popup(Constants.message,activity)	;
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(getApplicationContext(), Login_view.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }
}
