package com.ecovesolutions.model;

/**
 * Created by Admin on 4/17/2017.
 */

public class Allpayment_Model {
    String ph_id;
    String match_date;
    String amount_off;
    String payment_confirm;
    String username;
    String bitcoin;

    public Allpayment_Model( String ph_id, String match_date, String amount_off, String payment_confirm, String username, String bitcoin) {
        // TODO Auto-generated constructor stub
        this.ph_id=ph_id;
        this.match_date=match_date;
        this.amount_off=amount_off;
        this.payment_confirm=payment_confirm;
        this.username=username;
        this.bitcoin=bitcoin;


    }
    public String getPh_id() {
        return ph_id;
    }
    public void setPh_id(String ph_id) {
        this.ph_id = ph_id;
    }

    public String getMatch_date() {
        return match_date;
    }

    public void setMatch_date(String match_date) {
        this.match_date = match_date;
    }

    public String getAmount_off() {
        return amount_off;
    }

    public void setAmount_off(String amount_off) {
        this.amount_off = amount_off;
    }

    public String getPayment_confirm() {
        return payment_confirm;
    }

    public void setPayment_confirm(String payment_confirm) {
        this.payment_confirm = payment_confirm;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBitcoin() {
        return bitcoin;
    }

    public void setBitcoin(String bitcoin) {
        this.bitcoin = bitcoin;
    }



}
