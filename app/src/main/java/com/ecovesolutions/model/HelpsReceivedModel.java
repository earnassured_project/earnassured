package com.ecovesolutions.model;

/**
 * Created by Admin on 4/21/2017.
 */

public class HelpsReceivedModel {
    String id,name,amount,date;
    public HelpsReceivedModel(String id, String name,String amount,String date){
        this.id=id;
        this.name=name;
        this.amount=amount;
        this.date=date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
