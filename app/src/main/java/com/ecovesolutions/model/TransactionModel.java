package com.ecovesolutions.model;

/**
 * Created by Admin on 4/17/2017.
 */

public class TransactionModel {

    String id;
    String start_date;
    String amount_off;
    String amount_paid;
    String total_confirmed;
    String amountout;
    String cred_score;
    String status;

    public TransactionModel(String id, String start_date, String amount_off,String amount_paid, String total_confirmed,String amountout,String cred_score,String status) {
        // TODO Auto-generated constructor stub
        this.id=id;
        this.start_date=start_date;
        this.amount_off=amount_off;
        this.amount_paid=amount_paid;
        this.total_confirmed=total_confirmed;
        this.amountout=amountout;
        this.cred_score=cred_score;

        this.status=status;

    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getAmount_off() {
        return amount_off;
    }

    public void setAmount_off(String amount_off) {
        this.amount_off = amount_off;
    }

    public String getAmount_paid() {
        return amount_paid;
    }

    public void setAmount_paid(String amount_paid) {
        this.amount_paid = amount_paid;
    }

    public String getTotal_confirmed() {
        return total_confirmed;
    }

    public void setTotal_confirmed(String total_confirmed) {
        this.total_confirmed = total_confirmed;
    }

    public String getAmountout() {
        return amountout;
    }

    public void setAmountout(String amountout) {
        this.amountout = amountout;
    }

    public String getCred_score() {
        return cred_score;
    }

    public void setCred_score(String cred_score) {
        this.cred_score = cred_score;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

  /*  public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
*/


}
