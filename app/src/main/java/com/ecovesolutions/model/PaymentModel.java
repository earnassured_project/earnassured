package com.ecovesolutions.model;

/**
 * Created by Admin on 4/18/2017.
 */

public class PaymentModel {
    String username;
    String sno;
    String request_id;
    String expiry_date;
    String payment_confirm;
    String ph_id,profile_pic;
    public PaymentModel(String username, String sno, String request_id,String expiry_date,String payment_confirm,String ph_id,String profile_pic) {
        // TODO Auto-generated constructor amountout
        this.username=username;
        this.sno=sno;
        this.request_id=request_id;
        this.expiry_date=expiry_date;
        this.payment_confirm=payment_confirm;
        this.ph_id=ph_id;
        this.profile_pic=profile_pic;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getPayment_confirm() {
        return payment_confirm;
    }

    public void setPayment_confirm(String payment_confirm) {
        this.payment_confirm = payment_confirm;
    }

    public String getPh_id() {
        return ph_id;
    }

    public void setPh_id(String ph_id) {
        this.ph_id = ph_id;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }
}
