package com.ecovesolutions.model;

/**
 * Created by Admin on 4/14/2017.
 */

public class News_Model {
    String id,subject, message;
    public News_Model(String id,String subject, String message) {
        // TODO Auto-generated constructor stub
        this.id=id;
        this.subject=subject;
        this.message=message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
