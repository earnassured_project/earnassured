package com.ecovesolutions.model;

/**
 * Created by Admin on 4/19/2017.
 */

public class MakeRequestModel {
            String ph_id;
            String end_date;
            String amount_off;
            String yield_amount2;
            String bonus;
            String request;
            String status;
            String one_time_bonus;
public MakeRequestModel(String ph_id, String end_date, String amount_off,String yield_amount2,String bonus,String request,String status,String one_time_bonus) {
        // TODO Auto-generated constructor amountout
        this.ph_id=ph_id;
        this.end_date=end_date;
        this.amount_off=amount_off;
        this.yield_amount2=yield_amount2;
        this.bonus=bonus;
        this.request=request;
        this.status=status;
        this.one_time_bonus=one_time_bonus;
}

        public String getOne_time_bonus() {
                return one_time_bonus;
        }

        public void setOne_time_bonus(String one_time_bonus) {
                this.one_time_bonus = one_time_bonus;
        }

        public String getPh_id() {
                return ph_id;
        }

        public void setPh_id(String ph_id) {
                this.ph_id = ph_id;
        }

        public String getEnd_date() {
                return end_date;
        }

        public void setEnd_date(String end_date) {
                this.end_date = end_date;
        }

        public String getAmount_off() {
                return amount_off;
        }

        public void setAmount_off(String amount_off) {
                this.amount_off = amount_off;
        }

        public String getYield_amount2() {
                return yield_amount2;
        }

        public void setYield_amount2(String yield_amount2) {
                this.yield_amount2 = yield_amount2;
        }

        public String getBonus() {
                return bonus;
        }

        public void setBonus(String bonus) {
                this.bonus = bonus;
        }

        public String getRequest() {
                return request;
        }

        public void setRequest(String request) {
                this.request = request;
        }

        public String getStatus() {
                return status;
        }

        public void setStatus(String status) {
                this.status = status;
        }
}
