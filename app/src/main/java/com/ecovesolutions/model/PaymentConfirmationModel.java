package com.ecovesolutions.model;

/**
 * Created by Admin on 4/20/2017.
 */

public class PaymentConfirmationModel {

    String ph_id,match_date,expire_date,amount_off,username,type,view,action,status,profile_pic;
    public PaymentConfirmationModel(String ph_id, String match_date, String expire_date,String amount_off,String username,String type,String view,String action,String status,String profile_pic) {
        // TODO Auto-generated constructor amountout
        this.ph_id=ph_id;
        this.match_date=match_date;
        this.expire_date=expire_date;
        this.amount_off=amount_off;
        this.username=username;
        this.type=type;
        this.view=view;
        this.action=action;
        this.status=status;
        this.profile_pic=profile_pic;
    }

    public String getPh_id() {
        return ph_id;
    }

    public void setPh_id(String ph_id) {
        this.ph_id = ph_id;
    }

    public String getMatch_date() {
        return match_date;
    }

    public void setMatch_date(String match_date) {
        this.match_date = match_date;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }

    public String getAmount_off() {
        return amount_off;
    }

    public void setAmount_off(String amount_off) {
        this.amount_off = amount_off;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }
}
