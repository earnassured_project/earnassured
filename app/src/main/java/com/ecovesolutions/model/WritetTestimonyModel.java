package com.ecovesolutions.model;

/**
 * Created by Admin on 4/20/2017.
 */

public class WritetTestimonyModel {
    String id,username,message,amount,status;
    public WritetTestimonyModel(String id,String username,String message,String amount,String status) {
        // TODO Auto-generated constructor stub
        this.id=id;
        this.username=username;
        this.message=message;
        this.amount=amount;
        this.status=status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
