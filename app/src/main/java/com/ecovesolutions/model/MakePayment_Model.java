package com.ecovesolutions.model;

/**
 * Created by Admin on 4/17/2017.
 */

public class MakePayment_Model {
    String match_date,expire_date,amount_off,id,username,bankname,action,accountname,bitcoin,payment_confirm;
    public MakePayment_Model(String match_date, String expire_date, String amount_off, String id, String username, String bankname, String action, String accountname, String bitcoin, String payment_confirm) {
        this.match_date=match_date;
        this.expire_date=expire_date;
        this.amount_off=amount_off;
        this.id=id;
        this.username=username;
        this.bankname=bankname;
        this.action=action;
        this.accountname=accountname;
        this.bitcoin=bitcoin;
        this.payment_confirm=payment_confirm;
    }



    public String getMatch_date() {
        return match_date;
    }

    public void setMatch_date(String match_date) {
        this.match_date = match_date;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }

    public String getAmount_off() {
        return amount_off;
    }

    public void setAmount_off(String amount_off) {
        this.amount_off = amount_off;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String accountno) {
        this.action = accountno;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public String getBitcoin() {
        return bitcoin;
    }

    public void setBitcoin(String bitcoin) {
        this.bitcoin = bitcoin;
    }

    public String getPayment_confirm() {
        return payment_confirm;
    }

    public void setPayment_confirm(String payment_confirm) {
        this.payment_confirm = payment_confirm;
    }
}
