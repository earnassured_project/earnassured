package com.ecovesolutions.model;
/**
 * @author Satheeshkumar
 **/

public class ReferralBonus_Model {

    String sno, member, donation_amount,mybonus,status,date;
    public ReferralBonus_Model(String sno, String member, String donation_amount,String mybonus,String status,String date) {
        // TODO Auto-generated constructor stub
        this.sno=sno;
        this.member=member;
        this.donation_amount=donation_amount;
        this.mybonus=mybonus;
        this.status=status;
        this.date=date;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public String getDonation_amount() {
        return donation_amount;
    }

    public void setDonation_amount(String donation_amount) {
        this.donation_amount = donation_amount;
    }

    public String getMybonus() {
        return mybonus;
    }

    public void setMybonus(String mybonus) {
        this.mybonus = mybonus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date =date;
    }
}
