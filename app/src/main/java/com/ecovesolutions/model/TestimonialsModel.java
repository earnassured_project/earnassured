package com.ecovesolutions.model;

/**
 * Created by Admin on 4/19/2017.
 */

public class TestimonialsModel {
    String id,name,image,message;
    public TestimonialsModel(String id, String name, String image,String message){
        this.id=id;
        this.name=name;
        this.image=image;
        this.message=message;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
