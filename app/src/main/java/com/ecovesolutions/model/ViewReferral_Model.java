package com.ecovesolutions.model;
/**
 * @author Satheeshkumar
**/

public class ViewReferral_Model {
	
	String id, name, email,phone,country,date;
	public ViewReferral_Model(String id, String name, String email,String phone,String country,String date) {
		// TODO Auto-generated constructor stub
		this.id=id;
		this.name=name;
		this.email=email;
		this.phone=phone;
		this.country=country;
		this.date=date;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date =date;
	}
}
