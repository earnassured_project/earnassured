package com.ecovesolutions.model;

/**
 * Created by Admin on 4/18/2017.
 */

public class Reqhistory_Model {


    String sno;
    String request_date;
    String amount_off;
    String amount_paid;
    String amountout;
    String payment_status;
    String ref_bonus;
    public Reqhistory_Model(String sno, String request_date, String amount_off,String amount_paid,String amountout,String payment_status,String ref_bonus) {


        // TODO Auto-generated constructor amountout
        this.sno=sno;
        this.request_date=request_date;
        this.amount_off=amount_off;
        this.amount_paid=amount_paid;
        this.amountout=amountout;
        this.payment_status=payment_status;
        this.ref_bonus=ref_bonus;
    }

    public String getRef_bonus() {
        return ref_bonus;
    }

    public void setRef_bonus(String ref_bonus) {
        this.ref_bonus = ref_bonus;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getRequest_date() {
        return request_date;
    }

    public void setRequest_date(String request_date) {
        this.request_date = request_date;
    }

    public String getAmount_off() {
        return amount_off;
    }

    public void setAmount_off(String amount_off) {
        this.amount_off = amount_off;
    }

    public String getAmount_paid() {
        return amount_paid;
    }

    public void setAmount_paid(String amount_paid) {
        this.amount_paid = amount_paid;
    }

    public String getAmountout() {
        return amountout;
    }

    public void setAmountout(String amountout) {
        this.amountout = amountout;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

}

