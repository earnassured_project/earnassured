package com.ecovesolutions.utilities;


import com.ecovesolutions.model.Allpayment_Model;
import com.ecovesolutions.model.HelpsOfferedModel;
import com.ecovesolutions.model.HelpsReceivedModel;
import com.ecovesolutions.model.LastPaymentModel;
import com.ecovesolutions.model.MakePayment_Model;
import com.ecovesolutions.model.MakeRequestModel;
import com.ecovesolutions.model.News_Model;
import com.ecovesolutions.model.PaymentConfirmationModel;
import com.ecovesolutions.model.PaymentModel;
import com.ecovesolutions.model.ReferralBonus_Model;
import com.ecovesolutions.model.Reqhistory_Model;
import com.ecovesolutions.model.TestimonialsModel;
import com.ecovesolutions.model.TransactionModel;
import com.ecovesolutions.model.ViewReferral_Model;
import com.ecovesolutions.model.WritetTestimonyModel;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * @author Satheeshkumar
**/
public class Constants {
	public static String URL_BEST= "http://bbomplusfranchising.com/earnassured/";
	public class ServiceType {
		private static final String BASE_URL = "http://demo.randgoverseas.com/";
		public static final String LOGIN = BASE_URL + "login_api/";
		public static final String REGISTER = BASE_URL + "register_api/index";
		public static final String MYACCOUNT = BASE_URL + "admin/account/account_api/";
		public static final String REFERRALS = BASE_URL + "referal/view_referal_api";
		public static final String NEWS = BASE_URL + "news_api/shownews";
		public static final String LOGOUT=BASE_URL +"logout.php?";
		public static final String GIVEHELP=BASE_URL +"provide/provide_api/";
		public static final String RECEIVEHELP=BASE_URL +"request/request_api/";
		public static final String DONATEEADMIN=BASE_URL +"donate_api/admin";
		public static final String DONATECHARITY=BASE_URL +"donate_api/charity";
		public static final String DASHBOARD=BASE_URL +"dashboard_api";
	}

	public class Params {
		public static final String URL = "url";
		public static final String STATUS="status";
		public static final String MESSAGE="message";
		public static final String CUSTOMER_ID="customer_id";
		public static final String USER_ID="user_id";
		public static final String USER_NAME="username";
		public static final String ACTION = "action";
		public static final String ID="id";
		public static final String LOGIN_STATUS = "login_id";
		public static final String LOGIN_TYPE = "login_type";
		public static final String EMAIL = "email";
		public static final String FIRST_NAME = "first_name";
		public static final String LAST_NAME = "last_name";
		public static final String PHONE = "phone";
		public static final String PASSWORD = "password";
		public static final String BITCOIN = "bitcoin";
		public static final String ORDER_REF_ID = "order_ref_id";
		public static final String MOBILE = "mobile";
		public static final String NAME = "name";
		public static final String IMAGE_URL="image_url";
		public static final String PROFILE_PIC="profile_pic";
		public static final String TIME="time";
		public static final String TYPE="type";
		public static final String DATA="data";
		public static final String DATE="date";
		public static final String SUBJECT="subject";
		public static final String COUNTRY="country";
	}


//	 prefname
	public static String PREF_NAME = "sundar";
	public static String status="";
	public static String message="Something went wrong,please retry.";


	public static ArrayList<HashMap<String, String>> arr_order=new ArrayList<HashMap<String,String>>();

	public static ArrayList<TestimonialsModel> testimonialsModels=new ArrayList<TestimonialsModel>();
	public static ArrayList<ReferralBonus_Model> referralbonus_models=new ArrayList<ReferralBonus_Model>();
	public static ArrayList<ViewReferral_Model> viewreferral_model=new ArrayList<ViewReferral_Model>();
	public static ArrayList<News_Model> news_models=new ArrayList<News_Model>();
	public static ArrayList<TransactionModel> transactionModelArrayList=new ArrayList<TransactionModel>();
	public static ArrayList<MakePayment_Model> makePayment_models=new ArrayList<MakePayment_Model>();
	public static ArrayList<Allpayment_Model> allpaymnetModelArrayList=new ArrayList<Allpayment_Model>();
	public static ArrayList<Reqhistory_Model> Reqhistory_ModelArrayList=new ArrayList<Reqhistory_Model>();
    public static ArrayList<PaymentModel> payhistory_ModelArrayList=new ArrayList<PaymentModel>();
	public static ArrayList<MakeRequestModel> makeRequestModels=new ArrayList<MakeRequestModel>();
	public static ArrayList<PaymentConfirmationModel> paymentConfirmationModels=new ArrayList<PaymentConfirmationModel>();
	public static ArrayList<WritetTestimonyModel> writetTestimonyModels=new ArrayList<WritetTestimonyModel>();
	public static ArrayList<HelpsOfferedModel> helpsOfferedModels=new ArrayList<HelpsOfferedModel>();
	public static ArrayList<HelpsReceivedModel> helpsReceivedModels=new ArrayList<HelpsReceivedModel>();
	public static ArrayList<LastPaymentModel> lastPaymentModels=new ArrayList<LastPaymentModel>();

}
