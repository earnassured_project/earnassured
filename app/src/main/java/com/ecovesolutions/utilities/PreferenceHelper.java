package com.ecovesolutions.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.ecovesolutions.utilities.Constants;

/**
 * @author Satheeshkumar
**/
public class
PreferenceHelper {
	private SharedPreferences preference;
	@SuppressWarnings("unused")
	private Context context;


	public PreferenceHelper(Context context) {
		preference = context.getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
		this.context = context;
	}
	public void putLogin_Status(String login_id) {
		Editor edit = preference.edit();
		edit.putString(Constants.Params.LOGIN_STATUS, login_id);
		edit.commit();
	}
	public String getLogin_Status() {
		return preference.getString(Constants.Params.LOGIN_STATUS, null);
	}


	public void putLogin_type(String userId) {
		Editor edit = preference.edit();
		edit.putString(Constants.Params.LOGIN_TYPE, userId);
		edit.commit();	}
	public String getLogin_type() {
		return preference.getString(Constants.Params.LOGIN_TYPE, null);
	}




	public void putUserId(String userId) {
		Editor edit = preference.edit();
		edit.putString(Constants.Params.USER_ID, userId);
		edit.commit();	}

	public String getUserId() {
		return preference.getString(Constants.Params.USER_ID, null);

	}

	public void putEmail(String email) {
		Editor edit = preference.edit();
		edit.putString(Constants.Params.EMAIL, email);
		edit.commit();
	}
	public String getEmail() {
		return preference.getString(Constants.Params.EMAIL, null);
	}


	public void putMobile_No(String mobile_no) {
		Editor edit = preference.edit();
		edit.putString(Constants.Params.MOBILE, mobile_no);
		edit.commit();
	}
	public String getMobile_No() {
		return preference.getString(Constants.Params.MOBILE, null);
	}


	public void putPassword(String password) {
		Editor edit = preference.edit();
		edit.putString(Constants.Params.PASSWORD, password);
		edit.commit();
	}
	public String getPassword() {
		return preference.getString(Constants.Params.PASSWORD, null);
	}


	public void putName(String name) {
		Editor edit = preference.edit();
		edit.remove(Constants.Params.NAME).commit();
		edit.putString(Constants.Params.NAME, name);
		edit.commit();
	}
	public String getName() {
		return preference.getString(Constants.Params.NAME, null);
	}


	public void putProfileUrl(String profileUrl) {
		Editor edit = preference.edit();
		edit.remove(Constants.Params.IMAGE_URL).commit();
		edit.putString(Constants.Params.IMAGE_URL, profileUrl);
		edit.commit();
	}
	public String getProfileUrl() {
		return preference.getString(Constants.Params.IMAGE_URL, null);
	}


	public void putBitcoin(String profileUrl) {
		Editor edit = preference.edit();
		edit.remove(Constants.Params.BITCOIN).commit();
		edit.putString(Constants.Params.BITCOIN, profileUrl);
		edit.commit();
	}
	public String getBitcoin() {
		return preference.getString(Constants.Params.BITCOIN, null);
	}


	public void putCountry(String country) {
		Editor edit = preference.edit();
		edit.remove(Constants.Params.COUNTRY).commit();
		edit.putString(Constants.Params.COUNTRY, country);
		edit.commit();
	}
	public String getCountry() {
		return preference.getString(Constants.Params.COUNTRY, null);
	}



	public void Logout() {
		putUserId(null);
		preference.edit().clear().commit();
	}

}
