package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Admin on 4/10/2017.
 */

public class Change_Password extends Fragment implements AsyncTaskCompleteListener,Response.ErrorListener {
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    EditText et_opassword, et_npassword, et_cpassword;
    Button btn_update;
    TextView tv_reset;
    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;
    Typeface gotham_light,gotham_book;
    public static Change_Password newInstance() {
        Change_Password mapFragment = new Change_Password();
        return mapFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.change_password, container, false);
        pHelper = new PreferenceHelper(getActivity());
        requestQueue = Volley.newRequestQueue(getActivity());
        gotham_light= Typeface.createFromAsset(context.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(context.getAssets(), "fonts/gotham_book.otf");
        tv_reset=(TextView) view.findViewById(R.id.tv_reset);
        et_opassword = (EditText) view.findViewById(R.id.et_opassword);
        et_npassword = (EditText) view.findViewById(R.id.et_npassword);
        et_cpassword = (EditText) view.findViewById(R.id.et_cpassword);
        btn_update = (Button) view.findViewById(R.id.btn_update);
        tv_reset.setTypeface(gotham_book);
        et_opassword.setTypeface(gotham_light);
        et_npassword.setTypeface(gotham_light);
        et_cpassword.setTypeface(gotham_light);
        btn_update.setTypeface(gotham_light);
        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_opassword.getText().toString().length() == 0) {
                    Toast.makeText(getContext(), "Please enter Old Password Not Matching", Toast.LENGTH_SHORT).show();
                } else if (!et_opassword.getText().toString().matches(pHelper.getPassword())) {
                    Toast.makeText(getContext(), "Please enter Valid Password", Toast.LENGTH_SHORT).show();
                } else if (et_npassword.getText().toString().length() == 0) {
                    Toast.makeText(getContext(), "Please enter new Password", Toast.LENGTH_SHORT).show();
                }
//                else if (et_npassword.getText().toString().length() < 8) {
//                    Toast.makeText(getContext(), "Password Minimum 8 Characters", Toast.LENGTH_SHORT).show();
//                } else if (!AnyUtils.isValidPassword(et_opassword.getText().toString().trim())) {
//                    Toast.makeText(getContext(), "must be alpha numeric,must contain at least one symbol", Toast.LENGTH_SHORT).show();
//                }
                else if (et_opassword.getText().toString().matches(et_npassword.getText().toString())) {
                    Toast.makeText(getContext(), "Please enter Old and New Password Same", Toast.LENGTH_SHORT).show();
                } else if (et_cpassword.getText().toString().length() == 0) {
                    Toast.makeText(getContext(), "Please enter Confirm Password", Toast.LENGTH_SHORT).show();
                } else if (!et_npassword.getText().toString().matches(et_cpassword.getText().toString())) {
                    Toast.makeText(getContext(), "Please enter Password Not Matching", Toast.LENGTH_SHORT).show();
                } else {
                    changePassword();
                }
            }
        });
        return view;
    }

    public void changePassword() {
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(getResources().getString(R.string.NoInternet), getActivity());
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.MYACCOUNT+"passwordchangeconfirm");
        map.put("current_password", et_opassword.getText().toString());
        map.put(" new_pass1", et_npassword.getText().toString());
        map.put(" new_pass2", et_cpassword.getText().toString());
        map.put(Constants.Params.USER_ID, pHelper.getUserId());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map, 1, this, this));
    }

    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        if (!getActivity().isFinishing()) {
            AnyUtils.showserver_popup(getActivity());
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        switch (serviceCode) {
            case 1:
                switch (serviceCode) {
                    case 1:
                        try {
                            JSONObject jObj = new JSONObject(response);
                            Constants.status = jObj.getString(Constants.Params.STATUS);
                            Constants.message = jObj.getString(Constants.Params.MESSAGE);
                            if (Constants.status.matches("true")) {
                                pHelper.putPassword(et_npassword.getText().toString());
                                Toast.makeText(getContext(), Constants.message, Toast.LENGTH_SHORT).show();
                                et_opassword.setText("");
                                et_npassword.setText("");
                                et_cpassword.setText("");
                            } else {
                                if (!getActivity().isFinishing()) {
                                    AnyUtils.showserver_popup(Constants.message, getActivity());
                                }
                            }

                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        break;

                    default:
                        break;

                }
        }
    }
}
