package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ecovesolutions.earnassured.R;

/**
 * Created by Admin on 4/11/2017.
 */

public class Contact_Us extends Fragment {
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    Typeface gotham_light,gotham_book;
    TextView tv_name,tv_email,tv_phone,tv_message;
    EditText et_name,et_email,et_phone,et_message;
    Button btn_submit;
    public static Contact_Us newInstance() {
        Contact_Us mapFragment = new Contact_Us();
        return mapFragment;
    }
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.contact_us, container, false);
        gotham_light= Typeface.createFromAsset(context.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(context.getAssets(), "fonts/gotham_book.otf");
        tv_name=(TextView)view.findViewById(R.id.tv_name);
        tv_email=(TextView)view.findViewById(R.id.tv_email);
        tv_phone=(TextView)view.findViewById(R.id.tv_phone);
        tv_message=(TextView)view.findViewById(R.id.tv_message);

        et_name=(EditText)view.findViewById(R.id.et_name);
        et_email=(EditText)view.findViewById(R.id.et_email);
        et_phone=(EditText)view.findViewById(R.id.et_phone);
        et_message=(EditText)view.findViewById(R.id.et_message);

        btn_submit=(Button)view.findViewById(R.id.btn_submit);

        tv_name.setTypeface(gotham_book);
        tv_email.setTypeface(gotham_book);
        tv_phone.setTypeface(gotham_book);
        tv_message.setTypeface(gotham_book);

        et_name.setTypeface(gotham_light);
        et_email.setTypeface(gotham_light);
        et_phone.setTypeface(gotham_light);
        et_message.setTypeface(gotham_light);
        btn_submit.setTypeface(gotham_light);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_name.getText().toString().length()==0){
                    Toast.makeText(getContext(), "Please enter Name", Toast.LENGTH_SHORT).show();
                }else if(et_phone.getText().toString().length()==0){
                    Toast.makeText(getContext(), "Please enter Phone Number", Toast.LENGTH_SHORT).show();
                }else if(et_email.getText().toString().length()==0){
                    Toast.makeText(getContext(), "Please enter Email", Toast.LENGTH_SHORT).show();
                }else if(et_message.getText().toString().length()==0){
                    Toast.makeText(getContext(), "Please enter Message", Toast.LENGTH_SHORT).show();
                }else{

                }
            }
        });
        return  view;
    }

}