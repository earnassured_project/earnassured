package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.SlidingTabLayout;

/**
 * Created by Admin on 4/21/2017.
 */

public class RecentHelp_Fragment extends Fragment {
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    private ViewPager viewPager;
    private SlidingTabLayout tabLayout;
    CharSequence Titles[]={"Helps Offererd","Helps Received","Last 10 Payments"};
    int Numboftabs =3;
    public static RecentHelp_Fragment newInstance() {
        RecentHelp_Fragment mapFragment = new RecentHelp_Fragment();
        return mapFragment;
    }
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.recenthelp_fragment, container, false);


        viewPager = (ViewPager)view. findViewById(R.id.rhviewpager);
        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager(),Titles, Numboftabs);
        tabLayout = (SlidingTabLayout)view. findViewById(R.id.rhtab_layout);
        viewPager.setAdapter(adapter);
        try {
            viewPager.setCurrentItem(0, true);
        } catch (Exception e) {
            // TODO: handle exception
            Log.getStackTraceString(e);
        }
        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                // TODO Auto-generated method stub
                return getResources().getColor(R.color.colorPrimary);
            }
            @Override
            public int getDividerColor(int position) {
                // TODO Auto-generated method stub
                return Color.TRANSPARENT;
            }
        });
        tabLayout.setViewPager(viewPager);
        return  view;
    }
    public class PagerAdapter extends FragmentStatePagerAdapter {
        CharSequence Titles[];
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, CharSequence mTitles[], int NumOfTabs) {
            super(fm);
            this.Titles = mTitles;
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            Log.d("ttt","a"+position);
            switch (position) {
                case 0:
                    HelpsOffererd tab1 = new HelpsOffererd().newInstance();
                    return tab1;
                case 1:
                    HelpsReceived tab2 = new HelpsReceived().newInstance();
                    return tab2;
                case 2:
                    LastPayments tab3 = new LastPayments().newInstance();
                    return tab3;
                default:
                    return null;
            }
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }

}
