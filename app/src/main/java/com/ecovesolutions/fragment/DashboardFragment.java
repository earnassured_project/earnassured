package com.ecovesolutions.fragment;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.adapter.RecentAdapter;
import com.ecovesolutions.adapter.TestimonialsAdapter;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.HelpsOfferedModel;
import com.ecovesolutions.model.HelpsReceivedModel;
import com.ecovesolutions.model.LastPaymentModel;
import com.ecovesolutions.model.TestimonialsModel;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Admin on 4/7/2017.
 */

public class DashboardFragment extends Fragment implements AsyncTaskCompleteListener,Response.ErrorListener{
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;
    Typeface gotham_light,gotham_book;
    public static TextView tv_gs,tv_acnt_active,tv_percent,tv_percent_earning,tv_hp_nm,tv_hr_nm,tv_recent,tv_testimonials;
    Button btn_givehelp,btn_receivehelp;
    public static RecentAdapter adapter;
    public static ListView lv_recent;
    public static TestimonialsAdapter mAdapter;
    public static RecyclerView recyclerView;
    public static TestimonialsAdapter testimonialsAdapter;
    public static ScrollView sv_dash;
    public static YouTubePlayer YPlayer;
    public static DashboardFragment newInstance() {
        DashboardFragment mapFragment = new DashboardFragment();
        return mapFragment;
    }
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dashboardfragment, container, false);
        pHelper = new PreferenceHelper(getActivity());
        requestQueue = Volley.newRequestQueue(getActivity());
        Constants.arr_order.clear();
        Constants.testimonialsModels.clear();
        gotham_light= Typeface.createFromAsset(context.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(context.getAssets(), "fonts/gotham_book.otf");
        getExplicitIapIntent();

        sv_dash=(ScrollView)view.findViewById(R.id.sv_dash);

        YouTubePlayerSupportFragment  mYoutubePlayerFragment = new YouTubePlayerSupportFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.youTubePlayer, mYoutubePlayerFragment);
        fragmentTransaction.commit();

        mYoutubePlayerFragment.initialize("AIzaSyAiOPHNPjEEVkidAKU8T1eDJMFgwhWaCv8", new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider arg0, YouTubePlayer youTubePlayer, boolean b) {
                if (!b) {
                    YPlayer = youTubePlayer;
                    YPlayer.setFullscreen(false);
                    YPlayer.setShowFullscreenButton(false);
                    YPlayer.loadVideo("2UC_8NdR1To");
                    YPlayer.pause();
                }
            }
            @Override
            public void onInitializationFailure(YouTubePlayer.Provider arg0, YouTubeInitializationResult arg1) {
                // TODO Auto-generated method stub
            }
        });
        lv_recent = (ListView)view. findViewById(R.id.lv_recent);
        adapter=new RecentAdapter(getActivity(), Constants.arr_order);
        lv_recent.setAdapter(adapter);

        recyclerView = (RecyclerView)view.findViewById(R.id.rv_testimonials);
        recyclerView.setNestedScrollingEnabled(false);
        testimonialsAdapter = new TestimonialsAdapter(getActivity(), Constants.testimonialsModels);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(testimonialsAdapter);


        tv_gs=(TextView)view.findViewById(R.id.tv_gs);
        tv_acnt_active=(TextView)view.findViewById(R.id.tv_acnt_active);
        tv_percent=(TextView)view.findViewById(R.id.tv_percent);
        tv_percent_earning=(TextView)view.findViewById(R.id.tv_percent_earning);
        tv_hp_nm=(TextView)view.findViewById(R.id.tv_hp_nm);
        tv_hr_nm=(TextView)view.findViewById(R.id.tv_hr_nm);
        tv_recent=(TextView)view.findViewById(R.id.tv_recent);
        tv_testimonials=(TextView)view.findViewById(R.id.tv_testimonials);

        btn_givehelp=(Button)view.findViewById(R.id.btn_givehelp);
        btn_receivehelp=(Button)view.findViewById(R.id.btn_receivehelp);

        btn_givehelp.setTypeface(gotham_light);
        btn_receivehelp.setTypeface(gotham_light);

        tv_gs.setTypeface(gotham_book);
        tv_acnt_active.setTypeface(gotham_book);
        tv_percent.setTypeface(gotham_book);
        tv_percent_earning.setTypeface(gotham_light);
        tv_hp_nm.setTypeface(gotham_book);
        tv_hp_nm.setTypeface(gotham_book);
        tv_recent.setTypeface(gotham_book);
        tv_testimonials.setTypeface(gotham_book);

        setListViewHeightBasedOnChildren(lv_recent);

        btn_givehelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
            }
        });
//        getData();
        return  view;
    }

    public void getData(){
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
        AnyUtils.startDialog(getActivity());
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.DASHBOARD);
        map.put(Constants.Params.ACTION,"dashboard");
        map.put(Constants.Params.USER_ID,pHelper.getUserId());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,1, this, this));
    }
    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        if(!((Activity) context).isFinishing()){
            AnyUtils.showserver_popup(getActivity());
        }
    }
    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        switch (serviceCode) {
            case 1:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){
                        Constants.arr_order.clear();
                        Constants.testimonialsModels.clear();
                        Constants.helpsOfferedModels.clear();
                        Constants.helpsReceivedModels.clear();
                        Constants.lastPaymentModels.clear();
                        JSONArray data=jObj.getJSONArray("Recent Members");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject obj1=data.getJSONObject(i);
                            HashMap<String,String> map=new HashMap<String,String>();
                            map.put(Constants.Params.TIME, "20min");
                            map.put(Constants.Params.IMAGE_URL, obj1.getString("profile_pic"));
                            map.put(Constants.Params.TYPE, obj1.getString("status"));
                            map.put(Constants.Params.USER_NAME, obj1.getString("username"));
                            Constants.arr_order.add(map);
                        }

                        JSONArray testimonial=jObj.getJSONArray("testimonial");
                        for (int j = 0; j < testimonial.length(); j++) {
                            JSONObject obj2=testimonial.getJSONObject(j);
                            String username=obj2.getString("username");
                            String profile_pic=obj2.getString("profile_pic");
                            String message=obj2.getString("message");
                            TestimonialsModel model = new TestimonialsModel("1", username, profile_pic, message);
                            Constants.testimonialsModels.add(model);
                        }
                        JSONObject phone = jObj.getJSONObject("General Stats");
                        tv_acnt_active.setText(phone.getString("status"));
                        tv_percent.setText(phone.getString("score")+"%");
                        tv_hp_nm.setText(phone.getString("paired_with"));
                        tv_hr_nm.setText(phone.getString("user_id"));

                        JSONArray offered=jObj.getJSONArray("Recent Helps Offered");
                        for (int j = 0; j < offered.length(); j++) {
                            JSONObject obj2=offered.getJSONObject(j);
                            String username=obj2.getString("username");
                            String amount_off=obj2.getString("amount_off");
                            String start_date=obj2.getString("start_date");
                            HelpsOfferedModel model = new HelpsOfferedModel("1", username, amount_off, start_date);
                            Constants.helpsOfferedModels.add(model);
                        }
                        JSONArray received=jObj.getJSONArray("Recent Helps Received");
                        for (int x = 0; x < received.length(); x++) {
                            JSONObject obj4=received.getJSONObject(x);
                            String username=obj4.getString("username");
                            String amount_off=obj4.getString("amount_off");
                            String start_date=obj4.getString("start_date");
                            HelpsReceivedModel model = new HelpsReceivedModel("1", username, amount_off, start_date);
                            Constants.helpsReceivedModels.add(model);
                        }
                        JSONArray payment=jObj.getJSONArray("Payment conform");
                        for (int j = 0; j < payment.length(); j++) {
                            JSONObject obj2=payment.getJSONObject(j);
                            String username=obj2.getString("name");
                            String profile_pic=obj2.getString("profile_pic");
                            String amount_off=obj2.getString("amount_off");
                            String match_date=obj2.getString("match_date");
                            LastPaymentModel model = new LastPaymentModel("1", username, profile_pic, amount_off,match_date);
                            Constants.lastPaymentModels.add(model);
                        }
                    }else{
                        AnyUtils.stopDialog(getActivity());
                    }
                    adapter.notifyDataSetChanged();
                    testimonialsAdapter.notifyDataSetChanged();
                    setListViewHeightBasedOnChildren(lv_recent);
                    sv_dash.post(new Runnable() {
                        public void run() {
                            sv_dash.fullScroll(View.FOCUS_UP);
                        }
                    });
//                    lv_recent.smoothScrollToPosition(0);
//                    sv_dash.scrollTo(0, sv_dash.getTop());
//                    sv_dash.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                        @Override
//                        public void onGlobalLayout() {
//                            // Ready, move up
//                            sv_dash.fullScroll(View.FOCUS_UP);
//                        }
//                    });
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    AnyUtils.stopDialog(getActivity());
                    e.printStackTrace();
                }
//                sv_dash.fullScroll(sv_dash.FOCUS_UP);

                break;

            default:
                break;
        }
    }
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        RecentAdapter listAdapter = (RecentAdapter) listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
    private Intent getExplicitIapIntent() {
        PackageManager pm = getContext().getPackageManager();
        Intent implicitIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        List<ResolveInfo> resolveInfos = pm.queryIntentServices(implicitIntent, 0);
        // Is somebody else trying to intercept our IAP call?
        if (resolveInfos == null || resolveInfos.size() != 1) {
            return null;
        }

        ResolveInfo serviceInfo = resolveInfos.get(0);
        String packageName = serviceInfo.serviceInfo.packageName;
        String className = serviceInfo.serviceInfo.name;
        ComponentName component = new ComponentName(packageName, className);
        Intent iapIntent = new Intent();
        iapIntent.setComponent(component);
        return iapIntent;
    }
}
