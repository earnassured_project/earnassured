package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.HelpsOfferedModel;
import com.ecovesolutions.model.HelpsReceivedModel;
import com.ecovesolutions.model.LastPaymentModel;
import com.ecovesolutions.model.ReferralBonus_Model;
import com.ecovesolutions.model.TestimonialsModel;
import com.ecovesolutions.model.ViewReferral_Model;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.utilities.SlidingTabLayoutNotify;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Admin on 4/26/2017.
 */

public class Dashboard extends Fragment implements AsyncTaskCompleteListener,Response.ErrorListener {
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    private ViewPager viewPager;
    private SlidingTabLayoutNotify tabLayout;
    CharSequence Titles[]={"Dashboard","Recent Helps"};
    int Numboftabs =2;
    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;
    public static Dashboard newInstance() {
        Dashboard mapFragment = new Dashboard();
        return mapFragment;
    }
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dashboard, container, false);
        pHelper = new PreferenceHelper(getContext());
        requestQueue = Volley.newRequestQueue(getContext());

        viewPager = (ViewPager)view. findViewById(R.id.viewpager_dash);
        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager(),Titles, Numboftabs);
        tabLayout = (SlidingTabLayoutNotify)view. findViewById(R.id.tab_layout_dash);
        tabLayout.setDistributeEvenly(true);
        viewPager.setAdapter(adapter);
        try {
            viewPager.setCurrentItem(0, true);
        } catch (Exception e) {
            // TODO: handle exception
            Log.getStackTraceString(e);
        }
        tabLayout.setCustomTabColorizer(new SlidingTabLayoutNotify.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                // TODO Auto-generated method stub
                return getResources().getColor(R.color.colorPrimary);
            }
            @Override
            public int getDividerColor(int position) {
                // TODO Auto-generated method stub
                return Color.TRANSPARENT;
            }
        });
        tabLayout.setViewPager(viewPager);


        getData();

        return  view;
    }
    public void getData(){
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
        AnyUtils.startDialog(getActivity());
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.DASHBOARD);
        map.put(Constants.Params.USER_ID,pHelper.getUserId());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,1, this, this));
    }

    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        if(!((Activity) context).isFinishing()){
            AnyUtils.showserver_popup(getActivity());
        }
    }
    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        switch (serviceCode) {
            case 1:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){
                        Constants.arr_order.clear();
                        Constants.testimonialsModels.clear();
                        Constants.helpsOfferedModels.clear();
                        Constants.helpsReceivedModels.clear();
                        Constants.lastPaymentModels.clear();
                        JSONArray data=jObj.getJSONArray("Recent Members");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject obj1=data.getJSONObject(i);
                            HashMap<String,String> map=new HashMap<String,String>();
                            map.put(Constants.Params.TIME, "20min");
                            map.put(Constants.Params.IMAGE_URL, obj1.getString("profile_pic"));
                            map.put(Constants.Params.TYPE, obj1.getString("status"));
                            map.put(Constants.Params.USER_NAME, obj1.getString("username"));
                            Constants.arr_order.add(map);
                        }

                        JSONArray testimonial=jObj.getJSONArray("testimonial");
                        for (int j = 0; j < testimonial.length(); j++) {
                            JSONObject obj2=testimonial.getJSONObject(j);
                            if (obj2 != null && obj2.length() > 0) {
                                String username = obj2.getString("username");
                                String profile_pic = obj2.getString("profile_pic");
                                String message = obj2.getString("message");
                                TestimonialsModel model = new TestimonialsModel("1", username, profile_pic, message);
                                Constants.testimonialsModels.add(model);
                            }
                        }
                        JSONObject phone = jObj.getJSONObject("General Stats");
                        if (phone.has("status")) {
                            DashboardFragment.tv_acnt_active.setText(phone.getString("status"));
                        }
                        if (phone.has("score")) {
                            DashboardFragment.tv_percent.setText(phone.getString("score")+"%");
                        }
                        DashboardFragment.tv_hp_nm.setText(phone.getString("paired_with"));
                        DashboardFragment.tv_hr_nm.setText(phone.getString("message"));

                        JSONArray offered=jObj.getJSONArray("Recent Helps Offered");
                        for (int j = 0; j < offered.length(); j++) {

                            JSONObject obj2=offered.getJSONObject(j);
                            String username=obj2.getString("username");
                            String amount_off=obj2.getString("amount_off");
                            String start_date=obj2.getString("start_date");
                            HelpsOfferedModel model = new HelpsOfferedModel("1", username, amount_off, start_date);
                            Constants.helpsOfferedModels.add(model);
                        }
                        JSONArray received=jObj.getJSONArray("Recent Helps Received");
                        for (int x = 0; x < received.length(); x++) {
                            JSONObject obj4=received.getJSONObject(x);
                            String username=obj4.getString("username");
                            String amount_off=obj4.getString("amount_off");
                            String start_date=obj4.getString("start_date");
                            HelpsReceivedModel model = new HelpsReceivedModel("1", username, amount_off, start_date);
                            Constants.helpsReceivedModels.add(model);
                        }
                        JSONArray payment=jObj.getJSONArray("Payment conform");
                        for (int j = 0; j < payment.length(); j++) {
                            JSONObject obj2=payment.getJSONObject(j);
                            String username=obj2.getString("name");
                            String profile_pic=obj2.getString("profile_pic");
                            String amount_off=obj2.getString("amount_off");
                            String match_date=obj2.getString("match_date");
                            LastPaymentModel model = new LastPaymentModel("1", username, profile_pic, amount_off,match_date);
                            Constants.lastPaymentModels.add(model);
                        }
                    }else{
                        AnyUtils.stopDialog(getActivity());
                    }
                    DashboardFragment.adapter.notifyDataSetChanged();
                    DashboardFragment.testimonialsAdapter.notifyDataSetChanged();
                    DashboardFragment.setListViewHeightBasedOnChildren(DashboardFragment.lv_recent);
                    DashboardFragment.sv_dash.post(new Runnable() {
                        public void run() {
                            DashboardFragment.sv_dash.fullScroll(View.FOCUS_UP);
                        }
                    });
                    HelpsOffererd.refresh();
                    HelpsReceived.refresh();
                    Log.d("ttt","rho"+Constants.helpsOfferedModels.size());
//                    LastPayments.refresh();
//                    lv_recent.smoothScrollToPosition(0);
//                    sv_dash.scrollTo(0, sv_dash.getTop());
//                    sv_dash.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//                        @Override
//                        public void onGlobalLayout() {
//                            // Ready, move up
//                            sv_dash.fullScroll(View.FOCUS_UP);
//                        }
//                    });
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    AnyUtils.stopDialog(getActivity());
                    e.printStackTrace();
                }
//                sv_dash.fullScroll(sv_dash.FOCUS_UP);

                break;

            default:
                break;
        }
    }
    public class PagerAdapter extends FragmentStatePagerAdapter {
        CharSequence Titles[];
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, CharSequence mTitles[], int NumOfTabs) {
            super(fm);
            this.Titles = mTitles;
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    DashboardFragment tab1 = new DashboardFragment().newInstance();
                    return tab1;
                case 1:
                    RecentHelp_Fragment tab2 = new RecentHelp_Fragment().newInstance();
                    return tab2;
                default:
                    return null;
            }
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }
}
