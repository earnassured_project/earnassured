package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.SlidingTabLayout;

/**
 * Created by Admin on 4/12/2017.
 */

public class GiveHelp_Fragment extends Fragment {
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    public static ViewPager viewPager;
    private SlidingTabLayout tabLayout;
    CharSequence Titles[]={"New Donation","Make Payment","Lend Help History","All Payment"};
    int Numboftabs =4;
    public static GiveHelp_Fragment newInstance() {
        GiveHelp_Fragment mapFragment = new GiveHelp_Fragment();
        return mapFragment;
    }
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.givehelp_fragment, container, false);
        viewPager = (ViewPager)view. findViewById(R.id.gviewpager);
        Constants.transactionModelArrayList.clear();
        Constants.makePayment_models.clear();
        Constants.allpaymnetModelArrayList.clear();

        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager(),Titles, Numboftabs);
        tabLayout = (SlidingTabLayout)view. findViewById(R.id.gtab_layout);
        viewPager.setAdapter(adapter);
        try {
            viewPager.setCurrentItem(0, true);
        } catch (Exception e) {
            // TODO: handle exception
            Log.getStackTraceString(e);
        }
        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                // TODO Auto-generated method stub
                return getResources().getColor(R.color.colorPrimary);
            }
            @Override
            public int getDividerColor(int position) {
                // TODO Auto-generated method stub
                return Color.TRANSPARENT;
            }
        });
        tabLayout.setViewPager(viewPager);
        return  view;
    }
    public class PagerAdapter extends FragmentStatePagerAdapter {
        CharSequence Titles[];
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, CharSequence mTitles[], int NumOfTabs) {
            super(fm);
            this.Titles = mTitles;
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    New_Donation tab1 = new New_Donation().newInstance();
                    return tab1;
                case 1:
                    Make_Payment tab2 = new Make_Payment().newInstance();
                    return tab2;
                case 2:
                    Transaction_History tab3 = new Transaction_History().newInstance();
                    return tab3;
                case 3:
                    All_Payments tab4 = new All_Payments().newInstance();
                    return tab4;

                default:
                    return null;
            }
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
//        @Override
//        public void destroyItem(ViewGroup container, int position, Object object) {
//            ((ViewPager) container).removeView((View) object);
//            object=null;
//        }
    }
}