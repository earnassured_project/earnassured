package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.adapter.MakePaymentAdapter;
import com.ecovesolutions.adapter.TransactionAdapter;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.MakePayment_Model;
import com.ecovesolutions.model.TransactionModel;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Admin on 4/14/2017.
 */

public class Make_Payment extends Fragment implements AsyncTaskCompleteListener,Response.ErrorListener{
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    TextView tv_title;
    Typeface gotham_light,gotham_book;
    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;
    MakePaymentAdapter mAdapter;
    ListView lv_makepayment;
    TextView tv_no_data;
    ProgressBar pb_internet;
    public static Make_Payment newInstance() {
        Make_Payment mapFragment = new Make_Payment();
        return mapFragment;
    }
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUserVisibleHint(false);
        mBundle = savedInstanceState;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.make_payment, container, false);
        pHelper = new PreferenceHelper(getContext());
        requestQueue = Volley.newRequestQueue(getContext());

        gotham_light=Typeface.createFromAsset(context.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(context.getAssets(), "fonts/gotham_book.otf");
        tv_no_data=(TextView)view.findViewById(R.id.tv_no_data);
        tv_no_data.setTypeface(AnyUtils.getGothamBook(context));
        pb_internet=(ProgressBar)view.findViewById(R.id.pb_internet);

        tv_title=(TextView)view.findViewById(R.id.tv_title);
        tv_title.setTypeface(gotham_book);

        lv_makepayment = (ListView) view.findViewById(R.id.lv_makepayment);
        mAdapter=new MakePaymentAdapter(getActivity(),R.layout.makepaymentadapter, Constants.makePayment_models);
        lv_makepayment.setAdapter(mAdapter);


        return  view;
    }
    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            if(Constants.makePayment_models.size()==0) {
//                getRequestStatus(1);
                getMakePayment(1);
            }else{
                pb_internet.setVisibility(View.GONE);
                tv_no_data.setVisibility(View.GONE);
                lv_makepayment.setVisibility(View.VISIBLE);
            }
        }
    }
    public void getRequestStatus(int code) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.URL_BEST+"/provide_api/makepayment/"+pHelper.getUserId());
        requestQueue.add(new VolleyHttpRequest(Request.Method.GET, map, code, this, this));
    }
    public void getMakePayment(int code){
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
//        AnyUtils.startDialog(getActivity());
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.GIVEHELP+"makepayment");
        map.put(Constants.Params.USER_ID, pHelper.getUserId());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,code, this, this));
    }
    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        if(!((Activity) context).isFinishing()){
            AnyUtils.showserver_popup(getActivity());
        }
    }
    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        pb_internet.setVisibility(View.GONE);
        switch (serviceCode) {
            case 1:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){
                        Constants.makePayment_models.clear();
                        tv_no_data.setVisibility(View.GONE);
                        lv_makepayment.setVisibility(View.VISIBLE);

                        JSONArray data_array=jObj.getJSONArray("data");
                        for (int i = 0; i < data_array.length(); i++) {
                            JSONObject obj1=data_array.getJSONObject(i);
                            String id=obj1.getString("pair_id");
                            String match_date=obj1.getString("match_date");
                            String expire_date=obj1.getString("expire_date");
                            String amount_off=obj1.getString("amount_off");
                            String recipient=obj1.getString("RECIPIENT");
                            String bankname=obj1.getString("PAYMENT DETAILS");
                            String action=obj1.getString("ACTION");
                            String Contact_Recipient=obj1.getString("Contact Recipient");
//                            String bitcoin=obj1.getString("bitcoin");
//                            String payment_confirm=obj1.getString("payment_confirm");


                            MakePayment_Model model=new MakePayment_Model(match_date,expire_date,amount_off,id,recipient,bankname,action,Contact_Recipient,"","");
                            Constants.makePayment_models.add(model);

                        }

                    }else{
                        tv_no_data.setVisibility(View.VISIBLE);
                        tv_no_data.setText(Constants.message);
                        lv_makepayment.setVisibility(View.GONE);
//                        if(!((Activity) context).isFinishing()){
//                            AnyUtils.showserver_popup(Constants.message,getContext())	;
//                        }
                        AnyUtils.stopDialog(getActivity());
                    }
                    mAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    AnyUtils.stopDialog(getActivity());
                    e.printStackTrace();
                }
                break;

            default:
                break;
        }
    }

}