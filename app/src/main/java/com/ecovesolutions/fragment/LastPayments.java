package com.ecovesolutions.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ecovesolutions.adapter.LastPaymentsAdapter;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;

/**
 * Created by Admin on 4/21/2017.
 */

public class LastPayments extends Fragment {
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    public static ListView lv_lastpayments;
    public static TextView tv_no_data;
    public static ProgressBar pb_internet;
    public static LastPaymentsAdapter mAdapter;
    public static LastPayments newInstance() {
        LastPayments mapFragment = new LastPayments();
        return mapFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.lastpayments, container, false);
        tv_no_data=(TextView)view.findViewById(R.id.tv_no_data);
        tv_no_data.setTypeface(AnyUtils.getGothamBook(context));
        pb_internet=(ProgressBar)view.findViewById(R.id.pb_internet);
        lv_lastpayments = (ListView) view.findViewById(R.id.lv_lastpayments);

        if(Constants.lastPaymentModels.size()==0) {
            tv_no_data.setVisibility(View.VISIBLE);
            lv_lastpayments.setVisibility(View.GONE);
            pb_internet.setVisibility(View.GONE);
        }else{
            pb_internet.setVisibility(View.GONE);
            tv_no_data.setVisibility(View.GONE);
            lv_lastpayments.setVisibility(View.VISIBLE);
        }

        mAdapter=new LastPaymentsAdapter(getActivity(),R.layout.lastpaymentschild, Constants.lastPaymentModels);
        lv_lastpayments.setAdapter(mAdapter);
        return view;
    }
    public static void refresh(){
        mAdapter.notifyDataSetChanged();
        if(Constants.lastPaymentModels.size()==0) {
            tv_no_data.setVisibility(View.VISIBLE);
            lv_lastpayments.setVisibility(View.GONE);
            pb_internet.setVisibility(View.GONE);
        }else{
            pb_internet.setVisibility(View.GONE);
            tv_no_data.setVisibility(View.GONE);
            lv_lastpayments.setVisibility(View.VISIBLE);
        }
    }
}

