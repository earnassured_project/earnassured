package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.ReferralBonus_Model;
import com.ecovesolutions.model.ViewReferral_Model;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.utilities.SlidingTabLayoutNotify;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Admin on 4/10/2017.
 */

public class Myreferral_Fragment extends Fragment implements AsyncTaskCompleteListener,Response.ErrorListener {
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    private ViewPager viewPager;
    private SlidingTabLayoutNotify tabLayout;
    CharSequence Titles[]={"View Team","Team Bonus"};
    int Numboftabs =2;
    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;
    public static Myreferral_Fragment newInstance() {
        Myreferral_Fragment mapFragment = new Myreferral_Fragment();
        return mapFragment;
    }
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.myreferral_fragment, container, false);
        pHelper = new PreferenceHelper(getContext());
        requestQueue = Volley.newRequestQueue(getContext());

        viewPager = (ViewPager)view. findViewById(R.id.viewpager);
        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager(),Titles, Numboftabs);
        tabLayout = (SlidingTabLayoutNotify)view. findViewById(R.id.tab_layout);
        tabLayout.setDistributeEvenly(true);
        viewPager.setAdapter(adapter);
        try {
            viewPager.setCurrentItem(0, true);
        } catch (Exception e) {
            // TODO: handle exception
            Log.getStackTraceString(e);
        }
        tabLayout.setCustomTabColorizer(new SlidingTabLayoutNotify.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                // TODO Auto-generated method stub
                return getResources().getColor(R.color.colorPrimary);
            }
            @Override
            public int getDividerColor(int position) {
                // TODO Auto-generated method stub
                return Color.TRANSPARENT;
            }
        });
        tabLayout.setViewPager(viewPager);


//        ViewReferral_Model model=new ViewReferral_Model("S0001","Satheesh","satheesh@ecovesolutions","7598483512","India","03/06/2017");
//        Constants.viewreferral_model.add(model);
//        ReferralBonus_Model model1=new ReferralBonus_Model("S0001","Satheesh","1985","198.5","active","03/06/2017");
//        Constants.referralbonus_models.add(model1);

        getViewReferral(1);
        getReferalbonus(2);
        return  view;
    }
    public void getReferalbonus(int code){
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.REFERRALS+"/referalbonus");
        map.put(Constants.Params.USER_ID, pHelper.getUserId());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,code , this, this));
    }
    public void getViewReferral(int code){
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
        AnyUtils.startDialog(getActivity());
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.REFERRALS);
        map.put("username",pHelper.getName());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,code , this, this));
    }

    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        if(!((Activity) context).isFinishing()){
            AnyUtils.showserver_popup(getActivity());
        }
    }
    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        ViewReferral_Fragment.pb_internet.setVisibility(View.GONE);
        ReferralBonus_Fragment.pb_internet.setVisibility(View.GONE);

        switch (serviceCode) {
            case 3:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){
                        Constants.viewreferral_model.clear();
                        Constants.referralbonus_models.clear();
                        JSONArray arr_his1=jObj.getJSONArray("viewreferrals");
                        for (int i = 0; i < arr_his1.length(); i++) {
                            JSONObject obj1=arr_his1.getJSONObject(i);
                            String id=obj1.getString(Constants.Params.ID);
                            String username=obj1.getString(Constants.Params.USER_NAME);
                            String email=obj1.getString(Constants.Params.EMAIL);
                            String country=obj1.getString("leva");
                            String phone=obj1.getString(Constants.Params.PHONE);
                            String date=obj1.getString(Constants.Params.DATE);

                            ViewReferral_Model model=new ViewReferral_Model(id,username,email,phone,country,date);
                            Constants.viewreferral_model.add(model);
                        }
                        JSONArray arr_his2=jObj.getJSONArray("referralsbonus");
                        for (int i = 0; i < arr_his2.length(); i++) {
                            JSONObject obj2=arr_his2.getJSONObject(i);
                            String id=obj2.getString("bonus_id");
                            String amount=obj2.getString("amount");
                            String username=obj2.getString(Constants.Params.USER_NAME);
                            String amount_off=obj2.getString("amount_off");
                            String date=obj2.getString("date_used");
                            String status="";
                            if(obj2.getString("status").matches("1")){
                                status="active";
                            }else{
                                status="inactive";
                            }


                            ReferralBonus_Model model1=new ReferralBonus_Model(id,username,amount_off,amount,status,date);
                            Constants.referralbonus_models.add(model1);
                        }
                    }else{


//                        if(!((Activity) context).isFinishing()){
//				           AnyUtils.showserver_popup(Constants.message,getContext())	;
//			        	}
//                        AnyUtils.stopDialog(getActivity());
                    }
                    if(Constants.referralbonus_models.size()==0){
                        ReferralBonus_Fragment.tv_no_data.setVisibility(View.VISIBLE);
                        ReferralBonus_Fragment.tv_no_data.setText("No Record Found");
                        ReferralBonus_Fragment.lv_referralbonus.setVisibility(View.GONE);
                    }else{
                        ReferralBonus_Fragment.tv_no_data.setVisibility(View.GONE);
                        ReferralBonus_Fragment.lv_referralbonus.setVisibility(View.VISIBLE);
                    }
                    if(Constants.viewreferral_model.size()==0){
                        ViewReferral_Fragment.tv_no_data.setVisibility(View.VISIBLE);
                        ViewReferral_Fragment.tv_no_data.setText("No Record Found");
                        ViewReferral_Fragment.lv_viewreferral.setVisibility(View.GONE);
                    }else{
                        ViewReferral_Fragment.tv_no_data.setVisibility(View.GONE);
                        ViewReferral_Fragment.lv_viewreferral.setVisibility(View.VISIBLE);
                    }
                    ViewReferral_Fragment.mAdapter.notifyDataSetChanged();
                    ReferralBonus_Fragment.mAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    AnyUtils.stopDialog(getActivity());
                    e.printStackTrace();
                }
                break;
            case 1:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){
                        Constants.viewreferral_model.clear();
                        JSONArray arr_his=jObj.getJSONArray(Constants.Params.DATA);
                        for (int i = 0; i < arr_his.length(); i++) {
                            JSONObject obj1=arr_his.getJSONObject(i);
                            String id=obj1.getString("id");
                            String username=obj1.getString("username");
                            String email=obj1.getString("email");
                            String country=obj1.getString("leva");
                            String phone=obj1.getString("phone");
                            String date=obj1.getString("date");

                            ViewReferral_Model model=new ViewReferral_Model(id,username,email,phone,country,date);
                            Constants.viewreferral_model.add(model);
                        }
                    }else{
                        if(!((Activity) context).isFinishing()){
                            AnyUtils.showserver_popup(Constants.message,getContext())	;
                        }
                        AnyUtils.stopDialog(getActivity());
                    }
                    if(Constants.viewreferral_model.size()==0){
                        ViewReferral_Fragment.tv_no_data.setVisibility(View.VISIBLE);
                        ViewReferral_Fragment.tv_no_data.setText("No Record Found");
                        ViewReferral_Fragment.lv_viewreferral.setVisibility(View.GONE);
                    }else{
                        ViewReferral_Fragment.tv_no_data.setVisibility(View.GONE);
                        ViewReferral_Fragment.lv_viewreferral.setVisibility(View.VISIBLE);
                    }
                    ViewReferral_Fragment.mAdapter.notifyDataSetChanged();
                    ViewReferral_Fragment.setListViewHeightBasedOnChildren(ViewReferral_Fragment.lv_viewreferral);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    AnyUtils.stopDialog(getActivity());
                    e.printStackTrace();
                }
                break;
            case 2:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){
                        Constants.referralbonus_models.clear();
                        JSONArray arr_his=jObj.getJSONArray(Constants.Params.DATA);
                        for (int i = 0; i < arr_his.length(); i++) {
                            JSONObject obj2=arr_his.getJSONObject(i);
                            String id="";
                            String amount=obj2.getString("amount");
                            String username=obj2.getString("username");
                            String amount_off=obj2.getString("amount_off");
                            String date=obj2.getString("date_used");
                            String status="";
                            if(obj2.getString("status").matches("Used")){
                                status="active";
                            }else{
                                status="inactive";
                            }


                            ReferralBonus_Model model1=new ReferralBonus_Model(id,username,amount_off,amount,status,date);
                            Constants.referralbonus_models.add(model1);
                        }
                    }else{
                        if(!((Activity) context).isFinishing()){
                            AnyUtils.showserver_popup(Constants.message,getContext())	;
                        }
                        AnyUtils.stopDialog(getActivity());
                    }
                    if(Constants.referralbonus_models.size()==0){
                        ReferralBonus_Fragment.tv_no_data.setVisibility(View.VISIBLE);
                        ReferralBonus_Fragment.tv_no_data.setText("No Record Found");
                        ReferralBonus_Fragment.lv_referralbonus.setVisibility(View.GONE);
                    }else{
                        ReferralBonus_Fragment.tv_no_data.setVisibility(View.GONE);
                        ReferralBonus_Fragment.lv_referralbonus.setVisibility(View.VISIBLE);
                    }
                    ReferralBonus_Fragment.mAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    AnyUtils.stopDialog(getActivity());
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }
    public class PagerAdapter extends FragmentStatePagerAdapter {
        CharSequence Titles[];
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, CharSequence mTitles[], int NumOfTabs) {
            super(fm);
            this.Titles = mTitles;
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    ViewReferral_Fragment tab1 = new ViewReferral_Fragment();
                    return tab1;
                case 1:
                    ReferralBonus_Fragment tab2 = new ReferralBonus_Fragment();
                    return tab2;

                default:
                    return null;
            }
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }
}
