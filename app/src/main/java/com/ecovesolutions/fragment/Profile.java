package com.ecovesolutions.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ecovesolutions.earnassured.BuildConfig;
import com.ecovesolutions.earnassured.Login_Screen;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.CircleImageView;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.MultipartRequest2;
import com.ecovesolutions.volley.Singleton_volley;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by Admin on 4/10/2017.
 */

public class Profile extends Fragment implements AsyncTaskCompleteListener,Response.ErrorListener{
    int REQUEST_CAMERA=0;
    int SELECT_FILE=1;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static File filepathUrl;
    String imageUrl="";
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;
    Typeface gotham_light,gotham_book;
    TextView tv_name,tv_username,tv_phonenumber,tv_adress,tv_edit;
    EditText edt_phonenumber,edt_username,edt_adress;
    ImageView iv_profile;
    CircleImageView profile_image;
    String params = "";
    String result = "";
    int serverResponseCode = 0;
    private String userChoosenTask="";
    String[] PERMISSIONS = {Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE};
    public static Profile newInstance() {
        Profile mapFragment = new Profile();
        return mapFragment;
    }
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.profile, container, false);
        pHelper = new PreferenceHelper(getActivity());
        requestQueue = Volley.newRequestQueue(getActivity());
        gotham_light= Typeface.createFromAsset(context.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(context.getAssets(), "fonts/gotham_book.otf");
        profile_image=(CircleImageView)view.findViewById(R.id.profile_image);

        tv_name= (TextView) view.findViewById(R.id.tv_name);
        tv_username=(TextView) view.findViewById(R.id.tv_username);
        edt_username= (EditText) view.findViewById(R.id.edt_username);
        tv_phonenumber=(TextView) view.findViewById(R.id.tv_phonenumber);
        edt_phonenumber=(EditText) view.findViewById(R.id.edt_phonenumber);
        tv_adress=(TextView) view.findViewById(R.id.tv_adress);
        edt_adress=(EditText) view.findViewById(R.id.edt_adress);
        tv_edit=(TextView) view.findViewById(R.id.tv_edit);

        iv_profile=(ImageView)view.findViewById(R.id.iv_profile);

        tv_name.setTypeface(gotham_book);
        edt_username.setTypeface(gotham_light);
        edt_username.setTypeface(gotham_light);
        edt_phonenumber.setTypeface(gotham_light);
        tv_username.setTypeface(gotham_book);
        tv_phonenumber.setTypeface(gotham_book);
        tv_adress.setTypeface(gotham_book);
        tv_edit.setTypeface(gotham_light);
        edt_adress.setTypeface(gotham_light);

        tv_name.setText(pHelper.getName());
        edt_username.setText(pHelper.getEmail());
        edt_phonenumber.setText(pHelper.getMobile_No());
        edt_adress.setText(pHelper.getCountry());
        edt_phonenumber.setCursorVisible(false);
        String image=pHelper.getProfileUrl();
        if(image != null && !image.isEmpty()){
            Glide.with(getActivity())
                    .load(image)
                    .asBitmap()
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .priority(Priority.IMMEDIATE)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            profile_image.setImageBitmap(resource);
                        }
                    });

        }else {
            try {
                profile_image.setBackgroundResource(R.mipmap.pro_iv);
                profile_image.setScaleType(ImageView.ScaleType.CENTER_CROP);
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("exception", Log.getStackTraceString(e));
            }
        }
        tv_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!imageUrl.matches("")) {
                    AnyUtils.startDialog(getActivity());
                    new UploadProfileAsync().execute();
                }else{
                    if(edt_phonenumber.getText().toString().length()==0){
                        Toast.makeText(getContext(), "Please enter Phone Number", Toast.LENGTH_SHORT).show();
                    }else if (edt_phonenumber.getText().toString().length()<7) {
                        Toast.makeText(getContext(), "Please enter the Valid Phone Number", Toast.LENGTH_SHORT).show();
                    }else {
                        updatePhoneNo();
                    }
                }
            }
        });
        iv_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage();
            }
        });
        edt_phonenumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edt_phonenumber.setCursorVisible(true);
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(context,PERMISSIONS,1);
        }

        return  view;
    }
    public void updatePhoneNo(){
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
        AnyUtils.hideKeyboard(getActivity());
        AnyUtils.startDialog(getActivity());
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.MYACCOUNT+"profilemanagement");
        map.put(Constants.Params.USER_ID, pHelper.getUserId());
        map.put("phone", edt_phonenumber.getText().toString());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,1 , this, this));
    }
    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        if(!((Activity) context).isFinishing()){
            AnyUtils.showserver_popup(getActivity());
        }
    }
    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());

        switch (serviceCode) {
            case 1:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){
                        edt_phonenumber.setCursorVisible(false);
                        JSONObject data =jObj.getJSONObject("data");
                        Toast.makeText(getContext(), "Successfully Phone Number Updated", Toast.LENGTH_SHORT).show();
                        pHelper.putMobile_No(data.getString(Constants.Params.PHONE));
                    }else{
                        if(!((Activity) context).isFinishing()){
                            AnyUtils.showserver_popup(Constants.message,getContext())	;
                        }
                        AnyUtils.stopDialog(getActivity());
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    AnyUtils.stopDialog(getActivity());
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }

    class UploadDishAsync extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            Constants.status = "";
            Constants.message ="Something went wrong,please retry.";
            result="";
            params = "user_id=" + pHelper.getUserId();
            AnyUtils.startDialog(getActivity());
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub
            uploadFile(imageUrl);


         return null;
        }

        @Override
        protected void onPostExecute(Void result1) {
            // TODO Auto-generated method stub
            super.onPostExecute(result1);
            JsonParsing(result);
            AnyUtils.stopDialog(getActivity());
        }

    }
    public void JsonParsing(String result) {
        if (!result.matches("")) {
            try {
                JSONObject jObj=new JSONObject(result);
                Constants.status = jObj.getString(Constants.Params.STATUS);
                Constants.message = jObj.getString(Constants.Params.MESSAGE);
                if (Constants.status.matches("true")) {
                    imageUrl="";
                    pHelper.putProfileUrl(jObj.getString("image"));
                    Glide.with(context)
                            .load(pHelper.getProfileUrl())
                            .asBitmap()
                            .into(new BitmapImageViewTarget(FragmentDrawer.ivMenuProfile) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    //Play with bitmap
                                    super.setResource(resource);
                                }
                            });
                    Glide.with(context)
                            .load(pHelper.getProfileUrl())
                            .asBitmap()
                            .into(new BitmapImageViewTarget(profile_image) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    //Play with bitmap
                                    super.setResource(resource);
                                }
                            });

                    Toast.makeText(getActivity(), "Profile details has been updated sucesfully.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), ""+Constants.message, Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
    }
    class UploadProfileAsync extends AsyncTask<Void, Void, Void>{
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            Constants.status = "";
            Constants.message ="Something went wrong,please retry.";
        }

        @Override
        protected Void doInBackground(Void... params) {
            // TODO Auto-generated method stub

            HashMap<String, String> param=new HashMap<String, String>();
            param.put("user_id", pHelper.getUserId());
            MultipartRequest2 request=new MultipartRequest2(Constants.ServiceType.MYACCOUNT +"profilepicupload/", new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    // TODO Auto-generated method stub
                    Log.d("ttt", "arg="+response);
                    if (!response.matches("")) {
                        try {
                            JSONObject jObj=new JSONObject(response);
                            Constants.status = jObj.getString(Constants.Params.STATUS);
                            Constants.message = jObj.getString(Constants.Params.MESSAGE);
                            if (Constants.status.matches("true")) {
                                imageUrl="";
                                pHelper.putProfileUrl(jObj.getString("image"));
                                Glide.with(context)
                                        .load(pHelper.getProfileUrl())
                                        .asBitmap()
                                        .into(new BitmapImageViewTarget(FragmentDrawer.ivMenuProfile) {
                                            @Override
                                            protected void setResource(Bitmap resource) {
                                                //Play with bitmap
                                                super.setResource(resource);
                                            }
                                        });
                                Glide.with(context)
                                        .load(pHelper.getProfileUrl())
                                        .asBitmap()
                                        .into(new BitmapImageViewTarget(profile_image) {
                                            @Override
                                            protected void setResource(Bitmap resource) {
                                                //Play with bitmap
                                                super.setResource(resource);
                                            }
                                        });

                                Toast.makeText(getActivity(), "Profile details has been updated sucesfully.", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getContext(), ""+Constants.message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }


                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    // TODO Auto-generated method stub
                    Toast.makeText(getContext(), ""+error, Toast.LENGTH_SHORT).show();

                }
            }, new File(imageUrl), param);

            Singleton_volley.getInstance().addToRequestQueue(request);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            AnyUtils.stopDialog(getActivity());
        }

    }
    public int uploadFile(final String sourceFileUri) {
        Log.d("ttt", "FileUri=" + sourceFileUri);
        HttpURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        File sourceFile = new File(sourceFileUri);
        if (!sourceFile.isFile()) {
            return 0;
        } else {
            try {
                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                URL url = new URL(Constants.ServiceType.MYACCOUNT +"profilepicupload/"+ params);
                Log.d("WebService", "url=" + url);
                // Open a HTTP connection to the URL
                conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
                conn.setRequestProperty("userfile", sourceFileUri);
                dos = new DataOutputStream(conn.getOutputStream());
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name=\"userfile\";filename=\"" + sourceFileUri + "\"" + lineEnd);
                dos.writeBytes(lineEnd);
                // create a buffer of maximum size
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];
                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }
                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (result)
                int responseCode = conn.getResponseCode();
                Log.d("ssk", "result0=" + HttpsURLConnection.HTTP_OK);
                if (responseCode == HttpsURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    Log.d("ssk", "result1=" + br.readLine());
                    while ((line = br.readLine()) != null) {
                        result += line;
                        Log.d("ssk", "result2=" + result);

                    }

                }
                fileInputStream.close();
                dos.flush();
                dos.close();
            } catch (MalformedURLException ex) {
                ex.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return serverResponseCode;
        } // End else block
    }
    private void selectImage(){

        final CharSequence[] items = { "Take Photo", "Choose Gallery", "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result= AnyUtils.checkPermission(getActivity());
                if (items[item].equals("Take Photo")) {
                    userChoosenTask ="Take Photo";
                    if(result) {
                        try {
                            filepathUrl = AnyUtils.createImageFile();
                        } catch (IOException ex) {
                            // Error occurred while creating the File
                            return;
                        }
                        Uri photoURI = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", filepathUrl);
                        final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                        startActivityForResult(intent, REQUEST_CAMERA);
                    }
                } else if (items[item].equals("Choose Gallery")) {
                    userChoosenTask ="Choose from Library";
                    if(result) {
                        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                    }
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });

        builder.show();
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case AnyUtils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (userChoosenTask.equals("Take Photo")){
                        try {
                            filepathUrl = AnyUtils.createImageFile();
                        } catch (IOException ex) {
                            // Error occurred while creating the File
                            return;
                        }
                    Uri photoURI = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", filepathUrl);
                    final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(intent, REQUEST_CAMERA);
                }else if(userChoosenTask.equals("Choose from Library")) {
                        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        intent.setType("image/*");
                        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
                    }
                } else {
                    //code for deny
                }
                break;
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CAMERA) {
            new ImageCompression().execute(filepathUrl.getAbsolutePath());
        } else if (requestCode == SELECT_FILE && resultCode == getActivity().RESULT_OK && data != null && data.getData() != null) {
            filepathUrl= AnyUtils.getOutputMediaFile(MEDIA_TYPE_IMAGE);
            Uri uri = data.getData();
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(projection[0]);
            final String picturePath = cursor.getString(columnIndex);
            cursor.close();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AnyUtils.copyFile(picturePath, filepathUrl.getAbsolutePath());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            new ImageCompression().execute(filepathUrl.getAbsolutePath());
        }
    }
    public class ImageCompression extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... strings) {
            if (strings.length == 0 || strings[0] == null)
                return null;
            return AnyUtils.compressImage(strings[0]);
        }

        protected void onPostExecute(String imagePath) {
            imageUrl=imagePath;
            profile_image.setImageBitmap(BitmapFactory.decodeFile(new File(imagePath).getAbsolutePath()));

        }
    }
}