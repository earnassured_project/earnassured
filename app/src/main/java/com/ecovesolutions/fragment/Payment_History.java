package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.adapter.PaymentAdapter;
import com.ecovesolutions.adapter.RequestHistoryAdapter;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.PaymentModel;
import com.ecovesolutions.model.Reqhistory_Model;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Admin on 4/11/2017.
 */

public class Payment_History extends Fragment implements AsyncTaskCompleteListener,Response.ErrorListener{
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    Typeface gotham_light, gotham_book;
    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;
    ListView lv_paymenthistory;
    PaymentAdapter mAdapter;
    TextView tv_no_data;
    ProgressBar pb_internet;
    RelativeLayout rl_main;
    public static Payment_History newInstance() {
        Payment_History mapFragment = new Payment_History();
        return mapFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUserVisibleHint(false);
        mBundle = savedInstanceState;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.payment_history, container, false);
        pHelper = new PreferenceHelper(getContext());
        requestQueue = Volley.newRequestQueue(getContext());

        rl_main=(RelativeLayout)view.findViewById(R.id.rl_main);
        lv_paymenthistory = (ListView) view.findViewById(R.id.lv_paymenthistory);
        tv_no_data=(TextView)view.findViewById(R.id.tv_no_data);
        tv_no_data.setTypeface(AnyUtils.getGothamBook(context));
        pb_internet=(ProgressBar)view.findViewById(R.id.pb_internet);



        mAdapter = new PaymentAdapter(getActivity(), R.layout.payment_child, Constants.payhistory_ModelArrayList,rl_main);
        lv_paymenthistory.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            if(Constants.payhistory_ModelArrayList.size()==0) {
                getTransaction(3);
            }else{
                tv_no_data.setVisibility(View.GONE);
                lv_paymenthistory.setVisibility(View.VISIBLE);
            }

        }
    }
    public void getTransaction(int code) {
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
//        AnyUtils.startDialog(getActivity());
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.RECEIVEHELP+"paymenthistory");
        map.put(Constants.Params.USER_ID, pHelper.getUserId());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map, code, this, this));
    }

    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        if (!((Activity) context).isFinishing()) {
            AnyUtils.showserver_popup(getActivity());
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        pb_internet.setVisibility(View.GONE);
        switch (serviceCode) {
            case 3:
                try {
                    JSONObject jObj = new JSONObject(response);
                    Constants.status = jObj.getString(Constants.Params.STATUS);
                    Constants.message = jObj.getString(Constants.Params.MESSAGE);
                    if (Constants.status.matches("true")) {
                        Constants.payhistory_ModelArrayList.clear();
                        tv_no_data.setVisibility(View.GONE);
                        lv_paymenthistory.setVisibility(View.VISIBLE);
                        JSONArray data_array = jObj.getJSONArray("data");

                        for (int i = 0; i < data_array.length(); i++) {
                            JSONObject obj1 = data_array.getJSONObject(i);
                            String sno = obj1.getString("s.no.");
                            String username = obj1.getString("PAID BY");
                            String REQUEST_ID = obj1.getString("REQUEST ID");
                            String EXPIRY  = obj1.getString("EXPIRY DATE");
                            String payment_confirm = obj1.getString("PAYMENT STATUS");

                            String ph_id = obj1.getString("PROOF");
                            String profile_pic= obj1.getString("AMOUNT");
                            PaymentModel model = new PaymentModel(username, sno, REQUEST_ID, EXPIRY, payment_confirm, ph_id,profile_pic);
                            Constants.payhistory_ModelArrayList.add(model);

                        }

                    } else {
                        tv_no_data.setVisibility(View.VISIBLE);
                        tv_no_data.setText(Constants.message);
                        lv_paymenthistory.setVisibility(View.GONE);
//                        if (!((Activity) context).isFinishing()) {
//                            AnyUtils.showserver_popup(Constants.message, getContext());
//                        }
                        AnyUtils.stopDialog(getActivity());
                    }
                    if(Constants.payhistory_ModelArrayList.size()==0) {
                        tv_no_data.setVisibility(View.VISIBLE);
                        lv_paymenthistory.setVisibility(View.GONE);
                    }else{
                        tv_no_data.setVisibility(View.GONE);
                        lv_paymenthistory.setVisibility(View.VISIBLE);
                    }
                    mAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    AnyUtils.stopDialog(getActivity());
                    e.printStackTrace();
                }
                break;

            default:
                break;
        }
    }

}
