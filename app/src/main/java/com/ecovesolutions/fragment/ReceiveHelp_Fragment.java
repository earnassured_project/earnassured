package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.SlidingTabLayout;

/**
 * Created by Admin on 4/12/2017.
 */

public class ReceiveHelp_Fragment extends Fragment {
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    private ViewPager viewPager;
    private SlidingTabLayout tabLayout;
    CharSequence Titles[]={"Request Help","Matching History","Request History","Payment History"};
    int Numboftabs =4;
    public static ReceiveHelp_Fragment newInstance() {
        ReceiveHelp_Fragment mapFragment = new ReceiveHelp_Fragment();
        return mapFragment;
    }
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.receivehelp_fragment, container, false);
        Constants.paymentConfirmationModels.clear();
        Constants.Reqhistory_ModelArrayList.clear();
        Constants.payhistory_ModelArrayList.clear();
        Constants.makeRequestModels.clear();
        Constants.writetTestimonyModels.clear();

        viewPager = (ViewPager)view. findViewById(R.id.rviewpager);
        PagerAdapter adapter = new PagerAdapter(getChildFragmentManager(),Titles, Numboftabs);
        tabLayout = (SlidingTabLayout)view. findViewById(R.id.rtab_layout);
        viewPager.setAdapter(adapter);
        try {
            viewPager.setCurrentItem(0, true);
        } catch (Exception e) {
            // TODO: handle exception
            Log.getStackTraceString(e);
        }
        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                // TODO Auto-generated method stub
                return getResources().getColor(R.color.colorPrimary);
            }
            @Override
            public int getDividerColor(int position) {
                // TODO Auto-generated method stub
                return Color.TRANSPARENT;
            }
        });
        tabLayout.setViewPager(viewPager);
        return  view;
    }
    public class PagerAdapter extends FragmentStatePagerAdapter {
        CharSequence Titles[];
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, CharSequence mTitles[], int NumOfTabs) {
            super(fm);
            this.Titles = mTitles;
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    Make_Request tab1 = new Make_Request().newInstance();
                    return tab1;
                case 1:
                    Payment_Confirmation tab2 = new Payment_Confirmation().newInstance();
                    return tab2;
                case 2:
                    Request_History tab3 = new Request_History().newInstance();
                    return tab3;
                case 3:
                    Payment_History tab4 = new Payment_History().newInstance();
                    return tab4;

                default:
                    return null;
            }
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }
}