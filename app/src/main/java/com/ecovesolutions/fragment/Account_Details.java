package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.NumberKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Admin on 4/10/2017.
 */

public class Account_Details extends Fragment implements AsyncTaskCompleteListener,Response.ErrorListener{
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    EditText et_bname, et_chname, et_anumber,et_ifsccode,et_bitcoin;
    Button btn_update,btn_save;
    TextView tv_reset,tv_bank,tv_bitcoin,tv_bitmessage,tv_bname, tv_chname, tv_anumber,tv_ifsccode;
    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;
    Typeface gotham_light,gotham_book;
    public static Account_Details newInstance() {
        Account_Details mapFragment = new Account_Details();
        return mapFragment;
    }
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.account_details, container, false);
        pHelper = new PreferenceHelper(getActivity());
        requestQueue = Volley.newRequestQueue(getActivity());
        gotham_light= Typeface.createFromAsset(context.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(context.getAssets(), "fonts/gotham_book.otf");

        tv_bitcoin=(TextView) view.findViewById(R.id.tv_bitcoin);
        tv_bitmessage=(TextView) view.findViewById(R.id.tv_bitmessage);
        tv_reset=(TextView) view.findViewById(R.id.tv_reset);
        tv_bank=(TextView) view.findViewById(R.id.tv_bank);
        btn_save= (Button) view.findViewById(R.id.btn_save);

        et_bitcoin= (EditText) view.findViewById(R.id.et_bitcoin);
        et_bname = (EditText) view.findViewById(R.id.et_bname);
        et_chname = (EditText) view.findViewById(R.id.et_chname);
        et_anumber = (EditText) view.findViewById(R.id.et_anumber);
        et_ifsccode = (EditText) view.findViewById(R.id.et_ifsccode);

        tv_bname = (TextView) view.findViewById(R.id.tv_bname);
        tv_chname = (TextView) view.findViewById(R.id.tv_chname);
        tv_anumber = (TextView) view.findViewById(R.id.tv_anumber);
        tv_ifsccode = (TextView) view.findViewById(R.id.tv_ifsccode);

        btn_update = (Button) view.findViewById(R.id.btn_update);


        tv_bitcoin.setTypeface(gotham_book);
        tv_bitmessage.setTypeface(gotham_light);
        et_bitcoin.setTypeface(gotham_light);
        btn_save.setTypeface(gotham_light);

        tv_bank.setTypeface(gotham_book);

        tv_bname.setTypeface(gotham_book);
        tv_chname.setTypeface(gotham_book);
        tv_anumber.setTypeface(gotham_book);
        tv_ifsccode.setTypeface(gotham_book);

        tv_reset.setTypeface(gotham_light);
        et_bname.setTypeface(gotham_light);
        et_chname.setTypeface(gotham_light);
        et_anumber.setTypeface(gotham_light);
        btn_update.setTypeface(gotham_light);
        et_ifsccode.setTypeface(gotham_light);


        btn_update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_bname.getText().toString().length() == 0) {
                    Toast.makeText(getContext(), "Please enter My Perfect Money Account", Toast.LENGTH_SHORT).show();
                } else if (et_chname.getText().toString().length() == 0) {
                    Toast.makeText(getContext(), "Please enter My Perfect Money Name", Toast.LENGTH_SHORT).show();
                } else if (et_anumber.getText().toString().length() == 0) {
                    Toast.makeText(getContext(), "Please enter Valid Email ID", Toast.LENGTH_SHORT).show();
                }
//                else if (et_anumber.getText().toString().length() <19) {
//                    Toast.makeText(getContext(), "Please enter 16 digits Account Number", Toast.LENGTH_SHORT).show();
//                }
                else if (et_ifsccode.getText().toString().length()==0) {
                    Toast.makeText(getContext(), "Please enter Phone", Toast.LENGTH_SHORT).show();
                } else {
                    changeAccount();
                }
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_bitcoin.getText().toString().length() == 0) {
                    Toast.makeText(getContext(), "Please enter Bitcoin Address", Toast.LENGTH_SHORT).show();
                }  else {
                    changeBitcoin();
                }
            }
        });
        et_bitcoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                et_bitcoin.setCursorVisible(true);
            }
        });


        et_bitcoin.setCursorVisible(false);
//        et_bitcoin.setText(""+pHelper.getBitcoin());
//        et_bitcoin.setSelection(et_bitcoin.getText().length());
//        et_ifsccode.setFilters(new InputFilter[] {new InputFilter.AllCaps(),new InputFilter.LengthFilter(11)});
//        et_ifsccode.setKeyListener(new NumberKeyListener() {
//            @Override
//            public int getInputType() {
//                return InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS;
//            }
//            @Override
//            protected char[] getAcceptedChars() {
//                return new char[] {  '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',' ','A', 'B', 'C', 'D', 'E', 'F', 'G','H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P','Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y','Z'};
//            }
//        });
//        et_anumber.addTextChangedListener(new TextWatcher() {
//            private static final int TOTAL_SYMBOLS = 19; // size of pattern 0000-0000-0000-0000
//            private static final int TOTAL_DIGITS = 16; // max numbers of digits in pattern: 0000 x 4
//            private static final int DIVIDER_MODULO = 5; // means divider position is every 5th symbol beginning with 1
//            private static final int DIVIDER_POSITION = DIVIDER_MODULO - 1; // means divider position is every 4th symbol beginning with 0
//            private static final char DIVIDER = ' ';
//
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//                // noop
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                // noop
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                if (!isInputCorrect(s, TOTAL_SYMBOLS, DIVIDER_MODULO, DIVIDER)) {
//                    s.replace(0, s.length(), buildCorrecntString(getDigitArray(s, TOTAL_DIGITS), DIVIDER_POSITION, DIVIDER));
//                }
//            }
//
//            private boolean isInputCorrect(Editable s, int totalSymbols, int dividerModulo, char divider) {
//                boolean isCorrect = s.length() <= totalSymbols; // check size of entered string
//                for (int i = 0; i < s.length(); i++) { // chech that every element is right
//                    if (i > 0 && (i + 1) % dividerModulo == 0) {
//                        isCorrect &= divider == s.charAt(i);
//                    } else {
//                        isCorrect &= Character.isDigit(s.charAt(i));
//                    }
//                }
//                return isCorrect;
//            }
//
//            private String buildCorrecntString(char[] digits, int dividerPosition, char divider) {
//                final StringBuilder formatted = new StringBuilder();
//
//                for (int i = 0; i < digits.length; i++) {
//                    if (digits[i] != 0) {
//                        formatted.append(digits[i]);
//                        if ((i > 0) && (i < (digits.length - 1)) && (((i + 1) % dividerPosition) == 0)) {
//                            formatted.append(divider);
//                        }
//                    }
//                }
//
//                return formatted.toString();
//            }
//
//            private char[] getDigitArray(final Editable s, final int size) {
//                char[] digits = new char[size];
//                int index = 0;
//                for (int i = 0; i < s.length() && index < size; i++) {
//                    char current = s.charAt(i);
//                    if (Character.isDigit(current)) {
//                        digits[index] = current;
//                        index++;
//                    }
//                }
//                return digits;
//            }
//        });
        getBitcoin();
        return  view;
    }
    public void getBitcoin() {
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(getResources().getString(R.string.NoInternet), getActivity());
            return;
        }
        AnyUtils.startDialog(getActivity());
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.MYACCOUNT+"index");
        map.put(Constants.Params.USER_ID, pHelper.getUserId());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,1, this, this));

    }
    public void changeBitcoin() {
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(getResources().getString(R.string.NoInternet), getActivity());
            return;
        }
        et_bitcoin.setCursorVisible(false);
        AnyUtils.hideKeyboard(getActivity());
        AnyUtils.startDialog(getActivity());
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.MYACCOUNT+"index");
        map.put(Constants.Params.USER_ID, pHelper.getUserId());
        map.put(Constants.Params.BITCOIN, et_bitcoin.getText().toString());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,2, this, this));

    }
    public void changeAccount() {
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(getResources().getString(R.string.NoInternet), getActivity());
            return;
        }
        AnyUtils.hideKeyboard(getActivity());
        AnyUtils.startDialog(getActivity());
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.MYACCOUNT);
        map.put(Constants.Params.ID, pHelper.getUserId());
        map.put("bankname", et_bname.getText().toString());
        map.put("accountname", et_chname.getText().toString());
        map.put("accountno", et_anumber.getText().toString());
        map.put("ifsccode", et_ifsccode.getText().toString());
        map.put(Constants.Params.ACTION, "bankaccountupdate");
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,3, this, this));

    }

    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        if(!getActivity().isFinishing()){
            AnyUtils.showserver_popup(getActivity());
        }
    }
    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        switch (serviceCode) {
            case 1:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){
                        JSONObject data =jObj.getJSONObject("data");
                        et_bname.setText(data.getString("My Perfect Money Account"));
                        et_chname.setText(data.getString("My_Perfect_Money_Name"));
                        et_anumber.setText(data.getString("Email"));
                        et_ifsccode.setText(data.getString("Phone"));
                        et_bitcoin.setText(jObj.getJSONObject("bitcoin").getString("bitcoin"));
                    }else{
                        if(!getActivity().isFinishing()){
                            AnyUtils.showserver_popup(Constants.message,getActivity())	;
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            case 2:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){
                        JSONObject data =jObj.getJSONObject("data");
                        pHelper.putBitcoin(data.getString(Constants.Params.BITCOIN));
                    }else{
                        if(!getActivity().isFinishing()){
                            AnyUtils.showserver_popup(Constants.message,getActivity())	;
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            case 3:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){
                        Toast.makeText(getContext(), "Successfully Updated Bank Account", Toast.LENGTH_SHORT).show();
                    }else{
                        if(!getActivity().isFinishing()){
                            AnyUtils.showserver_popup(Constants.message,getActivity())	;
                        }
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }
}