package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.utilities.SlidingTabLayout;
import com.ecovesolutions.utilities.SlidingTabLayoutNotify;

/**
 * Created by Admin on 4/10/2017.
 */

public class MyAccount_Fragment extends Fragment {
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    private ViewPager viewPager;
    private SlidingTabLayout tabLayout;
    CharSequence Titles[]={"Account Details","Profile Management","Change Password"};
    CharSequence Titles1[]={"Account Details","Profile Management"};
    public static PreferenceHelper pHelper;

    int Numboftabs =3;
    int Numboftabs1 =2;
    public static MyAccount_Fragment newInstance() {
        MyAccount_Fragment mapFragment = new MyAccount_Fragment();
        return mapFragment;
    }
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.myaccount_fragment, container, false);
        pHelper = new PreferenceHelper(getActivity());


        viewPager = (ViewPager)view. findViewById(R.id.viewpager);
        tabLayout = (SlidingTabLayout)view. findViewById(R.id.tab_layout);

        if(pHelper.getLogin_type().matches("0")){
            PagerAdapter adapter = new PagerAdapter(getChildFragmentManager(),Titles, Numboftabs);
            viewPager.setAdapter(adapter);
        }else{
            PagerAdapter1 adapter = new PagerAdapter1(getChildFragmentManager(),Titles1, Numboftabs1);
            viewPager.setAdapter(adapter);
        }

        try {
            viewPager.setCurrentItem(0, true);
        } catch (Exception e) {
            // TODO: handle exception
            Log.getStackTraceString(e);
        }
        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                // TODO Auto-generated method stub
                return getResources().getColor(R.color.colorPrimary);
            }
            @Override
            public int getDividerColor(int position) {
                // TODO Auto-generated method stub
                return Color.TRANSPARENT;
            }
        });
        tabLayout.setViewPager(viewPager);
        return  view;
    }
    public class PagerAdapter extends FragmentStatePagerAdapter {
        CharSequence Titles[];
        int mNumOfTabs;

        public PagerAdapter(FragmentManager fm, CharSequence mTitles[], int NumOfTabs) {
            super(fm);
            this.Titles = mTitles;
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    Account_Details tab1 = new Account_Details().newInstance();
                    return tab1;
                case 1:
                    Profile tab2 = new Profile().newInstance();
                    return tab2;
                case 2:
                    Change_Password tab3 = new Change_Password().newInstance();
                    return tab3;
                default:
                    return null;
            }
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }
    public class PagerAdapter1 extends FragmentStatePagerAdapter {
        CharSequence Titles[];
        int mNumOfTabs;

        public PagerAdapter1(FragmentManager fm, CharSequence mTitles[], int NumOfTabs) {
            super(fm);
            this.Titles = mTitles;
            this.mNumOfTabs = NumOfTabs;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    Account_Details tab1 = new Account_Details().newInstance();
                    return tab1;
                case 1:
                    Profile tab2 = new Profile().newInstance();
                    return tab2;
                default:
                    return null;
            }
        }
        @Override
        public CharSequence getPageTitle(int position) {
            return Titles[position];
        }

        @Override
        public int getCount() {
            return mNumOfTabs;
        }
    }
}