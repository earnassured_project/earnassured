package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.adapter.WritetTestimonyAdapter;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.WritetTestimonyModel;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.utilities.SimpleDividerItemDecoration;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Admin on 4/14/2017.
 */

public class Write_Testimony extends Fragment implements AsyncTaskCompleteListener,Response.ErrorListener{
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    EditText et_testimony,et_requested;
    Button btn_save;
    Typeface gotham_light,gotham_book;

    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;
    WritetTestimonyAdapter mAdapter;
    RecyclerView rv_writetestimony;
    TextView tv_no_data;
    ProgressBar pb_internet;
    public static Write_Testimony newInstance() {
        Write_Testimony mapFragment = new Write_Testimony();
        return mapFragment;

    }
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.wirte_tesimony, container, false);
        pHelper = new PreferenceHelper(getContext());
        requestQueue = Volley.newRequestQueue(getContext());

        gotham_light=Typeface.createFromAsset(context.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(context.getAssets(), "fonts/gotham_book.otf");
        tv_no_data=(TextView)view.findViewById(R.id.tv_no_data);
        tv_no_data.setTypeface(AnyUtils.getGothamBook(context));
        pb_internet=(ProgressBar)view.findViewById(R.id.pb_internet);

        rv_writetestimony = (RecyclerView) view.findViewById(R.id.rv_writetestimony);
        rv_writetestimony.setNestedScrollingEnabled(false);
        mAdapter=new WritetTestimonyAdapter(getActivity(),R.layout.writetestimonychild, Constants.writetTestimonyModels);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);
        rv_writetestimony.setLayoutManager(mLayoutManager);
        rv_writetestimony.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        rv_writetestimony.setAdapter(mAdapter);

        et_testimony = (EditText) view.findViewById(R.id.et_testimony);
        et_requested=(EditText) view.findViewById(R.id.et_requested);
        btn_save = (Button) view.findViewById(R.id.btn_save);

        et_testimony.setTypeface(gotham_light);
        et_requested.setTypeface(gotham_light);
        btn_save.setTypeface(gotham_light);

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_testimony.getText().length()==0){
                    Toast.makeText(context, "Please enter Write your testimony", Toast.LENGTH_SHORT).show();
                }else{
                    postWritetestimony();
                }
            }
        });
        if(Constants.writetTestimonyModels.size()==0) {
            getWritetestimony();
        }else{
            pb_internet.setVisibility(View.GONE);
            tv_no_data.setVisibility(View.GONE);
            rv_writetestimony.setVisibility(View.VISIBLE);
        }

        return  view;
    }
    public void getWritetestimony(){
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
//        AnyUtils.startDialog(getActivity());
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.RECEIVEHELP+"writetestimony");
        map.put(Constants.Params.USER_NAME,pHelper.getName());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,1, this, this));
    }
    public void postWritetestimony(){
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
        AnyUtils.hideKeyboard(getActivity());
        AnyUtils.startDialog(getActivity());
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.RECEIVEHELP+"writetestimony");
        map.put(Constants.Params.USER_NAME,pHelper.getName());
        map.put(Constants.Params.MESSAGE,et_testimony.getText().toString());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,1, this, this));
    }

    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        if(!((Activity) context).isFinishing()){
            AnyUtils.showserver_popup(getActivity());
        }
    }
    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        pb_internet.setVisibility(View.GONE);
        switch (serviceCode) {
            case 1:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){
                        et_testimony.setText("");
                        et_requested.setText("");
                        Constants.writetTestimonyModels.clear();
                        tv_no_data.setVisibility(View.GONE);
                        rv_writetestimony.setVisibility(View.VISIBLE);
                        JSONArray data_array=jObj.getJSONArray("data");
                        for (int i = 0; i < data_array.length(); i++) {
                            JSONObject obj1=data_array.getJSONObject(i);
                            String test_id=obj1.getString("test_id");
                            String username=obj1.getString("username");
                            String message=obj1.getString("message");
                            String status=obj1.getString("status");
                            String amount="";
                            WritetTestimonyModel model=new WritetTestimonyModel(test_id,username,message,amount,status);
                            Constants.writetTestimonyModels.add(model);
                        }
                    }else{
                        tv_no_data.setVisibility(View.VISIBLE);
                        tv_no_data.setText(Constants.message);
                        rv_writetestimony.setVisibility(View.GONE);
                        AnyUtils.stopDialog(getActivity());
                    }
                    mAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    AnyUtils.stopDialog(getActivity());
                    e.printStackTrace();
                }
                break;

            default:
                break;
        }
    }

}
