package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.adapter.PaymentConfirmationAdapter;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.PaymentConfirmationModel;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Admin on 4/14/2017.
 */

public class Payment_Confirmation extends Fragment implements AsyncTaskCompleteListener,Response.ErrorListener{
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;

    ListView lv_paymentconfirmation;
    PaymentConfirmationAdapter mAdapter;
    TextView tv_no_data;
    ProgressBar pb_internet;
    RelativeLayout rl_main;
    public static Payment_Confirmation newInstance() {
        Payment_Confirmation mapFragment = new Payment_Confirmation();
        return mapFragment;
    }
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.payment_confirmation, container, false);
        pHelper = new PreferenceHelper(getContext());
        requestQueue = Volley.newRequestQueue(getContext());

        rl_main=(RelativeLayout)view.findViewById(R.id.rl_main);
        lv_paymentconfirmation = (ListView) view.findViewById(R.id.lv_paymentconfirmation);
        tv_no_data=(TextView)view.findViewById(R.id.tv_no_data);
        tv_no_data.setTypeface(AnyUtils.getGothamBook(context));
        pb_internet=(ProgressBar)view.findViewById(R.id.pb_internet);

        mAdapter = new PaymentConfirmationAdapter(getActivity(), R.layout.payment_child, Constants.paymentConfirmationModels,rl_main);
        lv_paymentconfirmation.setAdapter(mAdapter);
        if(Constants.paymentConfirmationModels.size()==0) {
            getTransaction();
        }else{
            tv_no_data.setVisibility(View.GONE);
            lv_paymentconfirmation.setVisibility(View.VISIBLE);
        }
        return  view;
    }
    public void getTransaction() {
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.RECEIVEHELP+"payment");
        map.put(Constants.Params.USER_ID, pHelper.getUserId());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map, 1, this, this));
    }

    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        if (!((Activity) context).isFinishing()) {
            AnyUtils.showserver_popup(getActivity());
        }
    }
    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        pb_internet.setVisibility(View.GONE);
        switch (serviceCode) {
            case 1:
                try {
                    JSONObject jObj = new JSONObject(response);
                    Constants.status = jObj.getString(Constants.Params.STATUS);
                    Constants.message = jObj.getString(Constants.Params.MESSAGE);
                    if (Constants.status.matches("true")) {
                        Constants.payhistory_ModelArrayList.clear();
                        tv_no_data.setVisibility(View.GONE);
                        lv_paymentconfirmation.setVisibility(View.VISIBLE);
                        JSONArray data_array = jObj.getJSONArray("data");

                        for (int i = 0; i < data_array.length(); i++) {
                            JSONObject obj1 = data_array.getJSONObject(i);
                            String ph_id = obj1.getString("provide_id");
                            String match_date = obj1.getString("match_date");
                            String expire_date = obj1.getString("expire_date");
                            String amount_off  = obj1.getString("amount_off");
                            String username = obj1.getString("paid_by");

                            String type = "";
                            String VIEW =obj1.getString("PAYMENT_PROOF");
                            String ACTION = "";
                            String STATUS = obj1.getString("Contact Recipient");
                            String profile_pic="";
                            PaymentConfirmationModel model = new PaymentConfirmationModel(ph_id, match_date, expire_date, amount_off, username, type,VIEW,ACTION,STATUS,profile_pic);
                            Constants.paymentConfirmationModels.add(model);

                        }

                    } else {
                        tv_no_data.setVisibility(View.VISIBLE);
                        tv_no_data.setText(Constants.message);
                        lv_paymentconfirmation.setVisibility(View.GONE);
                    }
                    if(Constants.paymentConfirmationModels.size()==0) {
                        tv_no_data.setVisibility(View.VISIBLE);
                        lv_paymentconfirmation.setVisibility(View.GONE);
                    }else{
                        tv_no_data.setVisibility(View.GONE);
                        lv_paymentconfirmation.setVisibility(View.VISIBLE);
                    }
                    mAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;

            default:
                break;
        }
    }

}
