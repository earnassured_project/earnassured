package com.ecovesolutions.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.adapter.TransactionAdapter;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.TransactionModel;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.HashMap;

/**
 * Created by Admin on 4/14/2017.
 */

public class Transaction_History extends Fragment implements AsyncTaskCompleteListener,Response.ErrorListener  {
    public static FragmentActivity context;
    private Bundle mBundle;
    Typeface gotham_light,gotham_book;
    public static PreferenceHelper pHelper;
    public static RequestQueue  requestQueue;
    TransactionAdapter mAdapter;
    ListView lv_transaction;
    TextView tv_transhistory;
    private View view;
    TextView tv_no_data;
    ProgressBar pb_internet;
    public static Transaction_History newInstance() {
        Transaction_History mapFragment = new Transaction_History();
        return mapFragment;
    }
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUserVisibleHint(false);
        mBundle = savedInstanceState;

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.transaction_historylist, container, false);
        pHelper = new PreferenceHelper(getContext());
        requestQueue = Volley.newRequestQueue(getContext());

        tv_no_data=(TextView)view.findViewById(R.id.tv_no_data);
        tv_no_data.setTypeface(AnyUtils.getGothamBook(context));
        pb_internet=(ProgressBar)view.findViewById(R.id.pb_internet);

        tv_transhistory = (TextView)view.findViewById(R.id.tv_transhistory);
        tv_transhistory.setTypeface(AnyUtils.getGothamBook(context));

        lv_transaction = (ListView) view.findViewById(R.id.lv_transaction);
        mAdapter=new TransactionAdapter(getActivity(), R.layout.transaction_history, Constants.transactionModelArrayList);
        lv_transaction.setAdapter(mAdapter);

        if(Constants.transactionModelArrayList.size()==0) {
//            getRequestStatus(3);
                getTransaction(3);
        }else{
            pb_internet.setVisibility(View.GONE);
            tv_no_data.setVisibility(View.GONE);
            lv_transaction.setVisibility(View.VISIBLE);
        }

        return  view;
    }


    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            if(Constants.transactionModelArrayList.size()==0) {
//                getRequestStatus();
//                getTransaction(3);
            }else{
                pb_internet.setVisibility(View.GONE);
                tv_no_data.setVisibility(View.GONE);
                lv_transaction.setVisibility(View.VISIBLE);
            }
        }
    }
    public void getRequestStatus(int code) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.URL_BEST+"/provide_api/transactionhistory/"+pHelper.getUserId());
        requestQueue.add(new VolleyHttpRequest(Request.Method.GET, map, code, this, this));
    }

    public void getTransaction(int code){
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
//        AnyUtils.startDialog(getActivity());
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.GIVEHELP+"transactionhistory");
        map.put(Constants.Params.USER_ID,pHelper.getUserId());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,code, this, this));
    }

    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        if(!((Activity) context).isFinishing()){
            AnyUtils.showserver_popup(getActivity());
        }
    }
    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        pb_internet.setVisibility(View.GONE);
        switch (serviceCode) {
            case 3:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){
                        Constants.transactionModelArrayList.clear();
                        tv_no_data.setVisibility(View.GONE);
                        lv_transaction.setVisibility(View.VISIBLE);
                        JSONArray data_array=jObj.getJSONArray("data");

                        for (int i = 0; i < data_array.length(); i++) {
                            JSONObject obj1=data_array.getJSONObject(i);
                            String id = obj1.getString("provide_id");
                            String amount_off = obj1.getString("amount_off");
                            String outstanding = obj1.getString("outstanding");
                            String total_confirmed = obj1.getString("one_time_bonus");
                            String req_amount = obj1.getString("req_amount");
                            String yield_amount = obj1.getString("yield_amount");
                            String start_date = obj1.getString("start_date");
                            String end_date = obj1.getString("end_date");

                            TransactionModel model = new TransactionModel(id, start_date, amount_off, outstanding, total_confirmed, req_amount, yield_amount, end_date);
                            Constants.transactionModelArrayList.add(model);

                        }

                    }else{
                        tv_no_data.setVisibility(View.VISIBLE);
                        tv_no_data.setText(Constants.message);
                        lv_transaction.setVisibility(View.GONE);
//                        if(!((Activity) context).isFinishing()){
//                            AnyUtils.showserver_popup(Constants.message,getContext())	;
//                        }
                        AnyUtils.stopDialog(getActivity());
                    }
                    mAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    AnyUtils.stopDialog(getActivity());
                    e.printStackTrace();
                }
                break;

            default:
                break;
        }
    }

}