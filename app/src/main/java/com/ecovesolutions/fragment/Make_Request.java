package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.adapter.MakeRequestAdapter;
import com.ecovesolutions.adapter.PaymentAdapter;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.MakeRequestModel;
import com.ecovesolutions.model.PaymentModel;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Admin on 4/14/2017.
 */

public class Make_Request extends Fragment implements AsyncTaskCompleteListener,Response.ErrorListener{
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;
    ListView lv_Make_Request;
    MakeRequestAdapter mAdapter;
    TextView tv_no_data;
    ProgressBar pb_internet;

    public static Make_Request newInstance() {
        Make_Request mapFragment = new Make_Request();
        return mapFragment;
    }
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.make_request, container, false);
        pHelper = new PreferenceHelper(getContext());
        requestQueue = Volley.newRequestQueue(getContext());


        lv_Make_Request = (ListView) view.findViewById(R.id.lv_Make_Request);
        tv_no_data=(TextView)view.findViewById(R.id.tv_no_data);
        tv_no_data.setTypeface(AnyUtils.getGothamBook(context));
        pb_internet=(ProgressBar)view.findViewById(R.id.pb_internet);

//        View headerView = getActivity().getLayoutInflater().inflate(R.layout.requesthelpheader, null);
//        lv_Make_Request.addHeaderView(headerView);
        TextView tv_rules_receive = (TextView) view.findViewById(R.id.tv_rules_receive);
        TextView tv_rules_msg = (TextView) view.findViewById(R.id.tv_rules_msg);
        tv_rules_receive.setTypeface(AnyUtils.getGothamLight(context));
        tv_rules_msg.setTypeface(AnyUtils.getGothamLight(context));

        mAdapter = new MakeRequestAdapter(getActivity(), R.layout.make_request_child, Constants.makeRequestModels);
        lv_Make_Request.setAdapter(mAdapter);


        if(Constants.makeRequestModels.size()==0) {
            getTransaction(3);
        }else{
            tv_no_data.setVisibility(View.GONE);
            lv_Make_Request.setVisibility(View.VISIBLE);
        }
        setListViewHeightBasedOnChildren(lv_Make_Request);
        return  view;
    }
    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {

        }
    }
    public void getTransaction(int code) {
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.RECEIVEHELP+"index");
        map.put(Constants.Params.USER_ID, pHelper.getUserId());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map, code, this, this));
    }
    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        if (!((Activity) context).isFinishing()) {
            AnyUtils.showserver_popup(getActivity());
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        pb_internet.setVisibility(View.GONE);
        switch (serviceCode) {
            case 3:
                try {
                    JSONObject jObj = new JSONObject(response);
                    Constants.status = jObj.getString(Constants.Params.STATUS);
                    Constants.message = jObj.getString(Constants.Params.MESSAGE);
                    if (Constants.status.matches("true")) {
                        Constants.makeRequestModels.clear();
                        JSONArray data_array = jObj.getJSONArray("data");
                        for (int i = 0; i < data_array.length(); i++) {
                            JSONObject obj1 = data_array.getJSONObject(i);
                            String ph_id = obj1.getString("provide_id");
                            String amount_off = obj1.getString("amount_off");
                            String end_date = obj1.getString("outstanding");
                            String yield_amount2  = obj1.getString("yield_amount");
                            String bonus = obj1.getString("bonus");
                            String one_time_bonus = obj1.getString("one_time_bonus");
                            String request = obj1.getString("req_amount");
                            String status = obj1.getString("ACTION");

                            MakeRequestModel model = new MakeRequestModel(ph_id, end_date, amount_off, yield_amount2, bonus, request,status,one_time_bonus);
                            Constants.makeRequestModels.add(model);
                        }

                    } else {
                        tv_no_data.setVisibility(View.VISIBLE);
                        tv_no_data.setText(Constants.message);
                        lv_Make_Request.setVisibility(View.GONE);
                    }
                    if(Constants.makeRequestModels.size()==0) {
                        tv_no_data.setVisibility(View.VISIBLE);
                        lv_Make_Request.setVisibility(View.GONE);
                    }else{
                        tv_no_data.setVisibility(View.GONE);
                        lv_Make_Request.setVisibility(View.VISIBLE);
                    }

                    mAdapter.notifyDataSetChanged();
                    setListViewHeightBasedOnChildren(lv_Make_Request);
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        MakeRequestAdapter listAdapter = (MakeRequestAdapter) listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
