package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.adapter.Customadapter;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Admin on 4/14/2017.
 */

public class New_Donation extends Fragment implements AsyncTaskCompleteListener,Response.ErrorListener {
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    ListView autocomplete;
    TextView tv_text, tv_testnews,tv_rules,tv_minimum,tv_max,tv_amount,tv_payment;
    EditText et_pay;
    Button btn_confirm;
    String type="bitcoin";
    Typeface gotham_light, gotham_book;
    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;

    public static New_Donation newInstance() {
        New_Donation mapFragment = new New_Donation();
        return mapFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.new_donation, container, false);
        pHelper = new PreferenceHelper(getContext());
        requestQueue = Volley.newRequestQueue(getContext());
        tv_rules=(TextView) view.findViewById(R.id.tv_rules);
        autocomplete = (ListView) view.findViewById(R.id.autoCompleteTextView1);
        autocomplete = (ListView) view.findViewById(R.id.autoCompleteTextView1);
        tv_text = (TextView) view.findViewById(R.id.tv_text);
        tv_testnews = (TextView) view.findViewById(R.id.tv_testnews);
        et_pay = (EditText) view.findViewById(R.id.et_pay);
        tv_minimum=(TextView) view.findViewById(R.id.tv_minimum);
        tv_max=(TextView) view.findViewById(R.id.tv_max);
        tv_payment=(TextView) view.findViewById(R.id.tv_payment);
        tv_amount=(TextView) view.findViewById(R.id.tv_amount);
        btn_confirm = (Button) view.findViewById(R.id.btn_confirm);
        //font style
        gotham_light = Typeface.createFromAsset(context.getAssets(), "fonts/gotham_light.ttf");
        gotham_book = Typeface.createFromAsset(context.getAssets(), "fonts/gotham_book.otf");

        tv_payment.setTypeface(gotham_book);
        tv_amount.setTypeface(gotham_book);
        tv_max.setTypeface(gotham_light);
        et_pay.setTypeface(gotham_light);
        btn_confirm.setTypeface(gotham_light);
        tv_rules.setTypeface(gotham_light);
        tv_minimum.setTypeface(gotham_light);
        tv_testnews.setTypeface(gotham_book);
        tv_text.setTypeface(gotham_light);
        tv_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (autocomplete.getVisibility() == View.VISIBLE) {
                    autocomplete.setVisibility(View.GONE);
                } else {
                    // Either gone or invisible
                    autocomplete.setVisibility(View.VISIBLE);
                }
            }

        });
        String[] arr = {"BITCOIN PAYMENT($)", "PERFECT MONEY($)"};
        Customadapter adapter = new Customadapter(getActivity(), arr);
        autocomplete.setAdapter(adapter);
        setListViewHeightBasedOnChildren(autocomplete);
        autocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = (String) parent.getItemAtPosition(position);
                autocomplete.setVisibility(View.GONE);
                tv_text.setText(selectedItem);
                if(selectedItem.equalsIgnoreCase("BITCOIN PAYMENT($)")){
                    type="bitcoin";
                    Log.d("ttt","a="+type);
                }else if(selectedItem.equalsIgnoreCase("PERFECT MONEY($)")){
                    type="perfect money";
                    Log.d("ttt","b="+type);
                }
            }
        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_pay.getText().length()==0){
                    Toast.makeText(getContext(), "Please Enter Amount", Toast.LENGTH_SHORT).show();
                }else{
                    postNewDonation();
                }

            }
        });
        getNewDonation();
        return view;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.MATCH_PARENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount()));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
    public void getNewDonation() {
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.GIVEHELP);
        map.put(Constants.Params.USER_ID, pHelper.getUserId());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map, 1, this, this));
    }
    public void postNewDonation() {
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
        AnyUtils.startDialog(getActivity());
        AnyUtils.hideKeyboard(getActivity());
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.GIVEHELP+"index");
        map.put(Constants.Params.USER_ID, pHelper.getUserId());
        map.put("channel", type);
        map.put("amount", et_pay.getText().toString());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map, 2, this, this));
    }

    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        if (!((Activity) context).isFinishing()) {
            AnyUtils.showserver_popup(getActivity());
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        switch (serviceCode) {
            case 1:
                try {
                    JSONObject jObj = new JSONObject(response);
                    Constants.status = jObj.getString(Constants.Params.STATUS);
                    Constants.message = jObj.getString(Constants.Params.MESSAGE);
                    if (Constants.status.matches("true")) {
                        JSONObject data =jObj.getJSONObject("data");
                        tv_minimum.setText(context.getResources().getString(R.string.min)+data.getString("Minimum Lend Help")+")");
                        tv_max.setText(context.getResources().getString(R.string.max)+data.getString("Maximum Lend Help")+")");

                    } else {
                        if (!((Activity) context).isFinishing()) {
                            AnyUtils.showserver_popup(Constants.message, getContext());
                        }
                        AnyUtils.stopDialog(getActivity());
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    AnyUtils.stopDialog(getActivity());
                    e.printStackTrace();
                }
                break;
            case 2:
                try {
                    JSONObject jObj = new JSONObject(response);
                    Constants.status = jObj.getString(Constants.Params.STATUS);
                    Constants.message = jObj.getString(Constants.Params.MESSAGE);
                    if (Constants.status.matches("true")) {
                        et_pay.setText("");
                        JSONObject data =jObj.getJSONObject("data");
                        tv_minimum.setText(context.getResources().getString(R.string.min)+data.getString("Minimum Lend Help")+")");
                        tv_max.setText(context.getResources().getString(R.string.max)+data.getString("Maximum Lend Help")+")");
                        GiveHelp_Fragment.viewPager.setCurrentItem(2, true);
                    } else {
                        if (!((Activity) context).isFinishing()) {
                            AnyUtils.showserver_popup(Constants.message, getContext());
                        }
                        AnyUtils.stopDialog(getActivity());
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    AnyUtils.stopDialog(getActivity());
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }
}