package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.adapter.AllpaymnetAdapter;
import com.ecovesolutions.adapter.TransactionAdapter;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.Allpayment_Model;
import com.ecovesolutions.model.TransactionModel;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Admin on 4/14/2017.
 */

public class All_Payments  extends Fragment implements AsyncTaskCompleteListener,Response.ErrorListener,SwipeRefreshLayout.OnRefreshListener{
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    Typeface gotham_light, gotham_book;
    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;
    ListView  lv_allpayment;
    AllpaymnetAdapter mAdapter;
    TextView tv_no_data;
    ProgressBar pb_internet;
    private SwipeRefreshLayout swipeRefreshLayout;
    public static All_Payments newInstance() {
        All_Payments mapFragment = new All_Payments();
        return mapFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUserVisibleHint(false);
        mBundle = savedInstanceState;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.allpaymnet_list, container, false);
        pHelper = new PreferenceHelper(getContext());
        requestQueue = Volley.newRequestQueue(getContext());

        tv_no_data=(TextView)view.findViewById(R.id.tv_no_data);
        tv_no_data.setTypeface(AnyUtils.getGothamBook(context));
        lv_allpayment = (ListView) view.findViewById(R.id.lv_allpayment);
        pb_internet=(ProgressBar)view.findViewById(R.id.pb_internet);
        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {

                                        if(Constants.allpaymnetModelArrayList.size()==0) {
                                            swipeRefreshLayout.setRefreshing(true);
                                            getTransaction(3);
//                                            getRequestStatus(3);
                                        }else{
                                            swipeRefreshLayout.setRefreshing(false);
                                            pb_internet.setVisibility(View.GONE);
                                            tv_no_data.setVisibility(View.GONE);
                                            swipeRefreshLayout.setVisibility(View.VISIBLE);
                                        }
                                    }
                                }
        );
        swipeRefreshLayout.setColorSchemeResources(
                R.color.app_color1,
                R.color.app_color2,
                R.color.colorPrimary);
        mAdapter = new AllpaymnetAdapter(getActivity(), R.layout.allpayment_child, Constants.allpaymnetModelArrayList);
        lv_allpayment.setAdapter(mAdapter);

        return view;
    }
    @Override
    public void onRefresh() {

        getTransaction(3);
//        getRequestStatus(3);
    }
    public void getRequestStatus(int code) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.URL_BEST+"/provide_api/allpayment/"+pHelper.getUserId());
        requestQueue.add(new VolleyHttpRequest(Request.Method.GET, map, code, this, this));
    }
    public void getTransaction(int code) {
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
        swipeRefreshLayout.setRefreshing(true);
//        AnyUtils.startDialog(getActivity());
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.GIVEHELP+"allpayment");
        map.put(Constants.Params.USER_ID, pHelper.getUserId());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map, code, this, this));
    }

    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        if (!((Activity) context).isFinishing()) {
            AnyUtils.showserver_popup(getActivity());
        }
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        swipeRefreshLayout.setRefreshing(false);
        pb_internet.setVisibility(View.GONE);
        AnyUtils.stopDialog(getActivity());
        switch (serviceCode) {
            case 3:
                try {
                    JSONObject jObj = new JSONObject(response);
                    Constants.status = jObj.getString(Constants.Params.STATUS);
                    Constants.message = jObj.getString(Constants.Params.MESSAGE);
                    if (Constants.status.matches("true")) {
                        Constants.allpaymnetModelArrayList.clear();
                        tv_no_data.setVisibility(View.GONE);
                        swipeRefreshLayout.setVisibility(View.VISIBLE);
                        JSONArray data_array = jObj.getJSONArray("data");

                        for (int i = 0; i < data_array.length(); i++) {
                            JSONObject obj1 = data_array.getJSONObject(i);
                            String id = obj1.getString("pair_id");
                            String match_date = obj1.getString("match_date");
                            String amount_off = obj1.getString("amount_off");
                            String payment_confirm = obj1.getString("payment_confirm");
                            String username = obj1.getString("username");
                            String bitcoin = obj1.getString("bitcoin");

                            Allpayment_Model model = new Allpayment_Model(id, match_date, amount_off, payment_confirm, username, bitcoin);
                            Constants.allpaymnetModelArrayList.add(model);

                        }

                    } else {
                        tv_no_data.setVisibility(View.VISIBLE);
                        tv_no_data.setText(Constants.message);
                        swipeRefreshLayout.setVisibility(View.GONE);
//                        if (!((Activity) context).isFinishing()) {
//                            AnyUtils.showserver_popup(Constants.message, getContext());
//                        }
                        AnyUtils.stopDialog(getActivity());
                    }
                    if(Constants.allpaymnetModelArrayList.size()==0) {
                        tv_no_data.setVisibility(View.VISIBLE);
                        lv_allpayment.setVisibility(View.GONE);
                    }else{
                        tv_no_data.setVisibility(View.GONE);
                        lv_allpayment.setVisibility(View.VISIBLE);
                    }
                    mAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    AnyUtils.stopDialog(getActivity());
                    e.printStackTrace();
                }
                break;

            default:
                break;
        }
    }

}
