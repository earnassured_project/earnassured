package com.ecovesolutions.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.ecovesolutions.adapter.NavDrawAdapter;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.Model_Drawer;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.CircleImageView;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;

import java.util.ArrayList;

/**
 * @author Satheeshkumar
 **/
public class FragmentDrawer extends Fragment implements AsyncTaskCompleteListener, ErrorListener {
	
	private RequestQueue requestQueue;
	PreferenceHelper pHelper;
	private View headerView;
	public static TextView tvMenuName;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private NavDrawAdapter adapter;
    ListView listDrawer;
    private View containerView;
    private FragmentDrawerListener drawerListener;
    public static CircleImageView ivMenuProfile;
    ArrayList<Model_Drawer> arr_list=new ArrayList<Model_Drawer>();
    private boolean mUserLearnedDrawer;
    public static final String PREF_FILE_NAME="test";
	public static final String KEY_USER_LEAREND_DRAWER="user";
	private boolean mFromSavedInstanceState;
    Typeface gotham_light,gotham_book;
    public void setDrawerListener(FragmentDrawerListener listener) {
        this.drawerListener = listener;
    }
 
    public void getData() {
        // preparing navigation drawer items
    	arr_list.clear();
        arr_list.add(new Model_Drawer(1,getString(R.string.dashboard),R.mipmap.dashicon_grey));
        arr_list.add(new Model_Drawer(2,getString(R.string.myreferrals),R.mipmap.refferal_icon));
        arr_list.add(new Model_Drawer(3,getString(R.string.myaccount),R.mipmap.user_grey));
        arr_list.add(new Model_Drawer(4,getString(R.string.givehelp),R.mipmap.question_mark));
        arr_list.add(new Model_Drawer(5,getString(R.string.receivehelp),R.mipmap.question_icon));
        arr_list.add(new Model_Drawer(6,getString(R.string.givetestimony),R.mipmap.triangle));
        arr_list.add(new Model_Drawer(7,getString(R.string.news),R.mipmap.triangle));
        arr_list.add(new Model_Drawer(8,getString(R.string.dt_admin),R.mipmap.user_grey));
        arr_list.add(new Model_Drawer(9,getString(R.string.dt_charity),R.mipmap.dollar_icon));
        arr_list.add(new Model_Drawer(10,getString(R.string.contact_us),R.mipmap.email_icon));
        arr_list.add(new Model_Drawer(11,getString(R.string.logout),R.mipmap.logout_icon));
    }
    
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
    	// TODO Auto-generated method stub
    	super.onCreate(savedInstanceState);
    	
    	mUserLearnedDrawer=Boolean.valueOf(readFramPreferences(getActivity(),KEY_USER_LEAREND_DRAWER,"true"));
		if(savedInstanceState!=null){
			mFromSavedInstanceState=true;
		}
    }
    
    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    	// TODO Auto-generated method stub
    	View layout = inflater.inflate(R.layout.fragment_nav_drawer, container, false);
    	pHelper = new PreferenceHelper(getActivity());
    	requestQueue = Volley.newRequestQueue(getActivity());
        gotham_light= Typeface.createFromAsset(getActivity().getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(getActivity().getAssets(), "fonts/gotham_book.otf");

        listDrawer = (ListView) layout.findViewById(R.id.left_drawer);

        headerView = getActivity().getLayoutInflater().inflate(R.layout.menu_drawer, null);
        listDrawer.addHeaderView(headerView);
        ivMenuProfile = (CircleImageView) headerView.findViewById(R.id.userProfilePic);
        tvMenuName = (TextView) headerView.findViewById(R.id.UserName);
        tvMenuName.setTypeface(gotham_book);
        tvMenuName.setText(""+pHelper.getName());

        String image=pHelper.getProfileUrl();
        if(image != null && !image.isEmpty()){
              Glide.with(getActivity())
                    .load(image)
                    .asBitmap()
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .priority(Priority.IMMEDIATE)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                            ivMenuProfile.setImageBitmap(resource);
                        }
                    });

        }else {
            try {
                ivMenuProfile.setBackgroundResource(R.mipmap.pro_iv);
                ivMenuProfile.setScaleType(ScaleType.CENTER_CROP);
            } catch (Exception e) {
                // TODO: handle exception
                Log.e("exception", Log.getStackTraceString(e));
            }
        }


 
        getData();
        adapter = new NavDrawAdapter(getContext(),R.layout.items_nav, arr_list);
        getActivity().runOnUiThread(new Runnable() {			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				listDrawer.setAdapter(adapter);
			}
		});



		listDrawer.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,final int position, long id) {
				// TODO Auto-generated method stub
                mDrawerLayout.closeDrawer(containerView);

				 drawerListener.onDrawerItemSelected(view, position);

			}       	
		});       
        return layout;
    }

    @Override
	public void onErrorResponse(VolleyError arg0) {
		// TODO Auto-generated method stub
		AnyUtils.stopDialog(getActivity());
		AnyUtils.showserver_popup(getActivity());

		
	}

	@SuppressWarnings("static-access")
	@Override
	public void onTaskCompleted(String response, int serviceCode) {
		// TODO Auto-generated method stub
		AnyUtils.stopDialog(getActivity());
		switch (serviceCode) {
		case 1:


			break;
		default:
			break;
		}
	}
	

    
   
    
    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            } 
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            } 
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        }; 
        if(!mUserLearnedDrawer &&!mFromSavedInstanceState){
    		mDrawerLayout.openDrawer(containerView);
    	}
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        }); 
    }    
    public static void saveToPreferences(Context context,String preferenceName,String mUserLearnedDrawer){
    	SharedPreferences sharedPreferences=context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    	SharedPreferences.Editor editor=sharedPreferences.edit();
    	editor.putString(preferenceName, mUserLearnedDrawer);
    	editor.apply();
    }
    public static String readFramPreferences(Context context,String preferenceName,String preferenceValue){
    	SharedPreferences sharedPreferences=context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    	return sharedPreferences.getString(preferenceName, preferenceValue);    	
    }    
    public static interface ClickListener {
        public void onClick(View view, int position); 
        public void onLongClick(View view, int position);
    }   
    public interface FragmentDrawerListener {
        public void onDrawerItemSelected(View view, int position);
    }   

}


