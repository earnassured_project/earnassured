package com.ecovesolutions.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ecovesolutions.adapter.ReferralBonusAdapter;
import com.ecovesolutions.adapter.ViewReferralAdapter;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;

/**
 * Created by Admin on 4/13/2017.
 */

public class ReferralBonus_Fragment extends Fragment {
    public static FragmentActivity context;
    Typeface gotham_light,gotham_book;
    public static  ReferralBonusAdapter mAdapter;
    public static ListView lv_referralbonus;
    public static TextView tv_no_data;
    public static ProgressBar pb_internet;
    public void onCreate(@Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }
    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    @Nullable
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.referralbonus_fragment, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        gotham_light=Typeface.createFromAsset(getContext().getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(getContext().getAssets(), "fonts/gotham_book.otf");
        tv_no_data=(TextView)view.findViewById(R.id.tv_no_data);
        tv_no_data.setTypeface(AnyUtils.getGothamBook(context));
        pb_internet=(ProgressBar)view.findViewById(R.id.pb_internet);


        lv_referralbonus=(ListView)view.findViewById(R.id.lv_referralbonus);
        mAdapter=new ReferralBonusAdapter(getActivity(), R.layout.referralbonusadapter, Constants.referralbonus_models);
        lv_referralbonus.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        // TODO Auto-generated method stub
        super.onAttach(context);
    }
    @Override
    public void onDetach() {
        // TODO Auto-generated method stub
        super.onDetach();
    }
}
