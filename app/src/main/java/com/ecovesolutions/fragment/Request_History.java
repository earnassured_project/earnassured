package com.ecovesolutions.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.adapter.AllpaymnetAdapter;
import com.ecovesolutions.adapter.ReferralBonusAdapter;
import com.ecovesolutions.adapter.RequestHistoryAdapter;
import com.ecovesolutions.earnassured.R;

import com.ecovesolutions.model.Reqhistory_Model;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Admin on 4/14/2017.
 */

public class Request_History extends Fragment implements AsyncTaskCompleteListener,Response.ErrorListener{
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    Typeface gotham_light, gotham_book;
    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;
    ListView  lv_requesthistory;
    RequestHistoryAdapter mAdapter;
    TextView tv_no_data;
    ProgressBar pb_internet;
    public static Request_History newInstance() {
        Request_History mapFragment = new Request_History();
        return mapFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUserVisibleHint(false);
        mBundle = savedInstanceState;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.reqest_history, container, false);
        pHelper = new PreferenceHelper(getContext());
        requestQueue = Volley.newRequestQueue(getContext());


        lv_requesthistory = (ListView) view.findViewById(R.id.lv_requesthistory);
        tv_no_data=(TextView)view.findViewById(R.id.tv_no_data);
        tv_no_data.setTypeface(AnyUtils.getGothamBook(context));
        pb_internet=(ProgressBar)view.findViewById(R.id.pb_internet);



        mAdapter = new RequestHistoryAdapter(getActivity(), R.layout.requesthistory_child, Constants.Reqhistory_ModelArrayList);
        lv_requesthistory.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible) {
            if(Constants.Reqhistory_ModelArrayList.size()==0) {
                getTransaction(3);
            }else{
                tv_no_data.setVisibility(View.GONE);
                lv_requesthistory.setVisibility(View.VISIBLE);
            }

        }
    }
    public void getTransaction(int code) {
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
//        AnyUtils.startDialog(getActivity());
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.RECEIVEHELP+"requesthistory");
        map.put(Constants.Params.USER_ID, pHelper.getUserId());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map, code, this, this));
    }

    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        if (!((Activity) context).isFinishing()) {
            AnyUtils.showserver_popup(getActivity());
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        pb_internet.setVisibility(View.GONE);
        switch (serviceCode) {
            case 3:
                try {
                    JSONObject jObj = new JSONObject(response);
                    Constants.status = jObj.getString(Constants.Params.STATUS);
                    Constants.message = jObj.getString(Constants.Params.MESSAGE);
                    if (Constants.status.matches("true")) {
                        Constants.Reqhistory_ModelArrayList.clear();
                        tv_no_data.setVisibility(View.GONE);
                        lv_requesthistory.setVisibility(View.VISIBLE);
                        JSONArray data_array = jObj.getJSONArray("data");

                        for (int i = 0; i < data_array.length(); i++) {
                            JSONObject obj1 = data_array.getJSONObject(i);
                            String sno = obj1.getString("s_no");
                            String request_date = obj1.getString("LEND_HELP_AMOUNT");
                            String amount_paid = obj1.getString("AMOUNT_TO_BE_MATCHED");
                            String amountout = obj1.getString("OUTSTANDING");
                            String amount_off = obj1.getString("ONE_TIME_BONUS");
                            String payment_status = obj1.getString("YIELD_AMOUNT");
                            String ref_bonus = obj1.getString("REF_BONUS");

                            Reqhistory_Model model = new Reqhistory_Model(sno, request_date, amount_off, amount_paid, amountout, payment_status,ref_bonus);
                            Constants.Reqhistory_ModelArrayList.add(model);

                        }

                    } else {
                        tv_no_data.setVisibility(View.VISIBLE);
                        tv_no_data.setText(Constants.message);
                        lv_requesthistory.setVisibility(View.GONE);
//                        if (!((Activity) context).isFinishing()) {
//                            AnyUtils.showserver_popup(Constants.message, getContext());
//                        }
                        AnyUtils.stopDialog(getActivity());
                    }
                    if(Constants.Reqhistory_ModelArrayList.size()==0) {
                        tv_no_data.setVisibility(View.VISIBLE);
                        lv_requesthistory.setVisibility(View.GONE);
                    }else{
                        tv_no_data.setVisibility(View.GONE);
                        lv_requesthistory.setVisibility(View.VISIBLE);
                    }

                    mAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    AnyUtils.stopDialog(getActivity());
                    e.printStackTrace();
                }
                break;

            default:
                break;
        }
    }

}
