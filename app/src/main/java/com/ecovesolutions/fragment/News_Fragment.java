package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.adapter.NewsAdapter;
import com.ecovesolutions.adapter.ReferralBonusAdapter;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.News_Model;
import com.ecovesolutions.model.ViewReferral_Model;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Admin on 4/12/2017.
 */

public class News_Fragment extends Fragment implements AsyncTaskCompleteListener,Response.ErrorListener {
    public static FragmentActivity context;
    private View view;
    Typeface gotham_light,gotham_book;
    public static NewsAdapter mAdapter;
    ListView lv_news;
    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;
    public static News_Fragment newInstance() {
        News_Fragment mapFragment = new News_Fragment();
        return mapFragment;
    }
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.news_fragment, container, false);
        pHelper = new PreferenceHelper(getContext());
        requestQueue = Volley.newRequestQueue(getContext());
        gotham_light= Typeface.createFromAsset(context.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(context.getAssets(), "fonts/gotham_book.otf");
        lv_news=(ListView)view.findViewById(R.id.lv_news);
        mAdapter=new NewsAdapter(getActivity(), R.layout.newsadapter, Constants.news_models);
        lv_news.setAdapter(mAdapter);

        getNews();
        return view;
    }
    public void getNews(){
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
        AnyUtils.startDialog(getActivity());
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.NEWS);
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map,1 , this, this));
    }
    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        if(!((Activity) context).isFinishing()){
            AnyUtils.showserver_popup(getActivity());
        }
    }
    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        switch (serviceCode) {
            case 1:
                try {
                    JSONObject jObj=new JSONObject(response);
                    Constants.status=jObj.getString(Constants.Params.STATUS);
                    Constants.message=jObj.getString(Constants.Params.MESSAGE);
                    if(Constants.status.matches("true")){
                        Constants.news_models.clear();
                        JSONArray arr_his=jObj.getJSONArray(Constants.Params.DATA);
                        for (int i = 0; i < arr_his.length(); i++) {
                            JSONObject obj1=arr_his.getJSONObject(i);
                            String id="";
                            String subject=obj1.getString(Constants.Params.SUBJECT);
                            String message=obj1.getString(Constants.Params.MESSAGE);
                            News_Model model=new News_Model(id,subject,message);
                            Constants.news_models.add(model);
                        }
                    }else{
                        if(!((Activity) context).isFinishing()){
                            AnyUtils.showserver_popup(Constants.message,getContext())	;
                        }
                        AnyUtils.stopDialog(getActivity());
                    }
                    mAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    AnyUtils.stopDialog(getActivity());
                    e.printStackTrace();
                }
                break;
            default:
                break;
        }
    }
}