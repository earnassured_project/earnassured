package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.ecovesolutions.adapter.Customadapter;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.model.PaymentModel;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;
import com.ecovesolutions.volley.AsyncTaskCompleteListener;
import com.ecovesolutions.volley.VolleyHttpRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Admin on 4/12/2017.
 */

public class DonateCharity_Fragment extends Fragment implements AsyncTaskCompleteListener,Response.ErrorListener{
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    ListView autocomplete;

    TextView tv_text,tv_testnews,tv_amount,tv_deatials,tv_name,tv_msg;
    EditText et_pay,et_message;
    Button btn_confirm;
    Typeface gotham_light,gotham_book;

    public static PreferenceHelper pHelper;
    public static RequestQueue requestQueue;
    public static DonateCharity_Fragment newInstance() {
        DonateCharity_Fragment mapFragment = new DonateCharity_Fragment();
        return mapFragment;
    }
    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.donate_charity, container, false);
        pHelper = new PreferenceHelper(getContext());
        requestQueue = Volley.newRequestQueue(getContext());

        autocomplete = (ListView) view.findViewById(R.id.autoCompleteTextView1);
        tv_msg= (TextView) view.findViewById(R.id.tv_msg);
        tv_name= (TextView) view.findViewById(R.id.tv_name);
        tv_text = (TextView) view.findViewById(R.id.tv_text);
        tv_testnews = (TextView) view.findViewById(R.id.tv_testnews);
        tv_amount = (TextView) view.findViewById(R.id.tv_amount);
        tv_deatials = (TextView) view.findViewById(R.id.tv_deatials);
        et_pay = (EditText) view.findViewById(R.id.et_pay);
        et_message=(EditText) view.findViewById(R.id.et_message);
        btn_confirm = (Button) view.findViewById(R.id.btn_confirm);
        //font style
        gotham_light= Typeface.createFromAsset(context.getAssets(), "fonts/gotham_light.ttf");
        gotham_book=Typeface.createFromAsset(context.getAssets(), "fonts/gotham_book.otf");
        et_message.setTypeface(gotham_light);
        tv_deatials.setTypeface(gotham_book);
        tv_msg.setTypeface(gotham_book);
        tv_amount.setTypeface(gotham_book);
        et_pay.setTypeface(gotham_light);
        btn_confirm.setTypeface(gotham_light);
        tv_testnews.setTypeface(gotham_book);
        tv_text.setTypeface(gotham_light);
        tv_name.setTypeface(gotham_book);
        tv_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (autocomplete.getVisibility() == View.VISIBLE) {
                    autocomplete.setVisibility(View.GONE);

                } else {
                    // Either gone or invisible
                    autocomplete.setVisibility(View.VISIBLE);


                    String[] arr = {"BANK PAYMENT($)"};

                    Customadapter adapter = new Customadapter(getActivity(), arr);
                    autocomplete.setAdapter(adapter);
                    setListViewHeightBasedOnChildren(autocomplete);

                    autocomplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String selectedItem = (String) parent.getItemAtPosition(position);
                            autocomplete.setVisibility(View.GONE);
                            tv_text.setText(selectedItem);
                        }
                    });

                }



            }

        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_pay.getText().length()==0){
                    Toast.makeText(getContext(), "Please Enter Amount", Toast.LENGTH_SHORT).show();
                }else if(et_message.getText().length()==0){
                    Toast.makeText(getContext(), "Please Enter Payment Details", Toast.LENGTH_SHORT).show();
                }else{
                    getDonateCharity();
                }

            }
        });

        return view;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight = 0;
        View view = null;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            view = listAdapter.getView(i, view, listView);
            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.MATCH_PARENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount()));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
    public void getDonateCharity() {
        if (!AnyUtils.isNetworkAvailable(getActivity())) {
            AnyUtils.showToast(context.getResources().getString(R.string.NoInternet), getContext());
            return;
        }
        AnyUtils.startDialog(getActivity());
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Constants.Params.URL, Constants.ServiceType.DONATECHARITY);
        map.put("confirm", "confirm");
        map.put("id", pHelper.getUserId());
        map.put("channel", "local");
        map.put("amount", et_pay.getText().toString());
        map.put("donation_details",et_message.getText().toString());
        requestQueue.add(new VolleyHttpRequest(Request.Method.POST, map, 1, this, this));
    }

    @Override
    public void onErrorResponse(VolleyError arg0) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        if (!((Activity) context).isFinishing()) {
            AnyUtils.showserver_popup(getActivity());
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        // TODO Auto-generated method stub
        AnyUtils.stopDialog(getActivity());
        switch (serviceCode) {
            case 1:
                try {
                    JSONObject jObj = new JSONObject(response);
                    Constants.status = jObj.getString(Constants.Params.STATUS);
                    Constants.message = jObj.getString(Constants.Params.MESSAGE);
                    if (Constants.status.matches("true")) {
                        et_pay.setText("");
                        et_message.setText("");
                        tv_msg.setText(jObj.getJSONObject(Constants.Params.DATA).getString(Constants.Params.MESSAGE));
                        tv_msg.setVisibility(View.VISIBLE);
                    } else {
                        if (!((Activity) context).isFinishing()) {
                            AnyUtils.showserver_popup(Constants.message, getContext());
                        }
                        AnyUtils.stopDialog(getActivity());
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    AnyUtils.stopDialog(getActivity());
                    e.printStackTrace();
                }
                break;

            default:
                break;
        }
    }
}