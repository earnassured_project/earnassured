package com.ecovesolutions.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.Request.Method;
import com.android.volley.RequestQueue;
import com.android.volley.Response.ErrorListener;
import com.android.volley.VolleyError;
import com.ecovesolutions.adapter.ViewReferralAdapter;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;
import com.ecovesolutions.utilities.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ViewReferral_Fragment extends Fragment {
	public static PreferenceHelper pHelper;
	public static FragmentActivity context;
	Typeface gotham_light,gotham_book;
	public static ViewReferralAdapter mAdapter;
	public static ListView lv_viewreferral;
	public static TextView tv_no_data,tv_share,tv_share_txt,tv_code,tv_spread;
	public static ProgressBar pb_internet;
	LinearLayout bt_share;
	public void onCreate(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	}
	@SuppressWarnings("deprecation")
	@Override
	public void onAttach(Activity activity) {
	    context = (FragmentActivity) activity;
	    super.onAttach(activity);
	}
	@Override
	@Nullable
	public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
	   View view = inflater.inflate(R.layout.viewreferral_fragment, container, false);
	   getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		pHelper = new PreferenceHelper(getContext());
	   gotham_light=Typeface.createFromAsset(getContext().getAssets(), "fonts/gotham_light.ttf");
       gotham_book=Typeface.createFromAsset(getContext().getAssets(), "fonts/gotham_book.otf");
		tv_no_data=(TextView)view.findViewById(R.id.tv_no_data);
		tv_no_data.setTypeface(AnyUtils.getGothamBook(context));
		pb_internet=(ProgressBar)view.findViewById(R.id.pb_internet);
		tv_share=(TextView)view.findViewById(R.id.tv_share);
		tv_share_txt=(TextView)view.findViewById(R.id.tv_share_txt);
		tv_code=(TextView)view.findViewById(R.id.tv_code);
		tv_spread=(TextView)view.findViewById(R.id.tv_spread);
		bt_share=(LinearLayout)view.findViewById(R.id.ll_share);

		tv_share.setTypeface(gotham_book);
		tv_share_txt.setTypeface(gotham_light);
		tv_code.setTypeface(gotham_book);
		tv_spread.setTypeface(gotham_light);
		String input=pHelper.getName().toUpperCase();
		tv_code.setText(input);
		lv_viewreferral=(ListView)view.findViewById(R.id.lv_viewreferral);
        mAdapter=new ViewReferralAdapter(getActivity(), R.layout.viewreferraladapter, Constants.viewreferral_model);
		lv_viewreferral.setAdapter(mAdapter);
		setListViewHeightBasedOnChildren(lv_viewreferral);

		final String value="Earnassured ! Use the Refferal Code to get exclusive offers on your first: "+tv_code.getText().toString();
		bt_share.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent sendIntent = new Intent();
				sendIntent.setAction(Intent.ACTION_SEND);
				sendIntent.putExtra(Intent.EXTRA_TEXT, ""+value);
				sendIntent.setType("text/plain");
				startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.shareto)));

			}
		});
       return view;
	}

	@Override
	public void onAttach(Context context) {
		// TODO Auto-generated method stub
		super.onAttach(context);
	}
	@Override
	public void onDetach() {
		// TODO Auto-generated method stub
		super.onDetach();
	}
	public static void setListViewHeightBasedOnChildren(ListView listView) {
		ViewReferralAdapter listAdapter = (ViewReferralAdapter) listView.getAdapter();
		if (listAdapter == null) {
			return;
		}
		int totalHeight = 0;
		for (int i = 0; i < listAdapter.getCount(); i++) {
			View listItem = listAdapter.getView(i, null, listView);
			listItem.measure(0, 0);
			totalHeight += listItem.getMeasuredHeight();
		}
		ViewGroup.LayoutParams params = listView.getLayoutParams();
		params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
		listView.setLayoutParams(params);
		listView.requestLayout();
	}
}
