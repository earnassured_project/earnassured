package com.ecovesolutions.fragment;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ecovesolutions.adapter.HelpsOffererdAdapter;
import com.ecovesolutions.earnassured.R;
import com.ecovesolutions.utilities.AnyUtils;
import com.ecovesolutions.utilities.Constants;

/**
 * Created by Admin on 4/21/2017.
 */

public class HelpsOffererd extends Fragment {
    public static FragmentActivity context;
    private Bundle mBundle;
    private View view;
    public static ListView lv_helpsoffererd;
    public static TextView tv_no_data;
    public static ProgressBar pb_internet;
    public static HelpsOffererdAdapter mAdapter;
    public static HelpsOffererd newInstance() {
        HelpsOffererd mapFragment = new HelpsOffererd();
        return mapFragment;
    }

    @Override
    public void onAttach(Activity activity) {
        context = (FragmentActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.helpsoffererd, container, false);
        tv_no_data=(TextView)view.findViewById(R.id.tv_no_data);
        tv_no_data.setTypeface(AnyUtils.getGothamBook(context));
        pb_internet=(ProgressBar)view.findViewById(R.id.pb_internet);


        lv_helpsoffererd = (ListView) view.findViewById(R.id.lv_helpsoffererd);
        if(Constants.helpsOfferedModels.size()==0) {
            tv_no_data.setVisibility(View.VISIBLE);
            lv_helpsoffererd.setVisibility(View.GONE);
            pb_internet.setVisibility(View.GONE);
        }else{
            pb_internet.setVisibility(View.GONE);
            tv_no_data.setVisibility(View.GONE);
            lv_helpsoffererd.setVisibility(View.VISIBLE);
        }

        mAdapter=new HelpsOffererdAdapter(getActivity(),R.layout.helpsreceivedchild, Constants.helpsOfferedModels);
        lv_helpsoffererd.setAdapter(mAdapter);
        return view;
    }
    public static void refresh(){
        mAdapter.notifyDataSetChanged();
        if(Constants.helpsOfferedModels.size()==0) {
            tv_no_data.setVisibility(View.VISIBLE);
            lv_helpsoffererd.setVisibility(View.GONE);
            pb_internet.setVisibility(View.GONE);
        }else{
            pb_internet.setVisibility(View.GONE);
            tv_no_data.setVisibility(View.GONE);
            lv_helpsoffererd.setVisibility(View.VISIBLE);
        }
    }
}

